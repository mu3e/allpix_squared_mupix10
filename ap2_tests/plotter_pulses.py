#!/usr/bin/python3

import ast
import ROOT


def ChargePulsePlot() -> ROOT.TCanvas:
    ROOT.SetTitleStyle()
    canvas = ROOT.TCanvas('canvas_ChargePulsePlot', 'canvas_ChargePulsePlot', -1280, 720)

    lines = list()
    with open('pulses_hists.txt', 'rt') as linegraphs_file:
        lines = linegraphs_file.readlines()[0:4]

    tfile = ROOT.TFile('output_linegraphs_plots/modules.root', 'READ')

    tgraph_pulse = ROOT.gDirectory.Get(lines[0][:-1])

    Q_tot = float(lines[1][6:-2])
    Q_range = ast.literal_eval(lines[2][14:-1])
    t_range = ast.literal_eval(lines[3][14:-1])

    tgraph_pulse.SetTitle(f'Induced charge per unit step, total charge {Q_tot:.0f}e;time [ns];charge [e]')
    tgraph_pulse.GetXaxis().SetRangeUser(t_range[0], t_range[1])
    tgraph_pulse.GetYaxis().SetRangeUser(Q_range[0], Q_range[1])

    tgraph_pulse.SetMarkerStyle(ROOT.kFullDotMedium)

    tgraph_pulse.Draw('ALP')

    canvas.Modified()
    canvas.Update()

    canvas.SaveAs('ChargePulsePlot.pdf')

    tfile.Close()

    print('Done: ChargePulsePlot')

    return canvas


def AmplifiedPulsePlot() -> ROOT.TCanvas:
    ROOT.SetTitleStyle()
    canvas = ROOT.TCanvas('canvas_AmplifiedPulsePlot', 'canvas_AmplifiedPulsePlot', -1280, 720)

    lines = list()
    with open('pulses_hists.txt', 'rt') as linegraphs_file:
        lines = linegraphs_file.readlines()[4:8]

    tfile = ROOT.TFile('output_linegraphs_plots/modules.root', 'READ')

    tgraph_pulse = ROOT.gDirectory.Get(lines[0][:-1])

    Q_eff = float(lines[1][6:-2])
    V_range = ast.literal_eval(lines[2][15:-1])
    t_range = ast.literal_eval(lines[3][15:-1])

    tgraph_pulse.SetTitle(f'Amplified pulse with noise, effective charge {Q_eff:.0f}e;time [ns];AmpOut [mV]')
    tgraph_pulse.GetXaxis().SetRangeUser(t_range[0], t_range[1])
    tgraph_pulse.GetYaxis().SetRangeUser(V_range[0], V_range[1])

    tgraph_pulse.Draw('AL')

    canvas.Modified()
    canvas.Update()

    canvas.SaveAs('AmplifiedPulsePlot.pdf')

    tfile.Close()

    print('Done: AmplifiedPulsePlot')

    return canvas


def PulsesAllPlots() -> None:
    ChargePulsePlot()
    AmplifiedPulsePlot()
    print('Done: Sr90')


if __name__ == '__main__':
    ROOT.gInterpreter.ProcessLine('#include \"../csaparam/AllpixStyle.hpp\"')
    ROOT.SetInitialStyle()

    _canvas_ChargePulsePlot = ChargePulsePlot()
    _canvas_AmplifiedPulsePlot = AmplifiedPulsePlot()

    input()
