#!/usr/bin/python3

from array import array
import numpy as np
import ROOT


def Sr90ToTSimSingleComp() -> ROOT.TCanvas:
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_Sr90ToTSimSingleComp', 'canvas_Sr90ToTSimSingleComp', -1280, 720)

    f_Sr90 = ROOT.TFile('Sr90_Cuts.root', 'READ')
    h_Sr90 = f_Sr90.Get('ClusteringHistograms/Sensor0/ToT_earliest_col_120_row_0sensor_0')
    h_Sr90.Rebin(16)

    f_ap2 = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_ap2 = f_ap2.Get('event_info')

    t_ap2.Draw('tot>>h_tot(63,0,8064)', 'tot<8000 && tot>500', 'colz')
    h_ap2 = ROOT.gDirectory.Get("h_tot")

    h_Sr90.SetTitle(';ToT [ns];normalized entries')
    h_ap2.SetLineColor(ROOT.kRed)

    h_Sr90.DrawNormalized('hist')
    h_ap2.DrawNormalized('same')

    leg = ROOT.TLegend(0.52, 0.65, 0.97, 0.94)
    leg.AddEntry(h_Sr90, 'Sr90 measurement, pixel (120,0)')
    leg.AddEntry(h_ap2, 'Y90 simulation')
    leg.Draw()

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90ToTSimSingleComp.pdf')

    f_Sr90.Close()
    f_ap2.Close()

    print('Done: Sr90ToTSimSingleComp')

    return canvas


def Sr90ToTSimAllComp() -> ROOT.TCanvas:
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_Sr90ToTSimAllComp', 'canvas_Sr90ToTSimAllComp', -1280, 720)

    f_Sr90 = ROOT.TFile('Sr90_Cuts.root', 'READ')
    h_Sr90 = f_Sr90.Get('ClusteringHistograms/Sensor0/ToT_earliestsensor_0')
    h_Sr90.Rebin(16)

    f_ap2 = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_ap2 = f_ap2.Get('event_info')

    t_ap2.Draw('tot>>h_tot(63,0,8064)', 'tot<8000 && tot>500', 'colz')
    h_ap2 = ROOT.gDirectory.Get("h_tot")

    h_Sr90.SetTitle(';ToT [ns];normalized entries')
    h_ap2.SetLineColor(ROOT.kRed)

    h_Sr90.DrawNormalized('hist')
    h_ap2.DrawNormalized('same')

    leg = ROOT.TLegend(0.52, 0.65, 0.97, 0.94)
    leg.AddEntry(h_Sr90, 'Sr90 measurement, full sensor')
    leg.AddEntry(h_ap2, 'Y90 simulation')
    leg.Draw()

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90ToTSimAllComp.pdf')

    f_Sr90.Close()
    f_ap2.Close()

    print('Done: Sr90ToTSimAllComp')

    return canvas


def Sr90ToTMeasurementComp() -> ROOT.TCanvas:
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_Sr90ToTMeasurementComp', 'canvas_Sr90ToTMeasurementComp', -1280, 720)

    f_120_0 = ROOT.TFile('Sr90_120_0.root', 'READ')
    h_120_0 = f_120_0.Get('SensorHistograms/SensorHistograms0/tot_spectrum_sensor_0')
    h_120_0.Rebin(16)

    f_all = ROOT.TFile('Sr90_all.root', 'READ')
    h_all = f_all.Get('SensorHistograms/SensorHistograms0/tot_spectrum_sensor_0')
    h_all.Rebin(16)

    h_all.SetTitle(';ToT [ns];normalized entries')
    h_all.SetLineColor(ROOT.kRed)

    h_all.DrawNormalized('hist')
    h_120_0.DrawNormalized('same hist')

    leg = ROOT.TLegend(0.7, 0.65, 0.97, 0.94)
    leg.SetTextSize(0.04)
    leg.AddEntry(h_120_0, 'pixel (120,0)')
    leg.AddEntry(h_all, 'full sensor')
    leg.Draw()

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90ToTMeasurementComp.pdf')

    f_120_0.Close()
    f_all.Close()

    print('Done: Sr90ToTMeasurementComp')

    return canvas


def Sr90ToTMeasurementCompCut() -> ROOT.TCanvas:
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_Sr90ToTMeasurementCompCut', 'canvas_Sr90ToTMeasurementCompCut', -1280, 720)

    f_cuts = ROOT.TFile('Sr90_Cuts.root', 'READ')

    h_all = f_cuts.Get('ClusteringHistograms/Sensor0/ToT_earliestsensor_0')
    h_all.Rebin(16)

    h_120_0 = f_cuts.Get('ClusteringHistograms/Sensor0/ToT_earliest_col_120_row_0sensor_0')
    h_120_0.Rebin(16)

    h_120_0.SetTitle(';ToT [ns];normalized entries')
    h_all.SetLineColor(ROOT.kRed)

    h_120_0.DrawNormalized('hist')
    h_all.DrawNormalized('same hist')

    leg = ROOT.TLegend(0.7, 0.65, 0.97, 0.94)
    leg.SetTextSize(0.04)
    leg.AddEntry(h_120_0, 'pixel (120,0)')
    leg.AddEntry(h_all, 'full sensor')
    leg.Draw()

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90ToTMeasurementCompCut.pdf')

    f_cuts.Close()

    print('Done: Sr90ToTMeasurementCompCut')

    return canvas


def Sr90ClusterSizeMeasurement() -> ROOT.TCanvas:
    ROOT.SetLogyStyle()
    canvas = ROOT.TCanvas('canvas_Sr90ClusterSizeMeasurement', 'canvas_Sr90ClusterSizeMeasurement', -1280, 720)

    f_clustersize = ROOT.TFile('Sr90_Clustersize.root', 'READ')
    h_clustersize = f_clustersize.Get('ClusteringHistograms/clustersize_overall')

    f_cs_cut = ROOT.TFile('Sr90_Cuts.root', 'READ')
    h_cs_cut = f_cs_cut.Get('ClusteringHistograms/Sensor0/clustersize_after_tot_cut')

    h_clustersize.SetTitle(';cluster size [pixel];normalized entries')
    h_cs_cut.SetLineColor(ROOT.kRed)

    h_clustersize.DrawNormalized()
    h_cs_cut.DrawNormalized('same')

    leg = ROOT.TLegend(0.7, 0.65, 0.97, 0.94)
    leg.AddEntry(h_clustersize, 'Without ToT cut')
    leg.AddEntry(h_cs_cut, 'With ToT cut')
    leg.Draw()

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90ClusterSizeMeasurement.pdf')

    f_clustersize.Close()

    print('Done: Sr90ClusterSizeMeasurement')

    return canvas


def Sr90ClusterSizeSimComp() -> ROOT.TCanvas:
    ROOT.SetLogyStyle()
    canvas = ROOT.TCanvas('canvas_Sr90ClusterSizeSimComp', 'canvas_Sr90ClusterSizeSimComp', -1280, 720)

    f_clustersize_measurement = ROOT.TFile('Sr90_Cuts.root', 'READ')
    h_clustersize_measurement = f_clustersize_measurement.Get('ClusteringHistograms/Sensor0/clustersize_after_tot_cut')

    f_clustersize_sim = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_sim = f_clustersize_sim.Get('event_info')
    t_sim.Draw('cluster_size>>h_clustersize_sim(26,-0.5,25.5)', '', 'colz')
    h_clustersize_sim = ROOT.gDirectory.Get('h_clustersize_sim')

    h_clustersize_measurement.SetTitle(';cluster size [pixel];normalized entries')
    h_clustersize_sim.SetTitle(';cluster size [pixel];normalized entries')
    h_clustersize_sim.SetLineColor(ROOT.kRed)

    if (h_clustersize_sim.GetNbinsX() > h_clustersize_measurement.GetNbinsX()):
        h_clustersize_sim.GetXaxis().Copy(h_clustersize_measurement.GetXaxis())
    elif (h_clustersize_sim.GetNbinsX() < h_clustersize_measurement.GetNbinsX()):
        h_clustersize_measurement.GetXaxis().Copy(h_clustersize_sim.GetXaxis())

    h_clustersize_measurement.DrawNormalized()
    h_clustersize_sim.DrawNormalized('same')

    leg = ROOT.TLegend(0.52, 0.65, 0.97, 0.94)
    leg.AddEntry(h_clustersize_measurement, 'Sr90 measurement')
    leg.AddEntry(h_clustersize_sim, 'Y90 simulation')
    leg.Draw()

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90ClusterSizeSimComp.pdf')

    f_clustersize_measurement.Close()
    f_clustersize_sim.Close()

    print('Done: Sr90ClusterSizeSimComp')

    return canvas


def Sr90SimPixelChargeKinEnergy() -> ROOT.TCanvas:
    ROOT.SetTH2Style()
    canvas = ROOT.TCanvas('canvas_Sr90SimPixelChargeKinEnergy', 'canvas_Sr90SimPixelChargeKinEnergy', -1280, 720)

    f_sim = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_sim = f_sim.Get('event_info')

    xbins = array('f', np.linspace(0., 2.2, num=88+1))
    ybins = array('f', np.linspace(0.5, 12., num=92+1))
    h_charge_vs_kin_energy = ROOT.TH2F('h_charge_vs_kin_energy', 'h_charge_vs_kin_energy', len(xbins)-1, xbins, len(ybins)-1, ybins)


    t_sim.Draw('charge[]/1e3:kin_energy>>h_charge_vs_kin_energy', 'pdg_code==11', 'colz')

    h_charge_vs_kin_energy.SetTitle(';kinetic energy [MeV];pixel charge [ke];entries')
    h_charge_vs_kin_energy.GetYaxis().SetMaxDigits(4)

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90SimPixelChargeKinEnergy.pdf')

    f_sim.Close()

    print('Done: Sr90SimPixelChargeKinEnergy')

    return canvas


def Sr90SimPixelChargeKinEnergyLogx() -> ROOT.TCanvas:
    ROOT.SetTH2LogxStyle()
    canvas = ROOT.TCanvas('canvas_Sr90SimPixelChargeKinEnergyLogx', 'canvas_Sr90SimPixelChargeKinEnergyLogx', -1280, 720)

    f_sim = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_sim = f_sim.Get('event_info')

    xbins = array('f', np.linspace(0.1, 2.2, num=84+1))
    ybins = array('f', np.linspace(0.5, 12., num=92+1))
    h_charge_vs_kin_energy_logx = ROOT.TH2F('h_charge_vs_kin_energy_logx', 'h_charge_vs_kin_energy_logx', len(xbins)-1, xbins, len(ybins)-1, ybins)

    t_sim.Draw('charge[]/1e3:kin_energy>>h_charge_vs_kin_energy_logx', 'pdg_code==11', 'colz')

    h_charge_vs_kin_energy_logx.GetXaxis().SetMoreLogLabels()
    h_charge_vs_kin_energy_logx.SetTitle(';kinetic energy [MeV];pixel charge [ke];entries')
    h_charge_vs_kin_energy_logx.GetYaxis().SetMaxDigits(4)

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90SimPixelChargeKinEnergyLogx.pdf')

    f_sim.Close()

    print('Done: Sr90SimPixelChargeKinEnergyLogx')

    return canvas


def Sr90SimTotalChargeKinEnergyLogx() -> ROOT.TCanvas:
    ROOT.SetTH2LogxStyle()
    canvas = ROOT.TCanvas('canvas_Sr90SimTotalChargeKinEnergyLogx', 'canvas_Sr90SimTotalChargeKinEnergyLogx', -1280, 720)

    f_sim = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_sim = f_sim.Get('event_info')

    xbins = array('f', np.linspace(0.1, 2.2, num=84+1))
    ybins = array('f', np.linspace(0.5, 12., num=92+1))
    h_charge_sum_vs_kin_energy_logx = ROOT.TH2F('h_charge_sum_vs_kin_energy_logx', 'h_charge_sum_vs_kin_energy_logx', len(xbins)-1, xbins, len(ybins)-1, ybins)

    t_sim.Draw('charge_sum/1e3:kin_energy>>h_charge_sum_vs_kin_energy_logx', 'pdg_code==11', 'colz')

    h_charge_sum_vs_kin_energy_logx.GetXaxis().SetMoreLogLabels()
    h_charge_sum_vs_kin_energy_logx.SetTitle(';kinetic energy [MeV];total charge [ke];entries')
    h_charge_sum_vs_kin_energy_logx.GetYaxis().SetMaxDigits(4)

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90SimTotalChargeKinEnergyLogx.pdf')

    f_sim.Close()

    print('Done: Sr90SimTotalChargeKinEnergyLogx')

    return canvas


def Sr90SimPixelToTKinEnergy() -> ROOT.TCanvas:
    ROOT.SetTH2Style()
    canvas = ROOT.TCanvas('canvas_Sr90SimPixelToTKinEnergy', 'canvas_Sr90SimPixelToTKinEnergy', -1280, 720)

    f_sim = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_sim = f_sim.Get('event_info')

    xbins = array('f', np.linspace(0., 2.2, num=88+1))
    ybins = array('f', np.linspace(0, 8064, num=63+1))
    h_tot_vs_kin_energy = ROOT.TH2F('h_tot_vs_kin_energy', 'h_tot_vs_kin_energy', len(xbins)-1, xbins, len(ybins)-1, ybins)

    t_sim.Draw('tot[]:kin_energy>>h_tot_vs_kin_energy', 'pdg_code==11 && tot<8000 && tot>128', 'colz')

    h_tot_vs_kin_energy.SetTitle(';kinetic energy [MeV];pixel ToT [ns];entries')

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90SimPixelToTKinEnergy.pdf')

    f_sim.Close()

    print('Done: Sr90SimPixelToTKinEnergy')

    return canvas


def Sr90SimPixelToTKinEnergyLogx() -> ROOT.TCanvas:
    ROOT.SetTH2LogxStyle()
    canvas = ROOT.TCanvas('canvas_Sr90SimPixelToTKinEnergyLogx', 'canvas_Sr90SimPixelToTKinEnergyLogx', -1280, 720)

    f_sim = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_sim = f_sim.Get('event_info')

    xbins = array('f', np.linspace(0.1, 2.2, num=84+1))
    ybins = array('f', np.linspace(0, 8064, num=63+1))
    h_tot_vs_kin_energy_logx = ROOT.TH2F('h_tot_vs_kin_energy_logx', 'h_tot_vs_kin_energy_logx', len(xbins)-1, xbins, len(ybins)-1, ybins)

    t_sim.Draw('tot[]:kin_energy>>h_tot_vs_kin_energy_logx', 'pdg_code==11 && tot<8000 && tot>128', 'colz')

    h_tot_vs_kin_energy_logx.GetXaxis().SetMoreLogLabels()
    h_tot_vs_kin_energy_logx.SetTitle(';kinetic energy [MeV];pixel ToT [ns];entries')

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90SimPixelToTKinEnergyLogx.pdf')

    f_sim.Close()

    print('Done: Sr90SimPixelToTKinEnergyLogx')

    return canvas


def Sr90SimToTSumKinEnergyLogx() -> ROOT.TCanvas:
    ROOT.SetTH2LogxStyle()
    canvas = ROOT.TCanvas('canvas_Sr90SimToTSumKinEnergyLogx', 'canvas_Sr90SimToTSumKinEnergyLogx', -1280, 720)

    f_sim = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_sim = f_sim.Get('event_info')

    xbins = array('f', np.linspace(0.1, 2.2, num=84+1))
    ybins = array('f', np.linspace(0, 12800, num=100+1))
    h_tot_sum_vs_kin_energy_logx = ROOT.TH2F('h_tot_sum_vs_kin_energy_logx', 'h_tot_sum_vs_kin_energy_logx', len(xbins)-1, xbins, len(ybins)-1, ybins)

    t_sim.Draw('tot_sum:kin_energy>>h_tot_sum_vs_kin_energy_logx', 'pdg_code==11 && tot<8000 && tot>128', 'colz')

    h_tot_sum_vs_kin_energy_logx.GetXaxis().SetMoreLogLabels()
    h_tot_sum_vs_kin_energy_logx.SetTitle(';kinetic energy [MeV];ToT sum [ns];entries')

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90SimToTSumKinEnergyLogx.pdf')

    f_sim.Close()

    print('Done: Sr90SimToTSumKinEnergyLogx')

    return canvas


def Sr90SimPixelToTPixelCharge() -> ROOT.TCanvas:
    ROOT.SetTH2Style()
    canvas = ROOT.TCanvas('canvas_Sr90SimPixelToTPixelCharge', 'canvas_Sr90SimPixelToTPixelCharge', -1280, 720)

    f_sim = ROOT.TFile('output_sr90_tcad_hhf_y90/analysis_data.root', 'READ')
    t_sim = f_sim.Get('event_info')

    t_sim.Draw('tot:charge/1e3>>h_tot_vs_charge(92,0.5,12.,63,0,8064)', 'tot<8000 && tot>128', 'colz')
    h_tot_vs_charge = ROOT.gDirectory.Get("h_tot_vs_charge")

    h_tot_vs_charge.SetTitle(';pixel charge [ke];pixel ToT [ns];entries')

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('Sr90SimPixelToTPixelCharge.pdf')

    f_sim.Close()

    print('Done: Sr90SimPixelToTPixelCharge')

    return canvas


def Sr90AllPlots() -> None:
    Sr90ToTSimSingleComp()
    Sr90ToTSimAllComp()
    Sr90ToTMeasurementComp()
    Sr90ClusterSizeMeasurement()
    Sr90ToTMeasurementCompCut()
    Sr90ClusterSizeSimComp()
    Sr90SimPixelChargeKinEnergy()
    Sr90SimPixelChargeKinEnergyLogx()
    Sr90SimTotalChargeKinEnergyLogx()
    Sr90SimPixelToTKinEnergy()
    Sr90SimPixelToTKinEnergyLogx()
    Sr90SimToTSumKinEnergyLogx()
    Sr90SimPixelToTPixelCharge()
    print('Done: Sr90')


if __name__ == '__main__':
    ROOT.gInterpreter.ProcessLine('#include \"../csaparam/AllpixStyle.hpp\"')
    ROOT.SetInitialStyle()

    _canvas_Sr90ToTSimSingleComp  = Sr90ToTSimSingleComp()
    _canvas_Sr90ToTSimAllComp  = Sr90ToTSimAllComp()
    _canvas_Sr90ToTMeasurementComp = Sr90ToTMeasurementComp()
    _canvas_Sr90ToTMeasurementCompCut = Sr90ToTMeasurementCompCut()
    _canvas_Sr90ClusterSizeMeasurement = Sr90ClusterSizeMeasurement()
    _canvas_Sr90ClusterSizeSimComp = Sr90ClusterSizeSimComp()
    _canvas_Sr90SimPixelChargeKinEnergy = Sr90SimPixelChargeKinEnergy()
    _canvas_Sr90SimPixelChargeKinEnergyLogx = Sr90SimPixelChargeKinEnergyLogx()
    _canvas_Sr90SimTotalChargeKinEnergyLogx = Sr90SimTotalChargeKinEnergyLogx()
    _canvas_Sr90SimPixelToTKinEnergy = Sr90SimPixelToTKinEnergy()
    _canvas_Sr90SimPixelToTKinEnergyLogx = Sr90SimPixelToTKinEnergyLogx()
    _canvas_Sr90SimToTSumKinEnergyLogx = Sr90SimToTSumKinEnergyLogx()
    _canvas_Sr90SimPixelToTPixelCharge = Sr90SimPixelToTPixelCharge()

    input()
