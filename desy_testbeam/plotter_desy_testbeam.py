#!/usr/bin/python3

import ROOT


def DesyToT() -> ROOT.TCanvas():
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_DesyToT', 'canvas_DesyToT', -1280, 720)

    f_DESY = ROOT.TFile('DESY_testbeam_35mV.root', 'READ')
    h_DESY = f_DESY.Get('SensorHistograms/SensorHistograms1/tot_spectrum_sensor_1')
    h_DESY.Rebin(16)

    f_ap2 = ROOT.TFile('output_desy_testbeam/analysis_data.root', 'READ')
    h_ap2 = f_ap2.Get('h_tot')

    h_DESY.SetTitle(';ToT [ns];normalized entries')
    h_ap2.SetLineColor(ROOT.kRed)

    h_DESY.GetYaxis().SetRangeUser(0., 0.15)

    h_DESY.Draw('hist')
    h_ap2.DrawNormalized('same')

    leg = ROOT.TLegend(0.52, 0.65, 0.97, 0.94)
    leg.AddEntry(h_DESY, 'DESY testbeam measurement')
    leg.AddEntry(h_ap2, '4GeV electron simulation')
    leg.Draw()

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs('DesyToT.pdf')

    f_DESY.Close()
    f_ap2.Close()

    print('Done: DesyToT')

    return canvas



def Sr90AllPlots() -> None:
    DesyToT()
    print('Done: DESY testbeam')


if __name__ == '__main__':
    ROOT.gInterpreter.ProcessLine('#include \"../csaparam/AllpixStyle.hpp\"')
    ROOT.SetInitialStyle()

    _canvas_DesyToT  = DesyToT()

    input()

