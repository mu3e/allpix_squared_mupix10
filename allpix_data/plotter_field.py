#!/usr/bin/python3

import os
import ROOT


def _ReverseYAxis(th2: ROOT.TH2) -> ROOT.TH2:
    reversed_th2 = th2.Clone(th2.GetName() + '_revy')

    nbinsx = th2.GetNbinsX()
    nbinsy = th2.GetNbinsY()
    for n in range(1, nbinsx + 1):
        for m in range(1, nbinsy + 1):
            bin_xy = th2.GetBin(n, m)
            content = th2.GetBinContent(bin_xy) * 1e4
            bin_xrevy = reversed_th2.GetBin(n, nbinsy - (m - 1))
            reversed_th2.SetBinContent(bin_xrevy, content)

    reversed_th2.SetOption('colz')
    reversed_th2.SetZTitle(f'field strength [kV/cm]')

    return reversed_th2


_mesh_plotter = '~/Projects/allpix-squared/bin/mesh_plotter'



def ElectricFieldYZCut() -> ROOT.TCanvas:
    ROOT.SetTH2Style()
    canvas = ROOT.TCanvas('canvas_ElectricFieldYZCut', 'canvas_ElectricFieldYZCut', -880, 720)
    canvas.SetRightMargin(0.17)
    canvas.SetLeftMargin(0.11)
    canvas.SetBottomMargin(0.12)

    os.system(f'{_mesh_plotter} -f mupix10_100um_100V_ElectricField.apf -p yz')

    tfile_yz = ROOT.TFile('mupix10_100um_100V_ElectricField_Interpolation_plots_yz_39.root', 'READ')
    th2_yz = tfile_yz.Get('ElectricField Norm')
    th2_yz_revy = _ReverseYAxis(th2_yz)
    th2_yz_revy.SetTitle(';y [um];z [um]')
    th2_yz_revy.GetZaxis().SetRangeUser(0, 50)
    th2_yz_revy.GetXaxis().SetTitleOffset(1.)
    th2_yz_revy.GetZaxis().SetTitleOffset(1.4)

    th2_yz_revy.Draw()

    canvas.Modified()
    canvas.Update()

    canvas.SaveAs('ElectricFieldYZCut.pdf')

    tfile_yz.Close()

    os.system('rm mupix10_100um_100V_ElectricField_Interpolation_plots_yz_39.root')
    os.system('rm mupix10_100um_100V_ElectricField_yz_39.png')

    print('Done: ElectricFieldYZCut')

    return canvas


def ElectricFieldXYImplant() -> ROOT.TCanvas:
    ROOT.SetTH2Style()
    canvas = ROOT.TCanvas('canvas_ElectricFieldXYImplant', 'canvas_ElectricFieldXYImplant', -880, 720)
    canvas.SetRightMargin(0.17)
    canvas.SetLeftMargin(0.11)
    canvas.SetBottomMargin(0.12)

    os.system(f'{_mesh_plotter} -f mupix10_100um_100V_ElectricField.apf -p xy -c 84')

    tfile_xz = ROOT.TFile('mupix10_100um_100V_ElectricField_Interpolation_plots_xy_84.root', 'READ')
    th2_xy = tfile_xz.Get('ElectricField Norm')
    th2_xy_revy = _ReverseYAxis(th2_xy)
    th2_xy_revy.SetTitle(';x [um];y [um]')
    th2_xy_revy.GetZaxis().SetRangeUser(0, 220)
    th2_xy_revy.GetXaxis().SetTitleOffset(1.)
    th2_xy_revy.GetZaxis().SetTitleOffset(1.4)

    th2_xy_revy.Draw()

    canvas.Modified()
    canvas.Update()

    canvas.SaveAs('ElectricFieldXYImplant.pdf')

    tfile_xz.Close()

    os.system('rm mupix10_100um_100V_ElectricField_Interpolation_plots_xy_84.root')
    os.system('rm mupix10_100um_100V_ElectricField_xy_84.png')

    print('Done: ElectricFieldXYImplant')

    return canvas


def ElectricFieldAllPlots() -> None:
    ElectricFieldYZCut()
    ElectricFieldXYImplant()
    print('Done: ElectricField')


if __name__ == '__main__':
    ROOT.gInterpreter.ProcessLine('#include \"../csaparam/AllpixStyle.hpp\"')
    ROOT.SetInitialStyle()

    _canvas_ElectricFieldYZCut = ElectricFieldYZCut()
    _canvas_ElectricFieldXYImplant = ElectricFieldXYImplant()

    input()
