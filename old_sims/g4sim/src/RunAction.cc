#include "RunAction.hh"

#include <G4RootAnalysisManager.hh>
#include <G4SystemOfUnits.hh>
#include <G4RunManager.hh>
#include <G4Run.hh>
#include <G4ios.hh>

#include "RunTimer.hh"


RunAction::RunAction()
  : G4UserRunAction(),
    fAnalysisManager(nullptr),
    fRunTimer(nullptr)
{
    fAnalysisManager = G4RootAnalysisManager::Instance();

    fAnalysisManager->SetNtupleMerging(true);
    fAnalysisManager->SetFirstNtupleId(0);
    fAnalysisManager->SetFirstNtupleColumnId(0);

    // According to DepositionReader module from Allpix-Squared
    // plus kinetic energy from the particle added
    fAnalysisManager->CreateNtuple("Deposition", "Simulation Data for Allpix-Squared DepositionReader");
    fAnalysisManager->CreateNtupleIColumn(0, "event");
    fAnalysisManager->CreateNtupleDColumn(0, "energy");
    fAnalysisManager->CreateNtupleDColumn(0, "time");
    fAnalysisManager->CreateNtupleDColumn(0, "position.x");
    fAnalysisManager->CreateNtupleDColumn(0, "position.y");
    fAnalysisManager->CreateNtupleDColumn(0, "position.z");
    fAnalysisManager->CreateNtupleSColumn(0, "detector");
    fAnalysisManager->CreateNtupleIColumn(0, "pdg_code");
    fAnalysisManager->CreateNtupleIColumn(0, "track_id");
    fAnalysisManager->CreateNtupleIColumn(0, "parent_id");
    fAnalysisManager->CreateNtupleDColumn(0, "kin_energy");
    fAnalysisManager->FinishNtuple(0);

    G4RunManager::GetRunManager()->SetPrintProgress(10000);

    fRunTimer = new RunTimer();
}

RunAction::~RunAction()
{
    delete fAnalysisManager;
    delete fRunTimer;
}

void RunAction::BeginOfRunAction(const G4Run*)
{
    if (IsMaster())
    {
        fRunTimer->StartTimer();
    }

    // TODO: store metadata in new RunData class

    auto fileName = fAnalysisManager->GetFileName();
    if (fileName == "")
    {
        fileName = "../data/run2.root";
    }
    fAnalysisManager->OpenFile(fileName);
}

void RunAction::EndOfRunAction(const G4Run* run)
{
    // merge and write
    fAnalysisManager->Write();

    if (IsMaster())
    {
        fRunTimer->StopTimer();

        G4cout << "Processed " << run->GetNumberOfEvent() << " events in " << fRunTimer->GetPrettyDuration() << "." << G4endl;
    }

    // close file and reset
    fAnalysisManager->CloseFile(true);

    // TODO: save metadata from RunData class in tree UserInfo
}
