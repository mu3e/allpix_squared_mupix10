#pragma once
#include <G4VUserPrimaryGeneratorAction.hh>

#include <G4Event.hh>
#include <G4GeneralParticleSource.hh>
#include <G4IonTable.hh>
#include <G4ParticleDefinition.hh>
#include <G4Types.hh>

class GeneratorMessenger;


class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
    public:
        PrimaryGeneratorAction();
        virtual ~PrimaryGeneratorAction();

    public:
        virtual void GeneratePrimaries(G4Event* event);

    private:
        GeneratorMessenger* fGeneratorMessenger;
        G4GeneralParticleSource* fGenericParticleSource;
        G4IonTable* fIonTable;
        G4ParticleDefinition* fGeantino;
        G4ParticleDefinition* fIon;

    public:
        void SetPositionZ(G4double dist);
};
