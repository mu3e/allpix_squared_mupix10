#include "SensitiveDetector.hh"

#include <random>

#include <G4HCofThisEvent.hh>
#include <G4Step.hh>
#include <G4TouchableHistory.hh>
#include <G4Track.hh>
#include <G4ThreeVector.hh>

#include "DetectorGlobals.hh"
#include "DetectorHit.hh"
#include "PhysicsGlobals.hh"


SensitiveDetector::SensitiveDetector()
  : G4VSensitiveDetector(detectorName),
    fHitsCollection(nullptr),
    fRandomGenerator()
{
    // seed random generator
    std::random_device randomDevice;
    std::seed_seq seedSequence = {randomDevice(), randomDevice(), randomDevice(), randomDevice(), randomDevice()};
    fRandomGenerator.seed(seedSequence);

    collectionName.insert(hcName);
}

SensitiveDetector::~SensitiveDetector()
{}

void SensitiveDetector::Initialize(G4HCofThisEvent* hitsCollection)
{
    fHitsCollection = new HitsCollection(detectorName, hcName);

    // hcID == 0 because we only have one hc
    hitsCollection->AddHitsCollection(0, fHitsCollection);
}

G4bool SensitiveDetector::ProcessHits(G4Step* step, G4TouchableHistory*)
{
    const auto* track = step->GetTrack();

    const auto PDGCode = track->GetDynamicParticle()->GetPDGcode();
    const auto isPhoton = (PDGCode == 22);

    const auto* preStep = step->GetPreStepPoint();
    const auto* postStep = step->GetPostStepPoint();

    const auto pos = isPhoton ? postStep->GetPosition() : (preStep->GetPosition() + postStep->GetPosition()) / 2;
    const auto time = isPhoton ? postStep->GetGlobalTime() : (preStep->GetGlobalTime() + postStep->GetGlobalTime()) / 2;

    const auto edep = step->GetTotalEnergyDeposit();

    // Code from Allpix-Squared
    // Calculate number of electron hole pairs produced, taking into account fluctuations between ionization and lattice
    // excitations via the Fano factor. We assume Gaussian statistics here.
    const auto meanCharge = edep / chargeCreationEnergy;
    std::normal_distribution<G4double> chargeFluctuation(meanCharge, std::sqrt(meanCharge * fanoFactor));
    const auto charge = static_cast<G4int>(chargeFluctuation(fRandomGenerator));

    // Add new deposit if the charge is more than zero
    if(charge == 0) {
        return false;
    }

    auto* hit = new DetectorHit(
        edep,
        time,
        pos.x(),
        pos.y(),
        pos.z(),
        PDGCode,
        track->GetTrackID(),
        track->GetParentID(),
        track->GetKineticEnergy());

    fHitsCollection->insert(hit);

    return true;
}
