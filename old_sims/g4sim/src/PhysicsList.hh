#pragma once
#include <G4VModularPhysicsList.hh>

#include <G4Types.hh>


// Physics List based FTFP_BERT, with:
// * Livermore as EM Physics
// * Radioactive Decay added
// * Ion Elastic scattering added
// * Step limiter added
// * zero cut value

class PhysicsList : public G4VModularPhysicsList
{
    public:
        PhysicsList(G4int ver = 0);
        virtual ~PhysicsList();

        PhysicsList(const PhysicsList&) = delete;
        PhysicsList& operator=(const PhysicsList&) = delete;
};
