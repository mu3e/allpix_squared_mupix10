#pragma once

#include <chrono>
#include <string>


class RunTimer
{
    public:
        RunTimer();
        virtual ~RunTimer();

    private:
        std::chrono::time_point<std::chrono::system_clock> fStartTime;
        std::chrono::time_point<std::chrono::system_clock> fStopTime;

    public:
        void StartTimer();
        void StopTimer();
        const std::string GetPrettyDuration() const;
};
