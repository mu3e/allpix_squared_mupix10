#include <G4Types.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>


// unchaning values
constexpr G4double worldSizeXY = 7. * mm;
constexpr G4double worldSizeZ = 46. * mm;
constexpr G4double detSizeX = 6. * mm;  // real value: 20.48 mm
constexpr G4double detSizeY = 6. * mm;  // real value: 20 mm
constexpr G4double detSizeZ = 50 * um;
constexpr G4double detPosZ = 22. * mm;
constexpr G4double sensorSizeZ = 35. * um;
const G4String detectorName = "detector";
const G4String hcName = "hc";

// default values
constexpr G4double stdAirTemp = STP_Temperature;
constexpr G4double stdAirPres = STP_Pressure;
constexpr G4double stdAirHumi = 0.;
constexpr G4double stdDetDist = 30. * mm;
constexpr G4double stdColLength = 25. * mm;
constexpr G4double stdColSidelength = 2. * mm;
constexpr G4double stdColDetDist = 0. * mm;
