#pragma once
#include <G4UImessenger.hh>

#include <G4UIcommand.hh>
#include <G4String.hh>
#include <G4UIdirectory.hh>
#include <G4UIcmdWithADoubleAndUnit.hh>
#include <G4UIcmdWithADouble.hh>

class DetectorConstruction;


class DetectorMessenger : public G4UImessenger
{
    public:
        DetectorMessenger(DetectorConstruction* detectorConstruction);
        virtual ~DetectorMessenger();

    public:
        void SetNewValue(G4UIcommand* cmd, G4String val);

    private:
        DetectorConstruction* fDetectorConstruction;
        G4UIdirectory* fDirSetup;
        G4UIdirectory* fDirAir;
        G4UIdirectory* fDirDet;
        G4UIdirectory* fDirCol;
        G4UIcmdWithADoubleAndUnit* fCmdAirTemp;
        G4UIcmdWithADoubleAndUnit* fCmdAirPres;
        G4UIcmdWithADouble* fCmdAirHumi;
        G4UIcmdWithADoubleAndUnit* fCmdColLength;
        G4UIcmdWithADoubleAndUnit* fCmdColSidelength;
        G4UIcmdWithADoubleAndUnit* fCmdColDetDist;
};
