#include "DetectorConstruction.hh"

#include <cmath>
#include <sstream>

#include <G4Material.hh>
#include <G4VSolid.hh>
#include <G4NistManager.hh>
#include <G4ios.hh>
#include <G4Box.hh>
#include <G4LogicalVolume.hh>
#include <G4Transform3D.hh>
#include <G4RotationMatrix.hh>
#include <G4ThreeVector.hh>
#include <G4PVPlacement.hh>
#include <G4VisAttributes.hh>
#include <G4UserLimits.hh>
#include <G4SDManager.hh>
#include <G4RunManager.hh>
#include <G4ExtrudedSolid.hh>
#include <G4SubtractionSolid.hh>

#include "DetectorGlobals.hh"
#include "DetectorMessenger.hh"
#include "PhysicsGlobals.hh"
#include "SensitiveDetector.hh"

G4Material* BuildHumidAir(G4double temp, G4double pres, G4double humi);
G4VSolid* BuildCollimator(G4double length, G4double sidelength);


DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fDetectorMessenger(nullptr),
    fAirTemp(stdAirTemp),
    fAirPres(stdAirPres),
    fAirHumi(stdAirHumi),
    fColLength(stdColLength),
    fColSidelength(stdColSidelength),
    fColDetDist(stdColDetDist)
{
    fDetectorMessenger = new DetectorMessenger(this);
}

DetectorConstruction::~DetectorConstruction()
{
    delete fDetectorMessenger;
}

G4VPhysicalVolume* DetectorConstruction::Construct()
{
    auto* NISTManager = G4NistManager::Instance();
    auto* worldMat = NISTManager->FindOrBuildMaterial("G4_AIR");
    auto* detectorMat = NISTManager->FindOrBuildMaterial("G4_Galactic");
    auto* sensorMat = NISTManager->FindOrBuildMaterial("G4_Si");
    auto* chipMat = NISTManager->FindOrBuildMaterial("G4_Al");
    auto* collimatorMat = NISTManager->FindOrBuildMaterial("G4_Al");
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;
    worldMat = BuildHumidAir(fAirTemp, fAirPres, fAirHumi);

    auto* worldSV = new G4Box(
        "worldSV",
        0.5 * worldSizeXY,
        0.5 * worldSizeXY,
        0.5 * worldSizeZ);
    auto* worldLV = new G4LogicalVolume(
        worldSV,
        worldMat,
        "worldLV");
    auto worldTransf = G4Transform3D(
        G4RotationMatrix(),
        G4ThreeVector());
    fWorldPV = new G4PVPlacement(
        worldTransf,
        "worldPV",
        worldLV,
        nullptr,
        false,
        0,
        false);
    auto* worldUserLimits = new G4UserLimits();
    worldUserLimits->SetUserMaxTime(cutoffTime);
    worldLV->SetUserLimits(worldUserLimits);

    auto* detectorSV = new G4Box(
        "detectorSV",
        0.5 * detSizeX,
        0.5 * detSizeY,
        0.5 * detSizeZ);
    auto* detectorLV = new G4LogicalVolume(
        detectorSV,
        detectorMat,
        "detectorLV");
    auto detectorTransf = G4Transform3D(
        G4RotationMatrix(),
        G4ThreeVector(0., 0., detPosZ));
    auto* detectorPV = new G4PVPlacement(
        detectorTransf,
        "detectorPV",
        detectorLV,
        fWorldPV,
        false,
        0,
        false);
    auto detectorVisAttr = G4VisAttributes();
    detectorVisAttr.SetVisibility(false);
    detectorLV->SetVisAttributes(detectorVisAttr);

    auto* sensorSV = new G4Box(
        "sensorSV",
        0.5 * detSizeX,
        0.5 * detSizeY,
        0.5 * sensorSizeZ);
    auto* sensorLV = new G4LogicalVolume(
        sensorSV,
        sensorMat,
        "sensorLV");
    auto sensorTransf = G4Transform3D(
        G4RotationMatrix(),
        G4ThreeVector(0., 0., -0.5 * (detSizeZ - sensorSizeZ)));
    new G4PVPlacement(
        sensorTransf,
        "sensorPV",
        sensorLV,
        detectorPV,
        false,
        0,
        false);
    auto* sensorUserLimits = new G4UserLimits();
    sensorUserLimits->SetMaxAllowedStep(maxStepSize);
    sensorLV->SetUserLimits(sensorUserLimits);
    auto sensorVisAttr = G4VisAttributes();
    sensorVisAttr.SetColor(G4Color(0.933, 0.498, 0.141, 0.9));
    sensorVisAttr.SetForceSolid();
    sensorLV->SetVisAttributes(sensorVisAttr);

    auto* chipSV = new G4Box(
        "chipSV",
        0.5 * detSizeX,
        0.5 * detSizeY,
        0.5 * (detSizeZ - sensorSizeZ));
    auto* chipLV = new G4LogicalVolume(
        chipSV,
        chipMat,
        "chipLV");
    auto chipTransf = G4Transform3D(
        G4RotationMatrix(),
        G4ThreeVector(0., 0., 0.5 * sensorSizeZ));
    new G4PVPlacement(
        chipTransf,
        "chipPV",
        chipLV,
        detectorPV,
        false,
        0,
        false);
    chipLV->SetUserLimits(sensorUserLimits);
    chipLV->SetVisAttributes(sensorVisAttr);

    auto* collimatorSV = BuildCollimator(
        fColLength,
        fColSidelength);
    auto* collimatorLV = new G4LogicalVolume(
        collimatorSV,
        collimatorMat,
        "collimatorLV");
    auto collimatorTransf = G4Transform3D(
        G4RotationMatrix(),
        GetColPos());
    fCollimatorPV = new G4PVPlacement(
        collimatorTransf,
        "collimatorPV",
        collimatorLV,
        fWorldPV,
        false,
        0,
        false);
    auto collimatorVisAttr = G4VisAttributes();
    collimatorVisAttr.SetColor(G4Color(0.542, 0.565, 0.588, 0.7));
    collimatorVisAttr.SetForceSolid();
    collimatorLV->SetVisAttributes(collimatorVisAttr);

    return fWorldPV;
}

void DetectorConstruction::ConstructSDandField()
{
    auto* SDManager = G4SDManager::GetSDMpointer();

    auto* sensitiveDetector = new SensitiveDetector();
    SDManager->AddNewDetector(sensitiveDetector);
    SetSensitiveDetector("sensorLV", sensitiveDetector);
}

void DetectorConstruction::SetAirTemp(G4double temp)
{
    fAirTemp = temp;
    UpdateAirMaterial();
}

void DetectorConstruction::SetAirPres(G4double pres)
{
    fAirPres = pres;
    UpdateAirMaterial();
}

void DetectorConstruction::SetAirHumi(G4double humi)
{
    fAirHumi = humi;
    UpdateAirMaterial();
}

void DetectorConstruction::SetColLength(G4double length)
{
    fColLength = length;
    fCollimatorPV->SetTranslation(GetColPos());
    UpdateColGeometry();
}

void DetectorConstruction::SetColSidelength(G4double sidelength)
{
    fColSidelength = sidelength;
    UpdateColGeometry();
}

void DetectorConstruction::SetColDetDist(G4double dist)
{
    fColDetDist = dist;
    fCollimatorPV->SetTranslation(GetColPos());
    NotifyUpdatedGeometry();
}

G4ThreeVector DetectorConstruction::GetColPos() const
{
    return G4ThreeVector(0., 0., detPosZ - (fColDetDist + 0.5 * (fColLength - detSizeZ)));
}

void DetectorConstruction::UpdateAirMaterial()
{
    auto* newMat = BuildHumidAir(fAirTemp, fAirPres, fAirHumi);
    fWorldPV->GetLogicalVolume()->SetMaterial(newMat);
    NotifyUpdatedGeometry();
}

void DetectorConstruction::UpdateColGeometry()
{
    auto* newCollimatorSV = BuildCollimator(fColLength, fColSidelength);
    fCollimatorPV->GetLogicalVolume()->SetSolid(newCollimatorSV);
    NotifyUpdatedGeometry();
}

void DetectorConstruction::NotifyUpdatedGeometry()
{
    // tell G4RunManager that we changed the geometry
    G4RunManager* runManager = G4RunManager::GetRunManager();
    runManager->GeometryHasBeenModified();
    runManager->Initialize();
}

G4Material* BuildHumidAir(G4double temp, G4double pres, G4double humi)
{
    auto* NISTManager = G4NistManager::Instance();

    std::ostringstream newMatName;
    newMatName << "AIR_" << temp << "_" << pres << "_" << humi;
    auto newMatNameStr = newMatName.str();

    G4Material* newMat = nullptr;
    newMat = NISTManager->FindMaterial(newMatNameStr);

    if ((void*)newMat == nullptr)
    {
        // TODO: add water vapor, for now only air
        newMat = NISTManager->ConstructNewGasMaterial(
            newMatNameStr,
            "G4_AIR",
            temp,
            pres,
            true);
    }

    NISTManager->PrintG4Material(newMatNameStr);

    return newMat;
}

G4VSolid* BuildCollimator(G4double length, G4double sidelength)
{
    const G4double y_far = 0.5 * sidelength;
    const G4double y_middle = sidelength;
    const G4double x_far = 0.5 * sqrt(3.) * sidelength;
    const G4double x_middle = 0.;

    auto* collimatorPolygon = new std::vector<G4TwoVector>();
    collimatorPolygon->reserve(6);

    // inner hexagon, anti-clockwise, beginning at upper left corner
    collimatorPolygon->emplace_back(G4TwoVector(x_far, y_far));
    collimatorPolygon->emplace_back(G4TwoVector(x_middle, y_middle));
    collimatorPolygon->emplace_back(G4TwoVector(-x_far, y_far));
    collimatorPolygon->emplace_back(G4TwoVector(-x_far, -y_far));
    collimatorPolygon->emplace_back(G4TwoVector(x_middle, -y_middle));
    collimatorPolygon->emplace_back(G4TwoVector(x_far, -y_far));

    // length needs to be a bit larger than in theory to avoid floating point errors
    auto* collimatorInnerHexagonSV = new G4ExtrudedSolid(
        "collmatorInnerHexagonSV",
        *collimatorPolygon,
        0.51 * length,
        G4TwoVector(),
        1.,
        G4TwoVector(),
        1.);

    auto* collimatorOutsideBoxSV = new G4Box(
        "collimatorOutsideBoxSV",
        0.5 * detSizeX,
        0.5 * detSizeY,
        0.5 * length);

    // substract hexagon from box
    auto* collimatorSV = new G4SubtractionSolid(
        "collimatorSV",
        collimatorOutsideBoxSV,
        collimatorInnerHexagonSV);

    return collimatorSV;
}
