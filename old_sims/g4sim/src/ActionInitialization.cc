#include "ActionInitialization.hh"

#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "EventAction.hh"


ActionInitialization::ActionInitialization()
  : G4VUserActionInitialization()
{}

ActionInitialization::~ActionInitialization()
{}

void ActionInitialization::BuildForMaster() const
{
    auto* runAction = new RunAction();
    SetUserAction(runAction);
}

void ActionInitialization::Build() const
{
    auto* primaryGeneratorAction = new PrimaryGeneratorAction();
    SetUserAction(primaryGeneratorAction);

    auto* runAction = new RunAction();
    SetUserAction(runAction);

    auto* eventAction = new EventAction();
    SetUserAction(eventAction);
}
