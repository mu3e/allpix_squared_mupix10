#include <G4Types.hh>
#include <G4SystemOfUnits.hh>

constexpr G4double cutoffTime = 10. * ns;
constexpr G4double maxStepSize = 0.1 * um;
constexpr G4double chargeCreationEnergy = 3.6 * eV;
constexpr G4double fanoFactor = 0.115;
