#include "EventAction.hh"

#include <G4RootAnalysisManager.hh>
#include <G4Event.hh>
#include <G4HCofThisEvent.hh>

#include "DetectorHit.hh"
#include "DetectorGlobals.hh"


EventAction::EventAction()
  : G4UserEventAction(),
    fAnalysisManager(nullptr)
{
    fAnalysisManager = G4RootAnalysisManager::Instance();
}

EventAction::~EventAction()
{}

void EventAction::EndOfEventAction(const G4Event* event)
{
    const auto* hitsCollection = static_cast<HitsCollection*>(event->GetHCofThisEvent()->GetHC(0));
    const auto n_hits = hitsCollection->entries();
    const auto eventID = event->GetEventID();

    for (size_t n = 0; n < n_hits; n++) {
        const auto* hit = static_cast<DetectorHit*>(hitsCollection->GetHit(n));
        fAnalysisManager->FillNtupleIColumn(0, 0, eventID);
        fAnalysisManager->FillNtupleDColumn(0, 1, hit->GetEnergyDeposit());
        fAnalysisManager->FillNtupleDColumn(0, 2, hit->GetGlobalTime());
        fAnalysisManager->FillNtupleDColumn(0, 3, hit->GetPositionX());
        fAnalysisManager->FillNtupleDColumn(0, 4, hit->GetPositionY());
        fAnalysisManager->FillNtupleDColumn(0, 5, hit->GetPositionZ());
        fAnalysisManager->FillNtupleSColumn(0, 6, detectorName);
        fAnalysisManager->FillNtupleIColumn(0, 7, hit->GetPDGCode());
        fAnalysisManager->FillNtupleIColumn(0, 8, hit->GetTrackID());
        fAnalysisManager->FillNtupleIColumn(0, 9, hit->GetParentID());
        fAnalysisManager->FillNtupleDColumn(0, 10, hit->GetKineticEnergy());
        fAnalysisManager->AddNtupleRow(0);
    }
}
