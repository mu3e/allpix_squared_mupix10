#include "DetectorHit.hh"


DetectorHit::DetectorHit()
  : G4VHit(),
    fEnergy(0.),
    fTime(0.),
    fPosX(0.),
    fPosY(0.),
    fPosZ(0.),
    fPDGCode(0),
    fTrackID(0),
    fParentID(0),
    fKinEnergy(0.)
{}

DetectorHit::DetectorHit(
    G4double energy,
    G4double time,
    G4double positionX,
    G4double positionY,
    G4double positionZ,
    G4int PDGCode,
    G4int trackID,
    G4int parentID,
    G4double kineticEnergy)
  : G4VHit(),
    fEnergy(energy),
    fTime(time),
    fPosX(positionX),
    fPosY(positionY),
    fPosZ(positionZ),
    fPDGCode(PDGCode),
    fTrackID(trackID),
    fParentID(parentID),
    fKinEnergy(kineticEnergy)
{}

DetectorHit::~DetectorHit()
{}

void DetectorHit::SetEnergyDeposit(G4double energy)
{
    fEnergy = energy;
}

void DetectorHit::SetGlobalTime(G4double time)
{
    fTime = time;
}

void DetectorHit::SetPositionX(G4double positionX)
{
    fPosX = positionX;
}

void DetectorHit::SetPositionY(G4double positionY)
{
    fPosY = positionY;
}

void DetectorHit::SetPositionZ(G4double positionZ)
{
    fPosZ = positionZ;
}

void DetectorHit::SetPDGCode(G4int PDGCode)
{
    fPDGCode = PDGCode;
}

void DetectorHit::SetTrackID(G4int trackID)
{
    fTrackID = trackID;
}

void DetectorHit::SetParentID(G4int parentID)
{
    fParentID = parentID;
}

void DetectorHit::SetKineticEnergy(G4double kineticEnergy)
{
    fKinEnergy = kineticEnergy;
}

G4double DetectorHit::GetEnergyDeposit() const
{
    return fEnergy;
}

G4double DetectorHit::GetGlobalTime() const
{
    return fTime;
}

G4double DetectorHit::GetPositionX() const
{
    return fPosX;
}

G4double DetectorHit::GetPositionY() const
{
    return fPosY;
}

G4double DetectorHit::GetPositionZ() const
{
    return fPosZ;
}

G4int DetectorHit::GetPDGCode() const
{
    return fPDGCode;
}

G4int DetectorHit::GetTrackID() const
{
    return fTrackID;
}

G4int DetectorHit::GetParentID() const
{
    return fParentID;
}

G4double DetectorHit::GetKineticEnergy() const
{
    return fKinEnergy;
}

G4ThreadLocal G4Allocator<DetectorHit>* DetectorHitAllocator = nullptr;
