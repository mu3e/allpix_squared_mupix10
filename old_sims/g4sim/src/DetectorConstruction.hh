#pragma once
#include <G4VUserDetectorConstruction.hh>

#include <G4VPhysicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4VSolid.hh>
#include <G4Material.hh>
#include <G4Types.hh>
#include <G4ThreeVector.hh>

class DetectorMessenger;


class DetectorConstruction : public G4VUserDetectorConstruction
{
    public:
        DetectorConstruction();
        virtual ~DetectorConstruction();

    public:
        virtual G4VPhysicalVolume* Construct();
        virtual void ConstructSDandField();

    public:
        void SetAirTemp(G4double temp);
        void SetAirPres(G4double pres);
        void SetAirHumi(G4double humi);
        void SetDetStepSize(G4double stepSize);
        void SetColLength(G4double length);
        void SetColSidelength(G4double sidelength);
        void SetColDetDist(G4double dist);

    private:
        DetectorMessenger* fDetectorMessenger;
        G4PVPlacement* fWorldPV;
        G4PVPlacement* fCollimatorPV;
        G4double fAirTemp;
        G4double fAirPres;
        G4double fAirHumi;
        G4double fColLength;
        G4double fColSidelength;
        G4double fColDetDist;
        G4ThreeVector GetColPos() const;
        void UpdateAirMaterial();
        void UpdateColGeometry();
        void NotifyUpdatedGeometry();
};
