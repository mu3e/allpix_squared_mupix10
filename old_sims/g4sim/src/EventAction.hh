#pragma once
#include <G4UserEventAction.hh>

#include <G4Event.hh>
#include <G4RootAnalysisManager.hh>


class EventAction : public G4UserEventAction
{
    public:
        EventAction();
        virtual ~EventAction();

    public:
        virtual void EndOfEventAction(const G4Event* event);

    private:
        G4RootAnalysisManager* fAnalysisManager;
};
