#include "DetectorMessenger.hh"

#include <G4UIcmdWithADoubleAndUnit.hh>
#include <G4PhysicalConstants.hh>
#include <G4UIcmdWithADouble.hh>
#include <G4UIcommand.hh>
#include <G4String.hh>

#include "DetectorConstruction.hh"


DetectorMessenger::DetectorMessenger(DetectorConstruction* detectorConstruction)
  : G4UImessenger(),
    fDetectorConstruction(detectorConstruction),
    fDirSetup(nullptr),
    fDirAir(nullptr),
    fDirCol(nullptr),
    fCmdAirTemp(nullptr),
    fCmdAirPres(nullptr),
    fCmdAirHumi(nullptr),
    fCmdColLength(nullptr),
    fCmdColSidelength(nullptr),
    fCmdColDetDist(nullptr)
{
    fDirSetup = new G4UIdirectory("/setup/");
    fDirSetup->SetGuidance("Commands regarding the experimental setup.");

    fDirAir = new G4UIdirectory("/setup/air/");
    fDirAir->SetGuidance("Commands regarding the air.");

    fDirCol = new G4UIdirectory("/setup/detector/");
    fDirCol->SetGuidance("Commands regarding the detector.");

    fDirCol = new G4UIdirectory("/setup/collimator/");
    fDirCol->SetGuidance("Commands regarding the collimator.");

    fCmdAirTemp = new G4UIcmdWithADoubleAndUnit(
        "/setup/air/setTemperature",
        this);
    fCmdAirTemp->SetGuidance("Set the temperature of the air in Kelvin.");
    fCmdAirTemp->SetParameterName("temp", false);
    fCmdAirTemp->SetRange("temp > 0");
    fCmdAirTemp->SetUnitCategory("Temperature");
    fCmdAirTemp->SetDefaultUnit("K");
    fCmdAirTemp->SetUnitCandidates("K");

    fCmdAirPres = new G4UIcmdWithADoubleAndUnit(
        "/setup/air/setPressure",
        this);
    fCmdAirPres->SetGuidance("Set the pressure of the air.");
    fCmdAirPres->SetParameterName("pres", false);
    fCmdAirPres->SetRange("pres > 0");
    fCmdAirPres->SetUnitCategory("Pressure");
    fCmdAirPres->SetDefaultUnit("Pa");
    fCmdAirPres->SetUnitCandidates("Pa bar atm");

    fCmdAirHumi = new G4UIcmdWithADouble(
        "/setup/air/setHumidity",
        this);
    fCmdAirHumi->SetGuidance("Set the relative humidity of the air.");
    fCmdAirHumi->SetParameterName("humi", false);
    fCmdAirHumi->SetRange("humi >= 0 && humi <= 1");

    fCmdColLength = new G4UIcmdWithADoubleAndUnit(
        "/setup/collimator/setLength",
        this);
    fCmdColLength->SetGuidance("Set the length of the collimator.");
    fCmdColLength->SetParameterName("length", false);
    fCmdColLength->SetRange("length > 0");
    fCmdColLength->SetUnitCategory("Length");
    fCmdColLength->SetDefaultUnit("mm");

    fCmdColSidelength = new G4UIcmdWithADoubleAndUnit(
        "/setup/collimator/setSidelength",
        this);
    fCmdColSidelength->SetGuidance("Set the sidelength of the hexagons in the collimator.");
    fCmdColSidelength->SetParameterName("radius", false);
    fCmdColSidelength->SetRange("radius > 0");
    fCmdColSidelength->SetUnitCategory("Length");
    fCmdColSidelength->SetDefaultUnit("mm");

    fCmdColDetDist = new G4UIcmdWithADoubleAndUnit(
        "/setup/collimator/setDetectorDistance",
        this);
    fCmdColDetDist->SetGuidance("Set the distance between the end of the collimator and the front of the detector");
    fCmdColDetDist->SetParameterName("dist", false);
    fCmdColDetDist->SetRange("dist >= 0");
    fCmdColDetDist->SetUnitCategory("Length");
    fCmdColDetDist->SetDefaultUnit("mm");
}

DetectorMessenger::~DetectorMessenger()
{
    delete fDirSetup;
    delete fDirAir;
    delete fDirDet;
    delete fDirCol;
    delete fCmdAirTemp;
    delete fCmdAirPres;
    delete fCmdAirHumi;
    delete fCmdColLength;
    delete fCmdColSidelength;
    delete fCmdColDetDist;
}

void DetectorMessenger::SetNewValue(G4UIcommand* cmd, G4String val)
{
    if (cmd == fCmdAirTemp)
    {
        fDetectorConstruction->SetAirTemp(fCmdAirTemp->GetNewDoubleValue(val));
    }
    else if (cmd == fCmdAirPres)
    {
        fDetectorConstruction->SetAirPres(fCmdAirPres->GetNewDoubleValue(val));
    }
    else if (cmd == fCmdAirHumi)
    {
        fDetectorConstruction->SetAirHumi(fCmdAirHumi->GetNewDoubleValue(val));
    }
    else if (cmd == fCmdColLength)
    {
        fDetectorConstruction->SetColLength(fCmdColLength->GetNewDoubleValue(val));
    }
    else if (cmd == fCmdColSidelength)
    {
        fDetectorConstruction->SetColSidelength(fCmdColSidelength->GetNewDoubleValue(val));
    }
    else if (cmd == fCmdColDetDist)
    {
        fDetectorConstruction->SetColDetDist(fCmdColDetDist->GetNewDoubleValue(val));
    }
}
