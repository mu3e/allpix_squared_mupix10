#include "PrimaryGeneratorAction.hh"

#include <G4GeneralParticleSource.hh>
#include <G4SingleParticleSource.hh>
#include <G4SystemOfUnits.hh>
#include <G4SPSEneDistribution.hh>
#include <G4SPSPosDistribution.hh>
#include <G4SPSAngDistribution.hh>
#include <G4ThreeVector.hh>
#include <G4IonTable.hh>
#include <G4Geantino.hh>
#include <G4Event.hh>

#include "GeneratorMessenger.hh"
#include "DetectorGlobals.hh"


PrimaryGeneratorAction::PrimaryGeneratorAction()
  : G4VUserPrimaryGeneratorAction(),
    fGeneratorMessenger(nullptr),
    fGenericParticleSource(nullptr),
    fIonTable(nullptr),
    fGeantino(nullptr),
    fIon(nullptr)
{
    fGeneratorMessenger = new GeneratorMessenger(this);

    fGenericParticleSource = new G4GeneralParticleSource();

    auto* singleParticleSource = fGenericParticleSource->GetCurrentSource();
    auto* energyDistribution = singleParticleSource->GetEneDist();
    auto* positionDistribution = singleParticleSource->GetPosDist();
    auto* angleDistribution = singleParticleSource->GetAngDist();

    singleParticleSource->SetNumberOfParticles(1);
    singleParticleSource->SetParticleTime(0.);
    singleParticleSource->SetParticleCharge(0. * eplus);

    energyDistribution->SetEnergyDisType("Mono");
    energyDistribution->SetMonoEnergy(0. * eV);

    positionDistribution->SetPosDisType("Point");
    positionDistribution->SetCentreCoords(G4ThreeVector(0., 0., detPosZ - (stdDetDist + 0.5 * detSizeZ)));

    angleDistribution->SetAngDistType("iso");

    // Ions can't be set during initilization
    fIonTable = G4IonTable::GetIonTable();
    fGeantino = G4Geantino::Geantino();
    fIon = fGeantino;
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
    delete fGeneratorMessenger;
    delete fGenericParticleSource;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* event)
{
    if (fIon == fGeantino)
    {
        fIon = fIonTable->GetIon(95, 241);
        fIon->SetPDGLifeTime(0.);
        fGenericParticleSource->SetParticleDefinition(fIon);
    }
    fGenericParticleSource->GeneratePrimaryVertex(event);
}

void PrimaryGeneratorAction::SetPositionZ(G4double dist)
{
    auto* positionDistribution = fGenericParticleSource->GetCurrentSource()->GetPosDist();
    positionDistribution->SetCentreCoords(G4ThreeVector(0., 0., detPosZ - (dist + 0.5 * detSizeZ)));
}
