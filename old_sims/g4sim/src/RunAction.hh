#pragma once
#include <G4UserRunAction.hh>

#include <G4Run.hh>
#include <G4RootAnalysisManager.hh>

#include "RunTimer.hh"


class RunAction : public G4UserRunAction
{
    public:
        RunAction();
        virtual ~RunAction();

    public:
        virtual void BeginOfRunAction(const G4Run* run);
        virtual void EndOfRunAction(const G4Run* run);

    private:
        G4RootAnalysisManager* fAnalysisManager;
        RunTimer* fRunTimer;
};
