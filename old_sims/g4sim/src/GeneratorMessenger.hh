#pragma once
#include <G4UImessenger.hh>

#include <G4UIcommand.hh>
#include <G4String.hh>
#include <G4UIdirectory.hh>
#include <G4UIcmdWithADoubleAndUnit.hh>

class PrimaryGeneratorAction;


class GeneratorMessenger : public G4UImessenger
{
    public:
        GeneratorMessenger(PrimaryGeneratorAction* primaryGeneratorAction);
        virtual ~GeneratorMessenger();

    public:
        void SetNewValue(G4UIcommand* cmd, G4String val);

    private:
        PrimaryGeneratorAction* fPrimaryGeneratorAction;
        G4UIdirectory* fDirSource;
        G4UIcmdWithADoubleAndUnit* fCmdSourceDist;
};
