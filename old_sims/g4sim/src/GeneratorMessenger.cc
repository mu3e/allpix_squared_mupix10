#include "GeneratorMessenger.hh"

#include "PrimaryGeneratorAction.hh"


GeneratorMessenger::GeneratorMessenger(PrimaryGeneratorAction* primaryGeneratorAction)
  : G4UImessenger(),
    fPrimaryGeneratorAction(primaryGeneratorAction),
    fDirSource(nullptr),
    fCmdSourceDist(nullptr)
{
    fDirSource = new G4UIdirectory("/setup/source/");
    fDirSource->SetGuidance("Commands regarding the source.");

    fCmdSourceDist = new G4UIcmdWithADoubleAndUnit(
        "/setup/source/setDistance",
        this);
    fCmdSourceDist->SetGuidance("Set the distance between the source and the detector.");
    fCmdSourceDist->SetParameterName("dist", false);
    fCmdSourceDist->SetRange("dist > 0");
    fCmdSourceDist->SetUnitCategory("Length");
    fCmdSourceDist->SetDefaultUnit("mm");
}

GeneratorMessenger::~GeneratorMessenger()
{
    delete fCmdSourceDist;
}

void GeneratorMessenger::SetNewValue(G4UIcommand* cmd, G4String val)
{
if (cmd == fCmdSourceDist)
    {
        fPrimaryGeneratorAction->SetPositionZ(fCmdSourceDist->GetNewDoubleValue(val));
    }
}
