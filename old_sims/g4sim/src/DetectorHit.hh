#pragma once
#include <G4VHit.hh>

#include <G4Types.hh>
#include <G4THitsCollection.hh>
#include <G4Allocator.hh>


class DetectorHit : public G4VHit
{
    public:
        DetectorHit();
        DetectorHit(
            G4double energy,
            G4double time,
            G4double positionX,
            G4double positionY,
            G4double positionZ,
            G4int PDGCode,
            G4int trackID,
            G4int parentID,
            G4double kineticEnergy);
        virtual ~DetectorHit();

        DetectorHit(const DetectorHit&) = delete;
        DetectorHit& operator=(const DetectorHit&) = delete;

        inline void* operator new(size_t);
        inline void operator delete(void *aHit);

    private:
        G4double fEnergy;
        G4double fTime;
        G4double fPosX;
        G4double fPosY;
        G4double fPosZ;
        G4int fPDGCode;
        G4int fTrackID;
        G4int fParentID;
        G4double fKinEnergy;

    public:
        void SetEnergyDeposit(G4double energy);
        void SetGlobalTime(G4double time);
        void SetPositionX(G4double positionX);
        void SetPositionY(G4double positionY);
        void SetPositionZ(G4double positionZ);
        void SetPDGCode(G4int PDGCode);
        void SetTrackID(G4int trackID);
        void SetParentID(G4int parentID);
        void SetKineticEnergy(G4double kineticEnergy);

    public:
        G4double GetEnergyDeposit() const;
        G4double GetGlobalTime() const;
        G4double GetPositionX() const;
        G4double GetPositionY() const;
        G4double GetPositionZ() const;
        G4int GetPDGCode() const;
        G4int GetTrackID() const;
        G4int GetParentID() const;
        G4double GetKineticEnergy() const;

};

typedef G4THitsCollection<DetectorHit> HitsCollection;

extern G4ThreadLocal G4Allocator<DetectorHit>* DetectorHitAllocator;

inline void* DetectorHit::operator new(size_t)
{
    if(!DetectorHitAllocator)
    {
        DetectorHitAllocator = new G4Allocator<DetectorHit>;
    }
    return (void*)DetectorHitAllocator->MallocSingle();
}

inline void DetectorHit::operator delete(void* aHit)
{
     DetectorHitAllocator->FreeSingle((DetectorHit*)aHit);
}
