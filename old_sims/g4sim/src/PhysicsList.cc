#include "PhysicsList.hh"

#include <G4EmLivermorePhysics.hh>
#include <G4EmExtraPhysics.hh>
#include <G4DecayPhysics.hh>
#include <G4HadronElasticPhysics.hh>
#include <G4HadronPhysicsFTFP_BERT.hh>
#include <G4StoppingPhysics.hh>
#include <G4IonPhysics.hh>
#include <G4NeutronTrackingCut.hh>
#include <G4RadioactiveDecayPhysics.hh>
#include <G4IonElasticPhysics.hh>
#include <G4StepLimiterPhysics.hh>


PhysicsList::PhysicsList(G4int ver)
  : G4VModularPhysicsList()
{
    SetVerboseLevel(ver);

    // Zero cut value
    SetDefaultCutValue(0.);

     // EM Physics
    RegisterPhysics(new G4EmLivermorePhysics(ver));

    // Synchroton Radiation & GN Physics
    RegisterPhysics(new G4EmExtraPhysics(ver));

    // Decays
    RegisterPhysics(new G4DecayPhysics(ver));

    // Hadron Elastic scattering
    RegisterPhysics(new G4HadronElasticPhysics(ver));

    // Hadron Physics
    RegisterPhysics(new G4HadronPhysicsFTFP_BERT(ver));

    // Stopping Physics
    RegisterPhysics( new G4StoppingPhysics(ver) );

    // Ion Physics
    RegisterPhysics(new G4IonPhysics(ver));

    // Neutron tracking cut
    RegisterPhysics(new G4NeutronTrackingCut(ver));

    // Radioactive Decay
    RegisterPhysics(new G4RadioactiveDecayPhysics(ver));

    // Ion Elastic scattering
    RegisterPhysics(new G4IonElasticPhysics(ver));

    // Step limiter
    RegisterPhysics(new G4StepLimiterPhysics(ver));
}

PhysicsList::~PhysicsList()
{}
