#include "RunTimer.hh"

#include <sstream>


RunTimer::RunTimer()
{}

RunTimer::~RunTimer()
{}

void RunTimer::StartTimer()
{
    fStartTime = std::chrono::system_clock::now();
}

void RunTimer::StopTimer()
{
    fStopTime = std::chrono::system_clock::now();
}

const std::string RunTimer::GetPrettyDuration() const
{
    std::ostringstream out;

    auto time_diff = fStopTime - fStartTime;
    std::chrono::milliseconds duration_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_diff);
    std::chrono::seconds duration_s = std::chrono::duration_cast<std::chrono::seconds>(time_diff);
    std::chrono::minutes duration_min = std::chrono::duration_cast<std::chrono::minutes>(time_diff);

    if (duration_s.count() < 10)
    {
        out.precision(3);
        out << std::fixed << 1e-3 * duration_ms.count() << " s";
    }
    else if (duration_min.count() < 2)
    {
        out.precision(1);
        out << std::fixed << 1e-3 * duration_ms.count() << " s";
    }
    else if (duration_min.count() < 15)
    {
        out << duration_min.count() << " min " << (duration_s - std::chrono::duration_cast<std::chrono::seconds>(duration_min)).count() << " s";
    }
    else
    {
        out << duration_min.count() << " min";
    }

    return out.str();
}
