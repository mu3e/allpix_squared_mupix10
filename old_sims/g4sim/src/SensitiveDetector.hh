#include <G4VSensitiveDetector.hh>

#include <random>

#include <G4String.hh>
#include <G4HCofThisEvent.hh>
#include <G4Step.hh>
#include <G4TouchableHistory.hh>

#include "DetectorHit.hh"


class SensitiveDetector : public G4VSensitiveDetector
{
    public:
        SensitiveDetector();
        virtual ~SensitiveDetector();

    public:
        virtual void Initialize(G4HCofThisEvent* hitsCollection);
        virtual G4bool ProcessHits(G4Step* step, G4TouchableHistory* history);

    private:
        HitsCollection* fHitsCollection;
        std::mt19937_64 fRandomGenerator;
};
