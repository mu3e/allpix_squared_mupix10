#include <stdlib.h>

#include <G4String.hh>
#include <G4RunManagerFactory.hh>
#include <G4UIExecutive.hh>
#include <G4UImanager.hh>
#include <G4VisExecutive.hh>

#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "ActionInitialization.hh"


int main(int argc, char** argv)
{
    // construct default run manager
    auto* runManager = G4RunManagerFactory::CreateRunManager(
        G4RunManagerType::Default,
        G4Threading::G4GetNumberOfCores()
    );

    auto* detectorConstruction = new DetectorConstruction();
    runManager->SetUserInitialization(detectorConstruction);

    auto* physicsList = new PhysicsList();
    runManager->SetUserInitialization(physicsList);

    auto* actionInitialization = new ActionInitialization();
    runManager->SetUserInitialization(actionInitialization);

    runManager->Initialize();

    // detect interactive mode
    G4UIExecutive* UIExecutive = nullptr;
    if (argc == 1) {
        UIExecutive = new G4UIExecutive(argc, argv);
    }

    auto* UImanager = G4UImanager::GetUIpointer();
    UImanager->ApplyCommand("/control/macroPath ../mac");

    auto* visManager = new G4VisExecutive();
    visManager->Initialize();

    // start session
    if (!UIExecutive) {
        // batch mode
        G4String fileName = argv[1];
        UImanager->ApplyCommand("/control/execute " + fileName);
    } else {
        // interactive mode
        UImanager->ApplyCommand("/control/execute init_vis.mac");
        UIExecutive->SessionStart();
        delete UIExecutive;
    }

    delete visManager;
    delete runManager;
    return EXIT_SUCCESS;
}
