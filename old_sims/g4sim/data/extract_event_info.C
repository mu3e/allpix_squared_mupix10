#include <iostream>

#include <TFile.h>
#include <TNtuple.h>

void extract_event_info(const char* run_file_name) {
    // read data from run
    auto* run_file = new TFile(run_file_name, "READ");
    auto* run_ntuple = (TNtuple*)run_file->Get("Deposition");

    int event;
    run_ntuple->SetBranchAddress("event", &event);

    int pdg_code;
    run_ntuple->SetBranchAddress("pdg_code", &pdg_code);

    int track_id;
    run_ntuple->SetBranchAddress("track_id", &track_id);

    double kin_energy;
    run_ntuple->SetBranchAddress("kin_energy", &kin_energy);

    // sequential event number
    int event_nr = 0;

    // store last event
    int last_event = 0;
    int last_track_id = 0;
    int last_pdg_code = 0;
    double last_kin_energy = 0;

    // new tree for output
    std::string output_file_name = run_file_name;
    output_file_name = output_file_name.substr(0, output_file_name.length() - 5);
    output_file_name += "_extracted.root";
    auto* output_file = new TFile(output_file_name.c_str(), "RECREATE");
    output_file->cd();
    auto* output_event_tree = new TTree("event_info", "Event information");

    output_event_tree->Branch("event", &event_nr);
    output_event_tree->Branch("pdg_code", &last_pdg_code);
    output_event_tree->Branch("kin_energy", &kin_energy);

    auto entries = run_ntuple->FindBranch("event")->GetEntries();
    for (int n = 0; n < entries; ++n)
    {
        run_ntuple->GetEntry(n);
        if (event != last_event)
        {
            // write event
            output_event_tree->Fill();

            if (pdg_code == 22)
            {
                std::cout << event_nr << std::endl;
            }

            // prepare new event
            event_nr++;
            last_event = event;
            last_track_id = track_id;
            last_pdg_code = pdg_code;
            last_kin_energy = kin_energy;

        }
        else if (track_id != last_track_id)
        {
            last_track_id = track_id;
            if (pdg_code != 11)
            {
                // wenn mehrere track ids im event die nicht electrons sind,
                //  dann nur neutrinos oder secondary e-
                std::cout << event << " " << track_id << " " << pdg_code << std::endl;
                // aber: ist davor immer alpha oder gamma dabei? A: einmal nicht apparently
            }
        }
        else
        {
            // same track, get max kin energy
            if (kin_energy > last_kin_energy)
            {
                last_kin_energy = kin_energy;
            }
        }
    }

    run_file->Close();
}
