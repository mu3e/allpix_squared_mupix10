#!/usr/bin/python3

import os.path
import sys

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from scipy.constants import milli, nano, elementary_charge
from scipy.optimize import curve_fit

# settings
precise = False

# matplotlib styling
rcParams['axes.grid'] = True
rcParams['axes.formatter.limits'] = (-3, 5)
rcParams['figure.figsize'] = (10, 6)

# impulse response function
def CSA(t, A, R, F):
    out = A*(1-np.exp(-t/R))-F*t
    return np.where(out < 0, 0, out)

# amplifies pulse
def Amp(pulse, CSA):
    if precise:
        # very slow, code like in Allpix-Squared
        amplified_pulse = [0. for _v in CSA]
        input_length = len(pulse)
        input_length_m1 = input_length - 1
        for k in range(len(CSA)):
            outsum = 0
            jmin = 0
            if (k >= input_length_m1):
                jmin = k - (input_length_m1)
            for i in range(jmin, k+1):
                if (k - i) < input_length:
                    outsum += pulse[k - i] * CSA[i]
            amplified_pulse[k] = outsum
        return np.array(amplified_pulse)
    else:
        # fast, ignores shape of pulse
        return CSA * np.sum(pulse)

# read data from csv (ugly but works)
t_f1, f1, t_f2, f2, t_f3, f3, t_f4, f4, t_f5, f5, t_f6, f6, t_f7, f7, \
    t_a1, a1, t_a2, a2, t_a3, a3, t_a4, a4, t_a5, a5, t_a6, a6, t_a7, a7, \
    t_v1, v1, t_v2, v2, t_v3, v3, t_v4, v4, t_v5, v5, t_v6, v6, t_v7, v7, \
    t_i1, i1, t_i2, i2, t_i3, i3, t_i4, i4, t_i5, i5, t_i6, i6, t_i7, i7 = \
    np.loadtxt(os.path.join(sys.path[0], 'CSASim.csv'), delimiter=',', skiprows=2, unpack=True,
    usecols=[n for n in range(0,4*14)],
    converters= {
        0: lambda s: float(s.strip() or 1e-4),
        1: lambda s: float(s.strip() or 0.6027),
        2: lambda s: float(s.strip() or 1e-4),
        3: lambda s: float(s.strip() or 0.6025),
        4: lambda s: float(s.strip() or 1e-4),
        5: lambda s: float(s.strip() or 0.6020),
        6: lambda s: float(s.strip() or 1e-4),
        7: lambda s: float(s.strip() or 0.6011),
        8: lambda s: float(s.strip() or 1e-4),
        9: lambda s: float(s.strip() or 0.5991),
        10: lambda s: float(s.strip() or 1e-4),
        11: lambda s: float(s.strip() or 0.5944),
        12: lambda s: float(s.strip() or 1e-4),
        13: lambda s: float(s.strip() or 0.5829),
        14: lambda s: float(s.strip() or 1e-4),
        15: lambda s: float(s.strip() or 1.2),
        16: lambda s: float(s.strip() or 1e-4),
        17: lambda s: float(s.strip() or 1.2),
        18: lambda s: float(s.strip() or 1e-4),
        19: lambda s: float(s.strip() or 1.199),
        20: lambda s: float(s.strip() or 1e-4),
        21: lambda s: float(s.strip() or 1.198),
        22: lambda s: float(s.strip() or 1e-4),
        23: lambda s: float(s.strip() or 1.196),
        24: lambda s: float(s.strip() or 1e-4),
        25: lambda s: float(s.strip() or 1.19),
        26: lambda s: float(s.strip() or 1e-4),
        27: lambda s: float(s.strip() or 1.176),
        28: lambda s: float(s.strip() or 1e-4),
        29: lambda s: float(s.strip() or 1.744),
        30: lambda s: float(s.strip() or 1e-4),
        31: lambda s: float(s.strip() or 1.744),
        32: lambda s: float(s.strip() or 1e-4),
        33: lambda s: float(s.strip() or 1.744),
        34: lambda s: float(s.strip() or 1e-4),
        35: lambda s: float(s.strip() or 1.744),
        36: lambda s: float(s.strip() or 1e-4),
        37: lambda s: float(s.strip() or 1.743),
        38: lambda s: float(s.strip() or 1e-4),
        39: lambda s: float(s.strip() or 1.743),
        40: lambda s: float(s.strip() or 1e-4),
        41: lambda s: float(s.strip() or 1.742),
        42: lambda s: float(s.strip() or 1e-4),
        43: lambda s: float(s.strip() or 0.),
        44: lambda s: float(s.strip() or 1e-4),
        45: lambda s: float(s.strip() or 0.),
        46: lambda s: float(s.strip() or 1e-4),
        47: lambda s: float(s.strip() or 0.),
        48: lambda s: float(s.strip() or 1e-4),
        49: lambda s: float(s.strip() or 0.),
        50: lambda s: float(s.strip() or 1e-4),
        51: lambda s: float(s.strip() or 0.),
        52: lambda s: float(s.strip() or 1e-4),
        53: lambda s: float(s.strip() or 0.),
        54: lambda s: float(s.strip() or 1e-4),
        55: lambda s: float(s.strip() or 0.),
    })

# remove baseline from AmpOut
o1 = a1 - a1[0]
o2 = a2 - a2[0]
o3 = a3 - a3[0]
o4 = a4 - a4[0]
o5 = a5 - a5[0]
o6 = a6 - a6[0]
o7 = a7 - a7[0]

# convert current to charge
# Note: signal was a triagnle signal, so calculate by hand
q1 = 0.5 * 0.50*milli * 0.1*nano
q2 = 0.5 * 1.08*milli * 0.1*nano
q3 = 0.5 * 2.32*milli * 0.1*nano
q4 = 0.5 * 5.00*milli * 0.1*nano
q5 = 0.5 * 10.8*milli * 0.1*nano
q6 = 0.5 * 23.2*milli * 0.1*nano
q7 = 0.5 * 50.0*milli * 0.1*nano

# fit
A = 1.7e+12
R = 1.3e-07
F = 1.5e+17
p_opt, _p_cov = curve_fit(CSA, t_a3, o3 / q3, p0=(A, R, F))
print(f'A = {p_opt[0] :1.1e} V/C')
print(f'R = {p_opt[1] :1.1e} s')
print(f'F = {p_opt[2] :1.1e} V/C/s')
t = np.linspace(0, 1e-4, num=2000)

plt.figure('OutSF')
plt.xlabel('t [s]')
plt.ylabel('Source Follow Out [V]')
plt.plot(t_f1, f1, label=f'Q={q1 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_f2, f2, label=f'Q={q2 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_f3, f3, label=f'Q={q3 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_f4, f4, label=f'Q={q4 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_f5, f5, label=f'Q={q5 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_f6, f6, label=f'Q={q6 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_f7, f7, label=f'Q={q7 * 1e-3  / elementary_charge :.0f}ke')
plt.legend()

plt.figure('AmpOut')
plt.xlabel('t [s]')
plt.ylabel('Amp Out [V]')
plt.plot(t_a1, a1, label=f'Q={q1 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a2, a2, label=f'Q={q2 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a3, a3, label=f'Q={q3 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a4, a4, label=f'Q={q4 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a5, a5, label=f'Q={q5 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a6, a6, label=f'Q={q6 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a7, a7, label=f'Q={q7 * 1e-3  / elementary_charge :.0f}ke')
plt.legend()

plt.figure('Vin')
plt.xlabel('t [s]')
plt.ylabel('Vin [V]')
plt.xlim((0., 1e-7))
plt.plot(t_v1, v1, label=f'Q={q1 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_v2, v2, label=f'Q={q2 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_v3, v3, label=f'Q={q3 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_v4, v4, label=f'Q={q4 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_v5, v5, label=f'Q={q5 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_v6, v6, label=f'Q={q6 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_v7, v7, label=f'Q={q7 * 1e-3  / elementary_charge :.0f}ke')
plt.legend()

plt.figure('Iin')
plt.xlabel('t [s]')
plt.ylabel('Iin [A]')
plt.xlim((1.998e-8, 2.012e-8))
plt.plot(t_i1, i1, label=f'Q={q1 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_i2, i2, label=f'Q={q2 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_i3, i3, label=f'Q={q3 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_i4, i4, label=f'Q={q4 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_i5, i5, label=f'Q={q5 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_i6, i6, label=f'Q={q6 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_i7, i7, label=f'Q={q7 * 1e-3  / elementary_charge :.0f}ke')
plt.legend()

plt.figure('AmpOutBLZ')
plt.xlabel('t [s]')
plt.ylabel('Amp Out without Baseline [mV]')
plt.xlim((-1e-7, 2e-5))
plt.plot(t_a1, o1 / milli, label=f'Q={q1 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a2, o2 / milli, label=f'Q={q2 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a3, o3 / milli, label=f'Q={q3 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a4, o4 / milli, label=f'Q={q4 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a5, o5 / milli, label=f'Q={q5 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a6, o6 / milli, label=f'Q={q6 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a7, o7 / milli, label=f'Q={q7 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t, Amp(q3, CSA(t, *p_opt)) / milli, label='Parameterization')
plt.legend()

plt.figure('AmpPQ')
plt.xlabel('t [s]')
plt.ylabel('Amplification per Charge [mV/C]')
plt.xlim((-1e-7, 2e-5))
plt.plot(t_a1, o1 / milli / q1, label=f'Q={q1 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a2, o2 / milli / q2, label=f'Q={q2 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a3, o3 / milli / q3, label=f'Q={q3 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a4, o4 / milli / q4, label=f'Q={q4 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a5, o5 / milli / q5, label=f'Q={q5 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a6, o6 / milli / q6, label=f'Q={q6 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t_a7, o7 / milli / q7, label=f'Q={q7 * 1e-3  / elementary_charge :.0f}ke')
plt.plot(t, CSA(t, *p_opt) / milli, label='Parameterization')
plt.legend()

plt.show()
