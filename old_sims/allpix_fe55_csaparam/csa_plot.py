#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from scipy.constants import elementary_charge, milli, nano

# settings
integration_time = 5000. * nano
threshold = 40. * milli
A = 3.6e+14
R = 1.5e-07
F = 7.6e+04

# pulse
pulse_gamma = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 50, 200, 800, 300, 100, 70, 50, 30, 10, 5, ])
pulse_binning = 0.010 * nano

# matplotlib styling
rcParams['axes.grid'] = True
rcParams['axes.formatter.limits'] = (-3, 5)
rcParams['figure.figsize'] = (10, 6)

# impulse response function
def CSA(t, charge):
    out = charge * A*(1.-np.exp(-t/R)) - F*t
    return np.where(out < 0., 0., out)

# amplify pulse
def Amp(pulse):
    ntimepoints = int(np.ceil(integration_time / pulse_binning))
    amplified_pulse = np.zeros(shape=(ntimepoints))
    input_length = len(pulse)
    total_charge = np.sum(pulse)
    kmin = 0
    # TODO: update with latest MuPixDigitzer code
    for k in range(input_length):
        if pulse[k] > 0:
            kmin = k
            break
    for k in range(ntimepoints):
        if k < kmin:
            amplified_pulse[k] = 0
        else:
            amplified_pulse[k] = CSA(k * pulse_binning, total_charge)
    return amplified_pulse

# calculate ToT
def ToT(amplified_pulse):
    ts1 = 0.
    ts2 = 0.
    for index, voltage in enumerate(amplified_pulse):
        if ts1 == 0.:
            if voltage >= threshold:
                ts1 = index * pulse_binning
        else:
            if voltage <= threshold:
                ts2 = index * pulse_binning
                break
    if ts1 > 0. and ts2 == 0.:
        ts2 = integration_time
    return ts2-ts1


if __name__ == "__main__":
    t = np.array([pulse_binning * n for n in range(int(np.ceil(integration_time / pulse_binning)))])

    amplified_pulse_gamma = Amp(pulse_gamma * elementary_charge)

    plt.figure('Pulse')
    plt.title(f'Pulse')
    plt.xlabel('t [ns]')
    plt.ylabel('Charge [e]')
    plt.plot([pulse_binning / nano * n for n in range(len(pulse_gamma))], pulse_gamma, label=f'Gamma, charge={np.sum(pulse_gamma) :.0f}e')
    plt.legend()

    plt.figure('Amplified Pulse')
    plt.title(f'Amplified Pulse (threshold={threshold / milli :.0f} mV)')
    plt.xlabel('t [ns]')
    plt.ylabel('Voltage [mV]')
    plt.plot(t / nano, amplified_pulse_gamma / milli, label=f'Gamma, ToT={ToT(amplified_pulse_gamma) / nano :.0f}ns')
    plt.plot(t / nano, [threshold / milli for _t in t])
    plt.legend()

    plt.show()
