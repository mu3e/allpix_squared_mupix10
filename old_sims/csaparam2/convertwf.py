#!/usr/bin/python3

# This file converts the csv waveforms from the Tektronix DPO7254C to
# a ROOT TTree with branches for time, ampout, hitbus and injection.

#%% import and settings
import os.path
import sys
import time
import re
from array import array
from natsort import natsorted
import ROOT

# enable multi-threading in ROOT
ROOT.EnableImplicitMT()

# measure script duration
t_start = time.perf_counter()

#%% data settings
datadir_prefix = 'std'
datadirs = [
    '200mV',
    '250mV',
    '300mV',
    '400mV',
    '500mV',
    '600mV',
    '700mV',
    '800mV',
    '900mV',
    '1000mV',
    '1100mV',
    '1200mV',
    '1300mV',
    '1400mV',
]

#%% function to convert csv lines to ttree
def convert_lines_to_ttree(event_nr, lines):
    ttree = ROOT.TTree(f'{n}', f'waveforms for event {n}')
    td_t = array('f', [0.])
    td_ampout = array('f', [0.])
    td_hitbus = array('f', [0.])
    td_injection = array('f', [0.])
    ttree.Branch('t', td_t, 't/F')
    ttree.Branch('ampout', td_ampout, 'ampout/F')
    ttree.Branch('hitbus', td_hitbus, 'hitbus/F')
    ttree.Branch('injection', td_injection, 'injection/F')
    for line in lines:
        values = line.split(',')
        td_t[0] = float(values[0])
        td_ampout[0] = float(values[1])
        td_hitbus[0] = float(values[2])
        td_injection[0] = float(values[3])
        ttree.Fill()
    ttree.Write(f'{event_nr}')

#%% loop datadirs
for datadir in datadirs:
    print(f'Starting conversion for {datadir}')
    datadir_path = os.path.join(sys.path[0], 'Data', datadir_prefix, datadir)
    t_conversion_start = time.perf_counter()

    # create ROOT file
    tfile_path = os.path.join(f'{datadir_path}.root')
    tfile = ROOT.TFile(tfile_path, 'RECREATE')

    # read folder in alphanumeric order, assume only csv files
    csvfiles = natsorted(os.listdir(datadir_path))

    # check if injection waveforms are available
    events = 0
    with_injection = False
    if 'Ch1' in csvfiles[0]:
        with_injection = True

    if with_injection:
        # loop through all events
        events = len(csvfiles) // 3
        for n in range(1, events + 1):

            # get file name (works because sorted)
            file_inj = csvfiles[3 * n - 3]
            file_amp = csvfiles[3 * n - 2]
            file_hit = csvfiles[3 * n - 1]

            # open files and read them into an array
            with open(os.path.join(datadir_path, file_amp), mode='rt') as csvfile:
                lines_amp = csvfile.readlines()
            with open(os.path.join(datadir_path, file_hit), mode='rt') as csvfile:
                lines_hit = csvfile.readlines()
            with open(os.path.join(datadir_path, file_inj), mode='rt') as csvfile:
                lines_inj = csvfile.readlines()

            # convert lines
            new_lines = []
            for m in range(0, 6):
                new_line = re.sub(r'^.*,([^,]*),([^,]*)\n$', r'\g<1>,\g<2>', lines_amp[m])
                new_line += re.sub(r'^.*(,[^,]*)\n$', r'\g<1>', lines_hit[m])
                new_line += re.sub(r'^.*(,[^,]*)\n$', r'\g<1>', lines_inj[m])
                new_lines.append(new_line)
            for m in range(6, len(lines_amp)):
                new_line = re.sub(r'^.*,,,(.*)\n$', r'\g<1>', lines_amp[m])
                new_line += re.sub(r'^.*(,[^,]*)\n$', r'\g<1>', lines_hit[m])
                new_line += re.sub(r'^.*(,[^,]*)\n$', r'\g<1>', lines_inj[m])
                new_lines.append(new_line)
            convert_lines_to_ttree(n, new_lines)

    else:
        # same code but replaces injection with 0
        events = len(csvfiles) // 2
        for n in range(1, events + 1):
            file_amp = csvfiles[2 * n - 2]
            file_hit = csvfiles[2 * n - 1]
            with open(os.path.join(datadir_path, file_amp), mode='rt') as csvfile:
                lines_amp = csvfile.readlines()
            with open(os.path.join(datadir_path, file_hit), mode='rt') as csvfile:
                lines_hit = csvfile.readlines()
            new_lines = []
            for m in range(0, 6):
                new_line = re.sub(r'^.*,([^,]*),([^,]*)\n$', r'\g<1>,\g<2>', lines_amp[m])
                new_line += re.sub(r'^.*(,[^,]*)\n$', r'\g<1>', lines_hit[m])
                new_line += ',0'
                new_lines.append(new_line)
            for m in range(6, len(lines_amp)):
                new_line = re.sub(r'^.*,,,(.*)\n$', r'\g<1>', lines_amp[m])
                new_line += re.sub(r'^.*(,[^,]*)\n$', r'\g<1>', lines_hit[m])
                new_line += ',0'
                new_lines.append(new_line)
            convert_lines_to_ttree(n, new_lines)

    # close ROOT file
    tfile.Close()

    # print duration
    t_conversion_diff = time.perf_counter() - t_conversion_start
    print(f' converted {events} events in {t_conversion_diff:#.3g}s')

# print total duration
t_diff = time.perf_counter() - t_start
print(f'Finished converting {len(csvfiles)} files in {t_diff/60:#.3g}min')
