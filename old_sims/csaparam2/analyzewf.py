#!/usr/bin/python3
#
# This script performs a calibration using the Fe55 data and analyzes the
# behavior of the fit parameters as function of the injection voltage.

#%% imports and settings
import os.path
import sys
from array import array
import ROOT
import numpy as np

# enable MT for RDataFrame
ROOT.EnableImplicitMT()
# use Minuit2 for fitting
ROOT.TVirtualFitter.SetDefaultFitter('Minuit2')
# disable stat box
ROOT.gStyle.SetOptStat(False)

#%% data settings
# waveform data
datadir_prefix = 'std'
injvs = array('f', [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4])
with_fe55 = False

# chip settings
threshold = 34e-3

#%% functions for calculated quantities
# include header for Lambert W function from GSL
# TODO: use ROOT.Math.lambert_W0 and ROOT.Math.lambert_Wm1 once available
#       see https://github.com/root-project/root/pull/8496
ROOT.gInterpreter.ProcessLine('#include <gsl/gsl_sf_lambert.h>')

# function to calculate maximum from fit parameters
cxx_expr_max_calc = '\
    const Double_t RF = R * F;\
    return A - RF * (1 + log(A / RF));\
'

# function to calculate ToT from fit parameters
cxx_expr_tot_calc = f'\
    const Double_t L = {threshold};\
    if (max_calc > L) {{\
        const Double_t RF = R * F;\
        const Double_t inner = -A / RF * exp((L - A) / RF);\
        return R * (gsl_sf_lambert_W0(inner) - gsl_sf_lambert_Wm1(inner));\
    }} else {{\
        return 0.;\
    }}\
'

#%% read data
file_path = os.path.join(sys.path[0], 'Data', datadir_prefix, 'FitResults.root')
tfile_fits = ROOT.TFile(file_path, 'READ')
ttree_fitres = tfile_fits.Get('fitres')

# RDataFrame for cuts
rdf_orig = ROOT.RDataFrame(ttree_fitres)

# book useful definitions
rdf_def1 = rdf_orig.Define('max_calc', cxx_expr_max_calc)
rdf_def2 = rdf_def1.Define('tot_calc', cxx_expr_tot_calc)
rdf_def3 = rdf_def2.Define('tot_diff', 'tot_calc-tot')
rdf_def4 = rdf_def3.Define('tot_ratio', 'tot_calc/tot')

# filter entries that are on fit boundary
filterstr = f'R>{1.01 * 1e-8} && R<{0.99 * 6e-7} && F>{1.01 * 1e4} && F<{0.99 * 4e5}'
rdf_fitf = rdf_def4.Filter(filterstr)

# separate RDFs for injection and Fe55
rdf_injv = rdf_fitf.Filter('injv>0')
rdf_fe55 = rdf_fitf.Filter('injv==0')

# filter possible noise entries for Fe55 (max > 60mV)
rdf_fe55_maxf = rdf_fe55.Filter('max>0.06')

# filter events with no hitbus entries for Fe55
rdf_fe55_hitf = rdf_fe55_maxf.Filter('tot>0')

# RDF for injection and Fe55 with noise filter for Fe55
rdf_all_fe55_maxf = rdf_fitf.Filter('(injv==0 ? max>0.06 : 1)')
rdf_all_hitf_fe55_maxf = rdf_all_fe55_maxf.Filter('tot>0')

# TODO: graphs with how many entries left

#%% function to format parameter
# don't ask how it works, just accept that it does (most of the time)
def format_value_error(value, error):
    out = ''
    error = round(error, 1 - int(np.floor(np.log10(abs(error)))))
    errorstr = f'({int(error * 10**(1 - int(np.floor(np.log10(abs(error))))))})'
    expdiff = int(np.floor(np.log10(value))) - int(np.floor(np.log10(error)))
    valstr = ('{' + f':#.{expdiff+2}g' + '}').format(value)
    if '.' in valstr:
        if valstr[-1] == '.':
            valstr = valstr[:-1]
        elif '.' in valstr[-2:]:
            valstr = 'e'
            # TODO: numstr, and if so just do a dot in the error
    # PROBLEM if 1.2(10) -> only one significant digit?
    if 'e' in valstr:
        valstr = ('{' + f':#.{expdiff+1}e' + '}').format(value)
        numstr, expstr = valstr.split('e')
        expstr = expstr.strip('+')
        out = f'{numstr}{errorstr}e{expstr}'
    else:
        out = f'{valstr}{errorstr}'
    return out

#%% output file
file_path = os.path.join(sys.path[0], 'Data', datadir_prefix, 'Analysis.root')
tfile_output = ROOT.TFile(file_path, 'RECREATE')

# function to create and switch folder in output file
def root_cd(dirname = ''):
    if tfile_output.GetDirectory(dirname) == None:
        tfile_output.mkdir(dirname)
        tfile_output.cd(dirname)
    else:
        tfile_output.cd(dirname)

#%% create canvas
tcanvas = ROOT.TCanvas('tcanvas', 'tcanvas')
tcanvas.SetCanvasSize(1920, 1080)
tcanvas.SetWindowSize(1280, 720)

#%% get parameter dependency on injection voltage
root_cd('injv_fitparams')

# gaus fit function
tf1_gaus = ROOT.TF1('tf1_gaus', 'gaus')

# TODO: maybe gaus landau convolution mit RooFit? or just landau for small injvs? check davids langau code

# arrays for parameter dependencies
inj_A = array('f', [])
inj_dA = array('f', [])
inj_R = array('f', [])
inj_dR = array('f', [])
inj_F = array('f', [])
inj_dF = array('f', [])
inj_tot = array('f', [])
inj_dtot = array('f', [])

# loop injection voltages
for injv in injvs:
    # new folder for each voltage
    root_cd(f'injv_fitparams/{np.round(injv * 1e3, 0):.0f}mV')

    # book filters
    rdf_thisinjv = rdf_injv.Filter(f'injv=={injv}')
    rdf_thisinjv_hitf = rdf_thisinjv.Filter('tot>0')

    # book histograms
    th1_thisinjv_A = rdf_thisinjv.Histo1D('A')
    th1_thisinjv_R = rdf_thisinjv.Histo1D('R')
    th1_thisinjv_F = rdf_thisinjv.Histo1D('F')
    th1_thisinjv_ToT = rdf_thisinjv.Histo1D('tot')

    # book counts
    events_count = rdf_thisinjv.Count()
    events_hitf_count = rdf_thisinjv_hitf.Count()

    # fit parameter A
    max_bin = th1_thisinjv_A.GetMaximumBin()
    tf1_gaus.SetParameter(0, th1_thisinjv_A.GetBinContent(max_bin))
    tf1_gaus.SetParameter(1, th1_thisinjv_A.GetBinCenter(max_bin))
    tf1_gaus.SetParameter(2, 0.01)
    tf1_gaus.SetParLimits(0, 0.2 * tf1_gaus.GetParameter(0), 2 * tf1_gaus.GetParameter(0))
    tf1_gaus.SetParLimits(1, th1_thisinjv_A.GetBinCenter(th1_thisinjv_A.FindFirstBinAbove(1)), th1_thisinjv_A.GetBinCenter(th1_thisinjv_A.FindLastBinAbove(1)))
    tf1_gaus.SetParLimits(2, 1e-2 * tf1_gaus.GetParameter(2), 1e2 * tf1_gaus.GetParameter(2))
    th1_thisinjv_A.Fit(tf1_gaus, 'BQ')
    inj_A.append(tf1_gaus.GetParameter(1))
    inj_dA.append(tf1_gaus.GetParameter(2))
    th1_thisinjv_A.Write('A')

    # fit parameter R
    max_bin = th1_thisinjv_R.GetMaximumBin()
    tf1_gaus.SetParameter(0, th1_thisinjv_R.GetBinContent(max_bin))
    tf1_gaus.SetParameter(1, th1_thisinjv_R.GetBinCenter(max_bin))
    tf1_gaus.SetParameter(2, 1e-8)
    tf1_gaus.SetParLimits(0, 0.2 * tf1_gaus.GetParameter(0), 2 * tf1_gaus.GetParameter(0))
    tf1_gaus.SetParLimits(1, th1_thisinjv_R.GetBinCenter(th1_thisinjv_R.FindFirstBinAbove(1)), th1_thisinjv_R.GetBinCenter(th1_thisinjv_R.FindLastBinAbove(1)))
    tf1_gaus.SetParLimits(2, 1e-2 * tf1_gaus.GetParameter(2), 1e2 * tf1_gaus.GetParameter(2))
    th1_thisinjv_R.Fit(tf1_gaus, 'BQ')
    inj_R.append(tf1_gaus.GetParameter(1))
    inj_dR.append(tf1_gaus.GetParameter(2))
    th1_thisinjv_R.Write('R')

    # fit parameter F
    max_bin = th1_thisinjv_F.GetMaximumBin()
    tf1_gaus.SetParameter(0, th1_thisinjv_F.GetBinContent(max_bin))
    tf1_gaus.SetParameter(1, th1_thisinjv_F.GetBinCenter(max_bin))
    tf1_gaus.SetParameter(2, 5e3)
    tf1_gaus.SetParLimits(0, 0.2 * tf1_gaus.GetParameter(0), 2 * tf1_gaus.GetParameter(0))
    tf1_gaus.SetParLimits(1, th1_thisinjv_F.GetBinCenter(th1_thisinjv_F.FindFirstBinAbove(1)), th1_thisinjv_F.GetBinCenter(th1_thisinjv_F.FindLastBinAbove(1)))
    tf1_gaus.SetParLimits(2, 1e-2 * tf1_gaus.GetParameter(2), 1e2 * tf1_gaus.GetParameter(2))
    th1_thisinjv_F.Fit(tf1_gaus, 'BQ')
    inj_F.append(tf1_gaus.GetParameter(1))
    inj_dF.append(tf1_gaus.GetParameter(2))
    th1_thisinjv_F.Write('F')

    # hitbus tot
    # only fit if at least 67% of the events triggered the hitbus
    if events_hitf_count.GetValue() > 0.67 * events_count.GetValue():
        max_bin = th1_thisinjv_ToT.GetMaximumBin()
        tf1_gaus.SetParameter(0, th1_thisinjv_ToT.GetBinContent(max_bin))
        tf1_gaus.SetParameter(1, th1_thisinjv_ToT.GetBinCenter(max_bin))
        tf1_gaus.SetParameter(2, 1e-7)
        tf1_gaus.SetParLimits(0, 0.2 * tf1_gaus.GetParameter(0), 2 * tf1_gaus.GetParameter(0))
        tf1_gaus.SetParLimits(1, th1_thisinjv_ToT.GetBinCenter(th1_thisinjv_ToT.FindFirstBinAbove(1)), th1_thisinjv_ToT.GetBinCenter(th1_thisinjv_ToT.FindLastBinAbove(1)))
        tf1_gaus.SetParLimits(2, 1e-2 * tf1_gaus.GetParameter(2), 1e2 * tf1_gaus.GetParameter(2))
        th1_thisinjv_ToT.Fit(tf1_gaus, 'BQ')
        inj_tot.append(tf1_gaus.GetParameter(1))
        inj_dtot.append(tf1_gaus.GetParameter(2))
    else:
        inj_tot.append(0.)
        inj_dtot.append(0.)
    th1_thisinjv_ToT.Write('ToT')

    # TODO: all the other parameters as well

#%% parameters as function of the injection voltage
root_cd('injv_paramdep')

# assume no error for injection
injvs_err = array('f', [0. for _injv in injvs])

# fit parameter A
tgraph_A = ROOT.TGraphErrors(len(injvs), injvs, inj_A, injvs_err, inj_dA)
tgraph_A.SetLineWidth(2)
tgraph_A.SetTitle('Fit parameter A vs injection voltage;injection voltage [V];A [V]')
tgraph_A.Write('A')

# linear fit for A with 0.3V <= injv <= 0.8V
tf1_pol1 = ROOT.TF1('tf1_pol1', 'pol1', 0.0, 1.5)
tf1_pol1.SetParameter(0, 0.04)
tf1_pol1.SetParameter(1, 0.43)
tgraph_A.Fit(tf1_pol1, 'BQN', '', 0.25, 0.85)

# draw fit and store canvas
# TODO: draw used fit values
tf1_pol1.SetTitle('Fit parameter A vs injection voltage;injection voltage [V];A [V]')
tf1_pol1.SetLineWidth(2)
tf1_pol1.Draw()
tgraph_A.Draw('same')
pdi_tlegend_A = ROOT.TLegend(0.1, 0.7, 0.5, 0.9)
pdi_tlegend_A.AddEntry(tgraph_A, 'data from injection')
pdi_tlegend_A.AddEntry(tf1_pol1, f'linear fit, m={format_value_error(tf1_pol1.GetParameter(1),tf1_pol1.GetParError(1))} V/V')
pdi_tlegend_A.Draw()
tcanvas.Write('A_fit')

# grab fit parameters for calibration
calibration_minj = tf1_pol1.GetParameter(1)
calibration_minj_err = tf1_pol1.GetParError(1)
calibration_A0 = tf1_pol1.GetParameter(0)
calibration_A0_err = tf1_pol1.GetParError(0)

# fit parameter R
tgraph_R = ROOT.TGraphErrors(len(injvs), injvs, inj_R, injvs_err, inj_dR)
tgraph_R.SetLineWidth(2)
tgraph_R.SetTitle('Fit parameter R vs injection voltage;injection voltage [V];R [s]')
tgraph_R.Write('R')

# fit parameter F
tgraph_F = ROOT.TGraphErrors(len(injvs), injvs, inj_F, injvs_err, inj_dF)
tgraph_F.SetLineWidth(2)
tgraph_F.SetTitle('Fit parameter F vs injection voltage;injection voltage [V];F [V/s]')
tgraph_F.Write('F')

# hitbus tot
tgraph_tot = ROOT.TGraphErrors(len(injvs), injvs, inj_tot, injvs_err, inj_dtot)
tgraph_tot.SetLineWidth(2)
tgraph_tot.SetTitle('Hitbus ToT vs injection voltage;injection voltage [V];ToT [s]')
tgraph_tot.Write('ToT')

# linear fit for tot with 0.3V <= injv
tf1_pol1.SetParameter(0, 0.4e-6)
tf1_pol1.SetParameter(1, 3.9e-6)
tgraph_tot.Fit(tf1_pol1, 'BQN', '', 0.25, 1.55)

# draw fit and store canvas
# TODO: draw used fit values
tf1_pol1.SetTitle('Hitbus ToT vs injection voltage;injection voltage [V];ToT [s]')
tf1_pol1.SetLineWidth(2)
tf1_pol1.Draw()
tgraph_tot.Draw('same')
pdi_tlegend_tot = ROOT.TLegend(0.1, 0.7, 0.5, 0.9)
pdi_tlegend_tot.AddEntry(tgraph_tot, 'data from injection')
pdi_tlegend_tot.AddEntry(tf1_pol1, f'linear fit')
pdi_tlegend_tot.Draw()
tcanvas.Write('ToT_fit')

#%% fe55 evnr
if with_fe55:
    root_cd('Fe55/evnr')

    # histogram models
    th2dm_fe55_A_vs_evnr = ROOT.RDF.TH2DModel('th2dm_fe55_A_vs_evnr', 'fe55_A_vs_evnr', 200, 0, 20000, 28, 0.02, 0.30)
    th2dm_fe55_v0_vs_evnr = ROOT.RDF.TH2DModel('th2dm_fe55_v0_vs_evnr', 'fe55_v0_vs_evnr', 200, 0, 20000, 25, 1.060, 1.085)
    th1dm_fe55_evnr = ROOT.RDF.TH1DModel('th1dm_fe55_evnr', 'fe55_evnr', 200, 0, 20000)

    # book histograms
    th2_fe55_A_vs_evnr = rdf_fe55.Histo2D(th2dm_fe55_A_vs_evnr, 'evnr', 'A')
    th2_fe55_v0_vs_evnr = rdf_fe55.Histo2D(th2dm_fe55_v0_vs_evnr, 'evnr', 'v0')
    th2_fe55_maxf_A_vs_evnr = rdf_fe55_maxf.Histo2D(th2dm_fe55_A_vs_evnr, 'evnr', 'A')
    th2_fe55_maxf_v0_vs_evnr = rdf_fe55_maxf.Histo2D(th2dm_fe55_v0_vs_evnr, 'evnr', 'v0')
    th1_fe55_evnr_maxf = rdf_fe55_maxf.Histo1D(th1dm_fe55_evnr, 'evnr')
    th1_fe55_evnr_hitf = rdf_fe55_hitf.Histo1D(th1dm_fe55_evnr, 'evnr')

    # A vs event number without noise filter
    th2_fe55_A_vs_evnr.SetTitle('Fitted amplitude parameter A vs event number without noise filter;event number;A [V]')
    th2_fe55_A_vs_evnr.SetOption('colz')
    th2_fe55_A_vs_evnr.Write('A')

    # basline vs event number without noise filter
    th2_fe55_v0_vs_evnr.SetTitle('Fitted amplifier baseline vs event number without noise filter;event number;baseline [V]')
    th2_fe55_v0_vs_evnr.SetOption('colz')
    th2_fe55_v0_vs_evnr.Write('v0')

    # A vs event number with noise filter
    th2_fe55_maxf_A_vs_evnr.SetTitle('Fitted amplifier baseline vs event number with noise filter;event number;A [V]')
    th2_fe55_maxf_A_vs_evnr.SetOption('colz')
    th2_fe55_maxf_A_vs_evnr.Write('A_maxf')

    # baseline vs event number with noise filter
    th2_fe55_maxf_v0_vs_evnr.SetTitle('Fitted amplifier baseline vs event number with noise filter;event number;baseline [V]')
    th2_fe55_maxf_v0_vs_evnr.SetOption('colz')
    th2_fe55_maxf_v0_vs_evnr.Write('v0_maxf')

    # event number with noise filter
    th1_fe55_evnr_maxf.SetTitle('Event number after noise filter;event number;entries')
    th1_fe55_evnr_maxf.Write('evnr_maxf')

    # event number with hit filter
    th1_fe55_evnr_hitf.SetTitle('Event number after hit filter;event number;entries')
    th1_fe55_evnr_hitf.Write('evnr_hitf')

#%% fe55 histograms
if with_fe55:
    root_cd('Fe55')

    # book histograms
    th1_fe55_A = rdf_fe55_hitf.Histo1D('A')
    th1_fe55_A_nohitf = rdf_fe55_maxf.Histo1D('A')
    th1_fe55_A_nomaxf = rdf_fe55.Histo1D('A')
    th1_fe55_R = rdf_fe55_hitf.Histo1D('R')
    th1_fe55_F = rdf_fe55_hitf.Histo1D('F')
    th1_fe55_ToT = rdf_fe55_hitf.Histo1D('tot')
    th1_fe55_max = rdf_fe55_hitf.Histo1D('max')
    th1_fe55_max_nohitf = rdf_fe55_maxf.Histo1D('max')
    th1_fe55_max_nomaxf = rdf_fe55.Histo1D('max')

    # fit parameter A
    th1_fe55_A.SetTitle('Fitted amplitude parameter A in Fe55 measurement;A [V];entries')
    max_bin = th1_fe55_A.GetMaximumBin()
    tf1_gaus.SetParameter(0, th1_fe55_A.GetBinContent(max_bin))
    tf1_gaus.SetParameter(1, th1_fe55_A.GetBinCenter(max_bin))
    tf1_gaus.SetParameter(2, 0.01)
    tf1_gaus.SetParLimits(0, 0.2 * tf1_gaus.GetParameter(0), 2 * tf1_gaus.GetParameter(0))
    tf1_gaus.SetParLimits(1, th1_fe55_A.GetBinCenter(th1_fe55_A.FindFirstBinAbove(1)), th1_fe55_A.GetBinCenter(th1_fe55_A.FindLastBinAbove(1)))
    tf1_gaus.SetParLimits(2, 1e-2 * tf1_gaus.GetParameter(2), 1e2 * tf1_gaus.GetParameter(2))
    th1_fe55_A.Fit(tf1_gaus, 'BQ')
    fe55_A_mean = tf1_gaus.GetParameter(1)
    fe55_A_err = tf1_gaus.GetParameter(2)
    th1_fe55_A.Write('A')

    # A without hit filter
    th1_fe55_A_nohitf.SetTitle('Fitted amplitude parameter A in Fe55 measurement without hit filter;A [V];entries')
    th1_fe55_A_nohitf.Write('A_nohitf')

    # A without max filter
    th1_fe55_A_nomaxf.SetTitle('Fitted amplitude parameter A in Fe55 measurement without noise filter;A [V];entries')
    th1_fe55_A_nomaxf.Write('A_nomaxf')

    # fit parameter R
    th1_fe55_R.SetTitle('Fitted rise parameter R in Fe55 measurement;R [s];entries')
    max_bin = th1_fe55_R.GetMaximumBin()
    tf1_gaus.SetParameter(0, th1_fe55_R.GetBinContent(max_bin))
    tf1_gaus.SetParameter(1, th1_fe55_R.GetBinCenter(max_bin))
    tf1_gaus.SetParameter(2, 1e-8)
    tf1_gaus.SetParLimits(0, 0.2 * tf1_gaus.GetParameter(0), 2 * tf1_gaus.GetParameter(0))
    tf1_gaus.SetParLimits(1, th1_fe55_R.GetBinCenter(th1_fe55_R.FindFirstBinAbove(1)), th1_fe55_R.GetBinCenter(th1_fe55_R.FindLastBinAbove(1)))
    tf1_gaus.SetParLimits(2, 1e-2 * tf1_gaus.GetParameter(2), 1e2 * tf1_gaus.GetParameter(2))
    th1_fe55_R.Fit(tf1_gaus, 'BQ')
    fe55_R_mean = tf1_gaus.GetParameter(1)
    fe55_R_err = tf1_gaus.GetParameter(2)
    th1_fe55_R.Write('R')

    # fit parameter F
    th1_fe55_F.SetTitle('Fitted fall parameter F in Fe55 measurement;F [V/s];entries')
    max_bin = th1_fe55_F.GetMaximumBin()
    tf1_gaus.SetParameter(0, th1_fe55_F.GetBinContent(max_bin))
    tf1_gaus.SetParameter(1, th1_fe55_F.GetBinCenter(max_bin))
    tf1_gaus.SetParameter(2, 5e3)
    tf1_gaus.SetParLimits(0, 0.2 * tf1_gaus.GetParameter(0), 2 * tf1_gaus.GetParameter(0))
    tf1_gaus.SetParLimits(1, th1_fe55_F.GetBinCenter(th1_fe55_F.FindFirstBinAbove(1)), th1_fe55_F.GetBinCenter(th1_fe55_F.FindLastBinAbove(1)))
    tf1_gaus.SetParLimits(2, 1e-2 * tf1_gaus.GetParameter(2), 1e2 * tf1_gaus.GetParameter(2))
    th1_fe55_F.Fit(tf1_gaus, 'BQ')
    fe55_F_mean = tf1_gaus.GetParameter(1)
    fe55_F_err = tf1_gaus.GetParameter(2)
    th1_fe55_F.Write('F')

    # hitbus tot
    th1_fe55_ToT.SetTitle('Hitbus ToT in Fe55 measurement;ToT [s];entries')
    max_bin = th1_fe55_ToT.GetMaximumBin()
    tf1_gaus.SetParameter(0, th1_fe55_ToT.GetBinContent(max_bin))
    tf1_gaus.SetParameter(1, th1_fe55_ToT.GetBinCenter(max_bin))
    tf1_gaus.SetParameter(2, 1e-7)
    tf1_gaus.SetParLimits(0, 0.2 * tf1_gaus.GetParameter(0), 2 * tf1_gaus.GetParameter(0))
    tf1_gaus.SetParLimits(1, th1_fe55_ToT.GetBinCenter(th1_fe55_ToT.FindFirstBinAbove(1)), th1_fe55_ToT.GetBinCenter(th1_fe55_ToT.FindLastBinAbove(1)))
    tf1_gaus.SetParLimits(2, 1e-2 * tf1_gaus.GetParameter(2), 1e2 * tf1_gaus.GetParameter(2))
    th1_fe55_ToT.Fit(tf1_gaus, 'BQ')
    fe55_tot_mean = tf1_gaus.GetParameter(1)
    fe55_tot_err = tf1_gaus.GetParameter(2)
    th1_fe55_ToT.Write('ToT')

    # maximum amplitude
    th1_fe55_max.SetTitle('Measured maximum amplitude in Fe55 measuremt;measured maximum amplitude [V];entries')
    th1_fe55_max.Write('max')

    # max without hit filter
    th1_fe55_max_nohitf.SetTitle('Measured maximum amplitude in Fe55 measurement without hit filter;measured maximum amplitude [V];entries')
    th1_fe55_max_nohitf.Write('max_nohitf')

    # max without max filter
    th1_fe55_max_nomaxf.SetTitle('Measured maximum amplitude in Fe55 measurement without noise filter;measured maximum amplitude [V];entries')
    th1_fe55_max_nomaxf.Write('max_nomaxf')

#%% R & F determination
root_cd('RFana')

# book filter
rdf_injv_seq1V = rdf_injv.Filter('injv<1.05')
rdf_all_hitf_fe55_maxf_leq300mV = rdf_all_hitf_fe55_maxf.Filter('injv>0.25')

# book histograms
th1_R = rdf_injv_seq1V.Histo1D('R')
th1_F = rdf_all_hitf_fe55_maxf_leq300mV.Histo1D('F')

# TODO: fits in code
injv_R_mean = 1.23e-7
injv_R_err = 0.17e-8
injv_F_mean = 7.63e4
injv_F_err = 0.39e3

# fit parameter R
th1_R.SetTitle('Fitted rise parameter R for Injection <= 1.0V;R [s];entries')
th1_R.Write('R')

# fit parameter F
th1_F.SetTitle('Fitted fall parameter F for Fe55 with hit filter and Injection >= 300mV;F [V/s];entries')
th1_F.Write('F')

#%% R & F vs A
root_cd('RFana')

# histogram models
th2dm_AR = ROOT.RDF.TH2DModel('th2dm_AR', 'AR', 100, 0., 0.6, 100, 0., 5e-7)
th2dm_AF = ROOT.RDF.TH2DModel('th2dm_AF', 'AF', 100, 0., 0.6, 100, 0., 2e+5)

# book histograms
th2_AR_injv = rdf_all_fe55_maxf.Histo2D(th2dm_AR, 'A', 'R')
th2_AF_injv = rdf_all_fe55_maxf.Histo2D(th2dm_AF, 'A', 'F')

# TODO: draw sigma interval (tgrapherrors with AP option or so)

# R vs A
th2_AR_injv.SetTitle('Fitted rise parameter R vs fitted amplitude parameter A;A [V];R [s]')
th2_AR_injv.Draw('colz')
tf1_AR_R = ROOT.TF1('tf1_AR_R', '[0]', 1e-2, 6e-1)
tf1_AR_R.SetParameter(0, injv_R_mean)
tf1_AR_R.Draw('same')
tcanvas.Write('R_vs_A')

# F vs A
th2_AF_injv.SetTitle('Fitted fall parameter F vs fitted amplitude parameter A;A [V];F [V/s]')
th2_AF_injv.Draw('colz')
tf1_AF_F = ROOT.TF1('tf1_AF_F', '[0]', 1e-2, 6e-1)
tf1_AF_F.SetParameter(0, injv_F_mean)
tf1_AF_F.Draw('same')
tcanvas.Write('F_vs_A')

#%% Fe55 and 300mV comparison (similar amplitude)
if with_fe55:
    root_cd('Fe55/comparison_300mV')

    # book filter
    rdf_injv_300mV = rdf_injv.Filter(f'injv=={injvs[1]}')

    # book histograms
    th2_AR_fe55 = rdf_fe55_maxf.Histo2D(th2dm_AR, 'A', 'R')
    th2_AF_fe55 = rdf_fe55_maxf.Histo2D(th2dm_AF, 'A', 'F')
    th2_AR_300mV = rdf_injv_300mV.Histo2D(th2dm_AR, 'A', 'R')
    th2_AF_300mV = rdf_injv_300mV.Histo2D(th2dm_AF, 'A', 'F')

    # divide canvas for 0.3V vs Fe55
    tcanvas.Clear()
    tcanvas.Divide(2,1, 0.02)

    # R vs A
    tcanvas.cd(1)
    th2_AR_300mV.SetTitle('300mV injection;A [V];R [s]')
    th2_AR_300mV.Draw('colz')
    tcanvas.cd(2)
    th2_AR_fe55.SetTitle('Fe55 measurement;A [V];R [s]')
    th2_AR_fe55.Draw('colz')
    tcanvas.Write('R_vs_A')

    # F vs A
    tcanvas.cd(1)
    th2_AF_300mV.SetTitle('300mV injection;A [V];F [V/s]')
    th2_AF_300mV.Draw('colz')
    tcanvas.cd(2)
    th2_AF_fe55.SetTitle('Fe55 measurement;A [V];F [V/s]')
    th2_AF_fe55.Draw('colz')
    tcanvas.Write('F_vs_A')

    # reset canvas
    tcanvas.Clear()
    tcanvas.Divide(1)

#%% tot comparison
root_cd('tot')

# histogram models
th2dm_tot_calc_vs_tot = ROOT.RDF.TH2DModel('th2dm_tot_calc_vs_tot', 'tot_calc_vs_tot', 100, 0., 6e-6, 100, 0., 8e-6)
tp1dm_tot_calc_vs_tot = ROOT.RDF.TProfile1DModel('tp1dm_tot_calc_vs_tot', 'tot_calc_vs_tot', 100, 0., 6e-6, 0., 8e-6)
th2dm_tot_vs_max = ROOT.RDF.TH2DModel('th2dm_tot_vs_max', 'tot_vs_max', 100, 0., 0.45, 100, 0., 6e-6)
th2dm_tot_calc_vs_max_calc = ROOT.RDF.TH2DModel('th2dm_tot_calc_vs_max_calc', 'tot_calc_vs_max_calc', 100, 0., 0.5, 100, 0., 8e-6)
th2dm_tot_calc_vs_max = ROOT.RDF.TH2DModel('th2dm_tot_calc_vs_max', 'tot_calc_vs_max', 100, 0., 0.45, 100, 0., 8e-6)
th2dm_tot_vs_A = ROOT.RDF.TH2DModel('th2dm_tot_vs_A', 'tot_vs_A', 100, 0., 0.6, 100, 0., 6e-6)
th2dm_tot_calc_vs_A = ROOT.RDF.TH2DModel('th2dm_tot_calc_vs_A', 'tot_calc_vs_A', 100, 0., 0.6, 100, 0., 8e-6)
th2dm_tot_diff_vs_tot = ROOT.RDF.TH2DModel('th2dm_tot_diff_vs_tot', 'tot_diff_vs_tot', 100, 0., 6e-6, 100, -1e-6, 2.5e-6)
th2dm_tot_ratio_vs_tot = ROOT.RDF.TH2DModel('th2dm_tot_ratio_vs_tot', 'tot_ratio_vs_tot', 100, 0., 6e-6, 100, 0.5, 2.5)

# book histograms
th2_tot_calc_vs_tot = rdf_all_hitf_fe55_maxf.Histo2D(th2dm_tot_calc_vs_tot, 'tot', 'tot_calc')
tp1_tot_calc_vs_tot = rdf_all_hitf_fe55_maxf.Profile1D(tp1dm_tot_calc_vs_tot, 'tot', 'tot_calc')
th2_tot_vs_max = rdf_all_hitf_fe55_maxf.Histo2D(th2dm_tot_vs_max, 'max', 'tot')
th2_tot_calc_vs_max_calc = rdf_all_fe55_maxf.Histo2D(th2dm_tot_calc_vs_max_calc, 'max_calc', 'tot_calc')
th2_tot_calc_vs_max = rdf_all_fe55_maxf.Histo2D(th2dm_tot_calc_vs_max, 'max', 'tot_calc')
th2_tot_vs_A = rdf_all_hitf_fe55_maxf.Histo2D(th2dm_tot_vs_A, 'A', 'tot')
th2_tot_calc_vs_A = rdf_all_fe55_maxf.Histo2D(th2dm_tot_calc_vs_A, 'A', 'tot_calc')
th2_tot_diff_vs_tot = rdf_all_hitf_fe55_maxf.Histo2D(th2dm_tot_diff_vs_tot, 'tot', 'tot_diff')
th2_tot_ratio_vs_tot = rdf_all_hitf_fe55_maxf.Histo2D(th2dm_tot_ratio_vs_tot, 'tot', 'tot_ratio')

# tot_calc vs tot
th2_tot_calc_vs_tot.SetTitle('Calculated ToT vs Hitbus ToT for entries with ToT>0;Hitbus ToT [s];Calculated ToT [s]')
th2_tot_calc_vs_tot.SetOption('colz')
th2_tot_calc_vs_tot.Write('tot_calc_vs_tot')

# tot_calc vs tot fit
tp1_tot_calc_vs_tot.SetTitle('Calculated ToT vs Hitbus ToT for entries with ToT>0;Hitbus ToT [s];Calculated ToT [s]')

tf1_lin0 = ROOT.TF1('tf1_lin0', '[0]*x', 0., 6e-6)
tf1_lin0.SetParameter(0, 1.23)
tp1_tot_calc_vs_tot.Fit(tf1_lin0, 'QN')

tp1_tot_calc_vs_tot.Draw()
tf1_lin0.Draw('same')
# TODO: Legend with fit results
tcanvas.Write('tot_calc_vs_tot_fit')

# TODO: add to th2_tot_calc_vs_tot instead?

# tot vs max
th2_tot_vs_max.SetTitle('Hitbus ToT vs measured maximum amplitude for entries with Hitbus ToT>0;measured maximum amplitude [V];Hitbus ToT [s]')
th2_tot_vs_max.SetOption('colz')
th2_tot_vs_max.Write('tot_vs_max')

# tot_calc vs max_calc
th2_tot_calc_vs_max_calc.SetTitle('Calculated ToT vs calculated maximum amplitude;calculated maximum amplitude [V];Calculated ToT [s]')
th2_tot_calc_vs_max_calc.SetOption('colz')
th2_tot_calc_vs_max_calc.Write('tot_calc_vs_max_calc')

# tot_calc vs max
th2_tot_calc_vs_max.SetTitle('Calculated ToT vs measured maximum amplitude;measured maximum amplitude [V];Calculated ToT [s]')
th2_tot_calc_vs_max.SetOption('colz')
th2_tot_calc_vs_max.Write('tot_calc_vs_max')

# tot vs A
th2_tot_vs_A.SetTitle('Hitbus ToT vs fitted amplitude parameter A for entries with Hitbus ToT>0;A [V];Hitbus ToT [s]')
th2_tot_vs_A.SetOption('colz')
th2_tot_vs_A.Write('tot_vs_A')

# tot_calc vs A
th2_tot_calc_vs_A.SetTitle('Calculated ToT vs fitted amplitude parameter A;A [V];Calculated ToT [s]')
th2_tot_calc_vs_A.SetOption('colz')
th2_tot_calc_vs_A.Write('tot_calc_vs_A')

# tot_diff vs tot
th2_tot_diff_vs_tot.SetTitle('Difference between calculated and Hitbus ToT vs Hitbus ToT with Hitbus ToT>0;Hitbus ToT [s];Calculated ToT - Hitbus ToT [s]')
th2_tot_diff_vs_tot.SetOption('colz')
th2_tot_diff_vs_tot.Write('tot_diff_vs_tot')

# tot_ratio vs tot
th2_tot_ratio_vs_tot.SetTitle('Ratio between calculated and Hitbus ToT vs Hitbus ToT with Hitbus ToT>0;Hitbus ToT [s];Calculated ToT / Hitbus ToT [s]')
th2_tot_ratio_vs_tot.SetOption('colz')
th2_tot_ratio_vs_tot.Write('tot_ratio_vs_tot')

#%% Amplitude analysis
root_cd('amplitude')

# histogram models
th2dm_A_vs_max = ROOT.RDF.TH2DModel('th2dm_A_vs_max', 'A_vs_max', 100, 0., 0.45, 100, 0., 0.6)
th2dm_A_vs_max_calc = ROOT.RDF.TH2DModel('th2dm_A_vs_max_calc', 'A_vs_max_calc', 100, 0., 0.5, 100, 0., 0.6)
th2dm_max_calc_vs_max = ROOT.RDF.TH2DModel('th2dm_max_calc_vs_max', 'max_calc_vs_max', 100, 0., 0.45, 100, 0., 0.5)

# book histograms
th2_A_vs_max = rdf_all_fe55_maxf.Histo2D(th2dm_A_vs_max, 'max', 'A')
th2_A_vs_max_calc = rdf_all_fe55_maxf.Histo2D(th2dm_A_vs_max_calc, 'max_calc', 'A')
th2_max_calc_vs_max = rdf_all_fe55_maxf.Histo2D(th2dm_max_calc_vs_max, 'max', 'max_calc')

# A vs max
th2_A_vs_max.SetTitle('Fitted amplitude parameter A vs measured maximum amplitude;measured maximum amplitude [V];A [V]')
th2_A_vs_max.SetOption('colz')
th2_A_vs_max.Write('A_vs_max')

# A vs max_calc
th2_A_vs_max_calc.SetTitle('Fitted amplitude parameter A vs calculated maximum amplitude;calculated maximum amplitude [V];A [V]')
th2_A_vs_max_calc.SetOption('colz')
th2_A_vs_max_calc.Write('A_vs_max_calc')

# max_calc vs max
th2_max_calc_vs_max.SetTitle('Calculated maximum amplitude vs measured maximum amplitude;measured maximum amplitude [V];calculated maximum amplitude [V]')
th2_max_calc_vs_max.SetOption('colz')
th2_max_calc_vs_max.Write('max_calc_vs_max')

#%% fe55 calibration, see thesis
# TODO: calaibration point is wrong? New Allpix data
# TODO: do calibration point fit somewhere (from Allpix data)
if with_fe55:
    fe55_charges = 1604
    fe55_charges_err = 15

    calibration_mq = (fe55_A_mean - calibration_A0) / fe55_charges
    calibration_mq_err = np.sqrt(fe55_A_err**2 + calibration_A0_err**2 + ((fe55_A_mean - calibration_A0) * fe55_charges_err / fe55_charges)**2) / fe55_charges

    calibration_Cinj = calibration_minj / calibration_mq
    calibration_Cinj_err = np.sqrt(calibration_minj_err**2 + (calibration_minj * calibration_mq_err / calibration_mq)**2) / calibration_mq

    injvs_as_q = calibration_Cinj * np.array(injvs)
    injvs_as_q_err = calibration_Cinj_err * np.array(injvs)

    print(f'\
        Acal = {format_value_error(fe55_A_mean, fe55_A_err)}\n\
        qcal = {format_value_error(fe55_charges, fe55_charges_err)}\n\
        A0   = {format_value_error(calibration_A0, calibration_A0_err)}\n\
        minj = {format_value_error(calibration_minj, calibration_minj_err)}\n\
        mq   = {format_value_error(calibration_mq, calibration_mq_err)}\n\
        Cinj = {format_value_error(calibration_Cinj, calibration_Cinj_err)}\n\
    ')

#%% calibrated parameter dependency
if with_fe55:
    root_cd('calibrated')

    tgraph_fe55 = ROOT.TGraphErrors(1, array('f', [fe55_charges]), array('f', [fe55_A_mean]), array('f', [fe55_charges_err]), array('f', [fe55_A_err]))
    tgraph_fe55.SetLineWidth(2)
    tgraph_fe55.SetLineColor(ROOT.kBlue)

    tf1_pol1.SetRange(0.0, 1.5 * calibration_Cinj)
    tf1_pol1.SetParameter(0, calibration_A0)
    tf1_pol1.SetParameter(1, calibration_mq)

    tgraph_A = ROOT.TGraphErrors(len(injvs_as_q), injvs_as_q, inj_A, injvs_as_q_err, inj_dA)
    tgraph_A.SetLineWidth(2)

    tf1_pol1.SetTitle('Fitted amplitude parameter A vs charge;charge [e];A [V]')
    tf1_pol1.Draw()
    tgraph_A.Draw('same')
    tgraph_fe55.Draw('same')
    tlegend_A = ROOT.TLegend(0.1, 0.7, 0.5, 0.9)
    tlegend_A.AddEntry(tgraph_A, 'data from injection')
    tlegend_A.AddEntry(tgraph_fe55, 'Fe55 calibration point')
    tlegend_A.AddEntry(tf1_pol1, f'linear thingy in V/e')
    tlegend_A.Draw()
    tcanvas.Write('A')

#%% Write results
tfile_output.Close()

# store fit values somewhere
