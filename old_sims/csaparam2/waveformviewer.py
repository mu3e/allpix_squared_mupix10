#!/usr/bin/python3
#
# This script allows to view waveforms stored in ttrees.

#%% imports
import sys
import ROOT

#%% get cli arguments
evnr_str = '1'
if len(sys.argv) == 1:
    raise Exception('No file given')
elif len(sys.argv) == 2:
    pass
elif len(sys.argv) == 3:
    evnr_str = sys.argv[2]
elif len(sys.argv) > 3:
    raise Exception('Too many arguments')

#%% open ROOT file
print(f'Opening file {sys.argv[1]}')
tfile = ROOT.TFile(sys.argv[1], 'READ')

#%% prepare canvases and draw initial waveforms
tcanvas_ampout = ROOT.TCanvas('tcanvas_ampout', 'AmpOut', 1280, 720)
tcanvas_hitbus = ROOT.TCanvas('tcanvas_hitbus', 'Hitbus', 1280, 720)
tcanvas_injection = ROOT.TCanvas('tcanvas_injection', 'Injection', 1280, 720)

#%% read loop
while(True):
    # open event
    ttree_event = tfile.Get(evnr_str)

    # prepare tprofiles
    entries = ttree_event.GetEntries()
    t_min = ttree_event.GetMinimum('t')
    t_max = ttree_event.GetMaximum('t')
    t_bin_size = (t_max - t_min) / (entries - 1)
    t_min_bin = t_min - t_bin_size / 2
    t_max_bin = t_max + t_bin_size / 2

    # create tprofiles
    tprofile_ampout = ROOT.TProfile('tprofile_ampout', 'AmpOut', entries, t_min_bin, t_max_bin)
    tprofile_hitbus = ROOT.TProfile('tprofile_hitbus', 'Hitbus', entries, t_min_bin, t_max_bin)
    tprofile_injection = ROOT.TProfile('tprofile_injection', 'Injection', entries, t_min_bin, t_max_bin)

    # draw ampout
    tcanvas_ampout.cd()
    ttree_event.Draw('ampout:t>>tprofile_ampout', '', 'prof goff')
    tprofile_ampout.SetTitle(f'AmpOut for event {evnr_str};time [s];AmpOut [V]')
    tprofile_ampout.Draw()

    # draw hitbus
    tcanvas_hitbus.cd()
    ttree_event.Draw('hitbus:t>>tprofile_hitbus', '', 'prof goff')
    tprofile_hitbus.SetTitle(f'Hitbus for event {evnr_str};time [s];Hitbus [V]')
    tprofile_hitbus.Draw()

    # draw injection
    tcanvas_injection.cd()
    ttree_event.Draw('injection:t>>tprofile_injection', '', 'prof goff')
    tprofile_injection.SetTitle(f'Injection for event {evnr_str};time [s];Injection [V]')
    tprofile_injection.Draw()

    # get event number from user input
    evnr_str = input('Draw event: ')

    # delete variables
    ttree_event.Delete()
    tprofile_ampout.Delete()
    tprofile_hitbus.Delete()
    tprofile_injection.Delete()
