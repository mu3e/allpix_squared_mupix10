#!/usr/bin/python3
#
# Creates average AmpOut waveforms.

#%% imports and settings
import os.path
import sys
import time
import ROOT

# enable multi-threading in ROOT
ROOT.EnableImplicitMT()
# use Minuit2 for fitting
ROOT.TVirtualFitter.SetDefaultFitter('Minuit2')

# measure script duration
t_start = time.perf_counter()
total_count = 0

#%% data settings
datadir_prefix = 'std'
datadirs = [
    '200mV',
    '300mV',
    '400mV',
    '500mV',
    '600mV',
    '700mV',
    '800mV',
    '900mV',
    '1000mV',
    '1100mV',
    '1200mV',
    '1300mV',
    '1400mV',
]

# waveform data binning
wf_entries = 2000
wf_t_min = -2e-6
wf_t_max = 1.799e-005
wf_t_bin_size = (wf_t_max - wf_t_min) / (wf_entries - 1)
wf_t_min_bin = wf_t_min - wf_t_bin_size / 2
wf_t_max_bin = wf_t_max + wf_t_bin_size / 2

#%% loop datadir
for datadir in datadirs:
    print(f'Starting averaging for {datadir}')
    datadir_path = os.path.join(sys.path[0], 'Data', datadir_prefix, datadir)
    t_averaging_start = time.perf_counter()

    #%% output file
    file_path = os.path.join(f'{datadir_path}_average.root')
    tfile_output = ROOT.TFile(file_path, 'RECREATE')

    # create tprofile for average
    tprofile_average = ROOT.TProfile(f'tprofile_{datadir}', f'{datadir}', wf_entries, wf_t_min_bin, wf_t_max_bin)
    tprofile_average.SetErrorOption('s')
    tprofile_average.SetOption('profs')
    tprofile_average.SetTitle(f'Average AmpOut for {datadir};time [s];voltage [V]')

    # open data file
    file_path = os.path.join(f'{datadir_path}.root')
    tfile_data = ROOT.TFile(file_path, 'READ')

    # loop events
    count = 0
    for key in tfile_data.GetListOfKeys():
        count += 1

        # get ttree of event
        ttree_event = tfile_data.Get(key.GetName())

        # create tprofile for ampout waveform
        tprofile_ampout = ROOT.TProfile('tprofile_ampout', 'AmpOut', wf_entries, wf_t_min_bin, wf_t_max_bin)

        # draw ampout into tprofile
        ttree_event.Draw('ampout:t>>tprofile_ampout', '', 'prof goff')

        # add tprofile to average
        tprofile_average.Add(tprofile_ampout)

        # clear memory
        ttree_event.Delete()
        tprofile_ampout.Delete()

    # close data file
    tfile_data.Close()

    # write average ampout waveform
    tfile_output.cd()
    tprofile_average.Write(f'{datadir}')
    tprofile_average.Delete()

    # print duration
    total_count += count
    t_averaging_diff = time.perf_counter() - t_averaging_start
    print(f' averaged {count} events in {t_averaging_diff:#.3g}s')

# print total duration
t_diff = time.perf_counter() - t_start
print(f'Finished averaging {total_count} waveforms in {t_diff:#.3g}s')
