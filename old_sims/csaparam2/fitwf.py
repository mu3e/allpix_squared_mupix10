#!/usr/bin/python3
#
# Fits the AmpOut waveforms, calculates the ToT from the AmpOut,
# calculates the ToT from the Hitbus and creates average AmpOut waveforms.

# TODO: Also fit averages and put results in own ttree.
#       Or no averages and fit it in other script?
#       Or fetch average output from other script?
# TODO: no number of injvs, but instead loop dir and add optional break

#%% imports and settings
import os.path
import sys
import time
from array import array
import ROOT

# enable multi-threading in ROOT
ROOT.EnableImplicitMT()
# use Minuit2 for fitting
ROOT.TVirtualFitter.SetDefaultFitter('Minuit2')

# measure script duration
t_start = time.perf_counter()

#%% data settings
# waveform data
datadir_prefix = 'std'
injvs = [0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4]
with_fe55 = False
number_of_injv_wf = 10000
number_of_fe55_wf = 20000

# waveform data binning
wf_entries = 2000
wf_t_min = -2e-6
wf_t_max = 1.799e-005
wf_t_bin_size = (wf_t_max - wf_t_min) / (wf_entries - 1)
wf_t_min_bin = wf_t_min - wf_t_bin_size / 2
wf_t_max_bin = wf_t_max + wf_t_bin_size / 2

# chip settings
threshold = 34e-3

# oscilloscope settings
ampout_offset = 0.
ampout_offset_deviation = 10e-3
hitbus_high = 720e-3
hitbus_low = 30e-3

#%% AmpOut fitfunction
# V(t) = A*(1-exp(-(t-t0)/R)) - F*(t-t0) + v0
# and v0 if it gets smaller than v0
tf1_ampout = ROOT.TF1('tf1_ampout', 'TMath::Max([0]*(1.-TMath::Exp(-(x-[3])/[1]))-[2]*(x-[3])+[4],[4])')

# function parameters
p_ampout_names = ['A',     'R',     'F',    't0',  'v0'                                  ]
p_ampout_init  = [ 1.5e-1,  1.5e-7,  6.2e4,  0.,    ampout_offset                        ]
p_ampout_lower = [ 5.0e-3,  5.0e-8,  5.0e3, -5e-7,  ampout_offset-ampout_offset_deviation]
p_ampout_upper = [ 7.0e-1,  4.0e-7,  2.0e5, +5e-7,  ampout_offset+ampout_offset_deviation]

# set fitfunction parameters and limits
for n in range(len(p_ampout_names)):
    tf1_ampout.SetParName(n, p_ampout_names[n])
    tf1_ampout.SetParameter(n, p_ampout_init[n])
    tf1_ampout.SetParLimits(n, p_ampout_lower[n], p_ampout_upper[n])

# include header for Lambert W function from GSL
# TODO: use ROOT.Math.lambert_W0 and ROOT.Math.lambert_Wm1 once available
#       see https://github.com/root-project/root/pull/8496
ROOT.gInterpreter.ProcessLine('#include <gsl/gsl_sf_lambert.h>')

# define function to get pre-fit-parameters:
def set_pre_fit_params(tprofile, tf1):
    max_bin = tprofile.GetMaximumBin()
    t_max = tprofile.GetBinCenter(max_bin)
    f_max = tprofile.GetBinContent(max_bin) - ampout_offset
    hmax_bin = tprofile.FindLastBinAbove(0.5 * f_max + ampout_offset, 1, max_bin)
    t_hmax = tprofile.GetBinCenter(hmax_bin)

    AdivF = 2 * t_hmax - t_max
    R = -t_max / ROOT.gsl_sf_lambert_Wm1(-1 * min(t_max / AdivF, 1/ROOT.TMath.E()))
    A = f_max / (1 - R / AdivF * (1 + ROOT.log(AdivF / R)))
    F = A / AdivF

    tf1.SetParameter(0, A)
    tf1.SetParLimits(0, 0.5 * A, 2 * A)
    tf1.SetParameter(1, R)
    tf1.SetParLimits(1, 0.5 * R, 5 * R)
    tf1.SetParameter(2, F)
    tf1.SetParLimits(2, 0.5 * F, 2 * F)

    tf1.SetParameter(3, p_ampout_init[3])
    tf1.SetParameter(4, p_ampout_init[4])

#%% Hitbus
# digital threshold level for ToT
hitbus_th = 0.5 * (hitbus_high - hitbus_low) + hitbus_low

# first bin below function
ROOT.gInterpreter.ProcessLine('\
    Int_t TH1_FindFirstBinBelow(const TH1& th1, Double_t threshold) {\
        const Int_t firstBin = 1;\
        const Int_t lastBin = th1.GetNbinsX();\
        for (Int_t binx = firstBin; binx <= lastBin; ++binx) {\
            if (th1.GetBinContent(binx) < threshold) return binx;\
        }\
        return -1;\
    };\
')

# last bin below function
ROOT.gInterpreter.ProcessLine('\
    Int_t TH1_FindLastBinBelow(const TH1& th1, Double_t threshold) {\
        const Int_t firstBin = 1;\
        const Int_t lastBin = th1.GetNbinsX();\
        for (Int_t binx = lastBin; binx >= firstBin; --binx) {\
            if (th1.GetBinContent(binx) < threshold) return binx;\
        }\
        return -1;\
    };\
')

#%% output file
file_path = os.path.join(sys.path[0], 'Data', datadir_prefix, 'FitResults.root')
tfile_output = ROOT.TFile(file_path, 'RECREATE')
tfile_output.mkdir('average_ampout')

#%% TTree with fit outputs
ttree = ROOT.TTree('fitres', 'waveform fit results')
td_injv = array('f', [0.])  # injection voltage, 0 for Fe55
td_evnr = array('I', [0])
td_A = array('f', [0.])
td_R = array('f', [0.])
td_F = array('f', [0.])
td_t0 = array('f', [0.])
td_v0 = array('f', [0.])
td_chi2 = array('f', [0.])
td_max = array('f', [0.])  # maximal measured value minus fitted baseline
td_tot = array('f', [0.])  # tot estimated from Hitbus, 0 if no hit
td_tot_ampout = array('f', [0.])  # tot estimated from AmpOut
ttree.Branch('injv', td_injv, 'injv/F')
ttree.Branch('evnr', td_evnr, 'evnr/I')
ttree.Branch('A', td_A, 'A/F')
ttree.Branch('R', td_R, 'R/F')
ttree.Branch('F', td_F, 'F/F')
ttree.Branch('t0', td_t0, 't0/F')
ttree.Branch('v0', td_v0, 'v0/F')
ttree.Branch('chi2', td_chi2, 'chi2/F')
ttree.Branch('max', td_max, 'max/F')
ttree.Branch('tot', td_tot, 'tot/F')
ttree.Branch('tot_ampout', td_tot_ampout, 'tot_ampout/F')

#%% fit procedure
def do_fit(tfile, event_nr, tprofile_average):
    # get ttree for event
    ttree_event = tfile.Get(str(event_nr))

    # create empty tprofile with correct bins
    tprofile_ampout = ROOT.TProfile('tprofile_ampout', 'AmpOut', wf_entries, wf_t_min_bin, wf_t_max_bin)
    tprofile_hitbus = ROOT.TProfile('tprofile_hitbus', 'Hitbus', wf_entries, wf_t_min_bin, wf_t_max_bin)

    # fill tprofiles from ttree
    ttree_event.Draw('ampout:t>>tprofile_ampout', '', 'prof goff')
    ttree_event.Draw('hitbus:t>>tprofile_hitbus', '', 'prof goff')

    # add ampout to average tprofile
    tprofile_average.Add(tprofile_ampout)

    # fit ampout
    set_pre_fit_params(tprofile_ampout, tf1_ampout)
    tprofile_ampout.Fit(tf1_ampout, 'BLQN')

    # get max amplitude from measured pulse
    max_ampout = tprofile_ampout.GetBinContent(tprofile_ampout.GetMaximumBin())

    # Hitbus ToT
    tot = 0.
    tot_t1_bin = ROOT.TH1_FindFirstBinBelow(tprofile_hitbus, hitbus_th)
    if tot_t1_bin != -1:
        tot_t2_bin = ROOT.TH1_FindLastBinBelow(tprofile_hitbus, hitbus_th)
        if tot_t2_bin != -1:
            tot = tprofile_hitbus.GetBinCenter(tot_t2_bin) - tprofile_hitbus.GetBinCenter(tot_t1_bin)

    # get fitted AmpOut baseline
    v0 = tf1_ampout.GetParameter(4)

    # calculate ToT from AmpOut pulse
    tot_ampout = 0.
    tot_ampout_t1_bin = tprofile_ampout.FindFirstBinAbove(threshold + v0)
    if tot_ampout_t1_bin != -1:
        tot_ampout_t2_bin = tprofile_ampout.FindLastBinAbove(threshold + v0)
        if tot_ampout_t2_bin != -1:
            tot_ampout = tprofile_ampout.GetBinCenter(tot_ampout_t2_bin) - tprofile_ampout.GetBinCenter(tot_ampout_t1_bin)


    # get fit parameters and fill ttree
    td_evnr[0] = event_nr
    td_A[0] = tf1_ampout.GetParameter(0)
    td_R[0] = tf1_ampout.GetParameter(1)
    td_F[0] = tf1_ampout.GetParameter(2)
    td_t0[0] = tf1_ampout.GetParameter(3)
    td_v0[0] = tf1_ampout.GetParameter(4)
    td_chi2[0] = tf1_ampout.GetChisquare()
    td_max[0] = max_ampout - td_v0[0]
    td_tot[0] = tot
    td_tot_ampout[0] = tot_ampout
    ttree.Fill()

    # clear memory
    ttree_event.Delete()
    tprofile_ampout.Delete()
    tprofile_hitbus.Delete()

#%% fits for injection voltages
for injv in injvs:
    injv_mV_str = f'{int(injv * 1e3)}mV'
    print(f'Start fitting routine for {injv_mV_str}')
    t_fit_start = time.perf_counter()
    # set injection value for ttree
    td_injv[0] = injv
    # store new initial value for A (estimated)
    p_ampout_init[0] = 0.02 + 0.38 * injv
    # tprofile for average ampout waveform
    tprofile_data = ROOT.TProfile(f'tprofile_{injv_mV_str}', f'{injv_mV_str}', wf_entries, wf_t_min_bin, wf_t_max_bin)
    # open data file
    file_path = os.path.join(sys.path[0], 'Data', datadir_prefix, f'{injv_mV_str}.root')
    tfile_data = ROOT.TFile(file_path, 'READ')
    # do the fits
    for n in range(1, number_of_injv_wf+1):
        do_fit(tfile_data, n, tprofile_data)
    # close data file
    tfile_data.Close()
    # write average ampout waveform
    tfile_output.cd('average_ampout')
    tprofile_data.SetOption('hist L')
    tprofile_data.Write(f'{injv_mV_str}')
    tprofile_data.Delete()
    # print duration
    t_fit_diff = time.perf_counter() - t_fit_start
    print(f' fitted {number_of_injv_wf} events in {t_fit_diff:#.3g}s')

#%% fits for Fe55, same procedure with injv=0
if with_fe55:
    print(f'Start fitting routine for Fe55')
    t_fit_start = time.perf_counter()
    td_injv[0] = 0.
    p_ampout_init[0] = 0.14
    tprofile_data = ROOT.TProfile(f'tprofile_Fe55', 'Fe55', wf_entries, wf_t_min_bin, wf_t_max_bin)
    file_path = os.path.join(sys.path[0], 'Data', datadir_prefix, 'Fe55.root')
    tfile_data = ROOT.TFile(file_path, 'READ')
    for n in range(1, number_of_fe55_wf+1):
        do_fit(tfile_data, n, tprofile_data)
    tfile_data.Close()
    tfile_output.cd('average_ampout')
    tprofile_data.SetOption('hist L')
    tprofile_data.Write('Fe55')
    tprofile_data.Delete()
    t_fit_diff = time.perf_counter() - t_fit_start
    print(f' fitted {number_of_fe55_wf} events in {t_fit_diff:#.3g}s')

#%% store trees and close output file
tfile_output.cd()
ttree.Write()
tfile_output.Close()

# print total duration
t_diff = time.perf_counter() - t_start
number_of_total_wf = number_of_injv_wf * len(injvs) + number_of_fe55_wf
print(f'Finished fitting {number_of_total_wf} events in {t_diff/60:#.3g}min')
