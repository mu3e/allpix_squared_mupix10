#!/usr/bin/python3

# This scrips fits the average waveforms for injection.

#%% imports
from array import array
import ROOT

from csaparam.timer import Timer
from csaparam import tools
from csaparam import roottools
from csaparam.fitter import Fitter, FitResTree

#%% measure script duration
timer = Timer()
timer.Start()

#%% data settings
datadir_prefix = 'std'
datadirs = [
    '200mV',
    '300mV',
    '400mV',
    '500mV',
    '600mV',
    '700mV',
    '800mV',
    '900mV',
    '1000mV',
    '1100mV',
    '1200mV',
    '1300mV',
    '1400mV',
    'Fe55',
]

#%% output file
tfile_output = roottools.OpenTFile(datadir_prefix, 'FitResultsAverage', 'RECREATE')
tfile_output.mkdir('averages')
tfile_output.mkdir('average_derivatives')

#%% prepare tree with fit results
fitres_tree = FitResTree()

#%% loop datadirs
for datadir in datadirs:
    print(f'Starting fitting for {datadir}')
    timer_datadir = Timer()
    timer_datadir.Start()

    # get injection voltage
    injv = tools.GetInjectionFromDatadir(datadir)

    # open data TFile
    tfile_data = roottools.OpenTFile(datadir_prefix, datadir)

    # get average waveform
    tprofile_average = tfile_data.Get('average')

    # fit waveform
    tprofile_average_fitter = Fitter(tprofile_average)
    fitres = tprofile_average_fitter.Fit()

    # store fitresults
    fitres_tree.Fill(fitres, injv)

    # calculate and write derviate
    tfile_output.cd('average_derivatives')
    tgraph_average_deriv = roottools.CalcTProfileDerivate(tprofile_average)
    tgraph_average_deriv.Write(datadir)

    # write average tprofile with drawed fit
    tfile_output.cd('averages')
    tprofile_average.Write(datadir)

    # close data TFile
    tfile_data.Close()

    # print duration
    timer_datadir.Stop()
    print(f' fitted average waveform in {timer_datadir.GetPrettyDuration()}')

#%% write ttree and close file
tfile_output.cd()
fitres_tree.Write()
tfile_output.Close()

#%% print total duration
timer.Stop()
print(f'Finished fitting {len(datadirs)} waveforms in {timer.GetPrettyDuration()}')
