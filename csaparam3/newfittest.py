#!/usr/bin/python3

import ROOT
import numpy as np
import matplotlib.pyplot as plt

ROOT.Math.MinimizerOptions.SetDefaultMinimizer('Minuit2', 'Simplex')
ROOT.Math.MinimizerOptions.SetDefaultTolerance(0.001)
ROOT.Math.MinimizerOptions.SetDefaultStrategy(0)
ROOT.Math.MinimizerOptions.SetDefaultPrintLevel(2)

ROOT.gInterpreter.ProcessLine('#include "csaparam/MuPixPulseFF.hpp"')
tf1 = ROOT.TF1('mupix_pulse_ff', ROOT.MuPixPulseFitFunction, -0.2, 5., 10)
tf1.SetNpx(200)
tf1.SetParName(0, 'timestep')
tf1.SetParName(1, 't_0')
tf1.SetParName(2, 'U_0')
tf1.SetParName(3, 'A')
tf1.SetParName(4, 't_R')
tf1.SetParName(5, 't_F')
tf1.SetParName(6, 't_S')
tf1.SetParName(7, 'Fb')
tf1.SetParName(8, 'Fb_D')
tf1.SetParName(9, 'U_sat')


tf1.FixParameter(tf1.GetParNumber('timestep'), 0.001)

tf1.SetParameter(tf1.GetParNumber('t_0'), -0.02)
tf1.SetParLimits(tf1.GetParNumber('t_0'), -0.15, 0.05)

tf1.FixParameter(tf1.GetParNumber('U_0'), 1.4)

tf1.SetParameter(tf1.GetParNumber('A'), 200.)
tf1.SetParLimits(tf1.GetParNumber('A'), 40., 900.)

#tf1.FixParameter(tf1.GetParNumber('t_R'), 0.1)
tf1.SetParameter(tf1.GetParNumber('t_R'), 0.1)
tf1.SetParLimits(tf1.GetParNumber('t_R'), 0.01, 0.2)

#tf1.FixParameter(tf1.GetParNumber('t_F'), 4.8)
tf1.SetParameter(tf1.GetParNumber('t_F'), 4.8)
tf1.SetParLimits(tf1.GetParNumber('t_F'), 2., 30.)

#tf1.FixParameter(tf1.GetParNumber('t_S'), 0.1)
tf1.SetParameter(tf1.GetParNumber('t_S'), 0.1)
tf1.SetParLimits(tf1.GetParNumber('t_S'), 0.01, 0.2)

#tf1.FixParameter(tf1.GetParNumber('Fb'), 23.)
tf1.SetParameter(tf1.GetParNumber('Fb'), 23.)
tf1.SetParLimits(tf1.GetParNumber('Fb'), 15., 70.)

tf1.FixParameter(tf1.GetParNumber('Fb_D'), 2.)
#tf1.SetParameter(tf1.GetParNumber('Fb_D'), 2.)
#tf1.SetParLimits(tf1.GetParNumber('Fb_D'), 1., 5.)

tf1.FixParameter(tf1.GetParNumber('U_sat'), 360.)
#tf1.SetParameter(tf1.GetParNumber('U_sat'), 360.)
#tf1.SetParLimits(tf1.GetParNumber('U_sat'), 340., 500-.)


do_input = True


A_arr = []
t_R_arr = []
t_F_arr = []
t_S_arr = []
Fb_arr = []
Fb_D_arr = []


print('Fe55')
tfile_Fe55 = ROOT.TFile('Data/old_blpix/Fe55.root', 'READ')
tprofile_Fe55 = tfile_Fe55.Get('average')
tf1.SetParameter('A', 85.)
tf1.SetParameter('t_0', -0.1)
tf1.SetRange(-0.5, tprofile_Fe55.GetBinCenter(tprofile_Fe55.GetMaximumBin()))
tprofile_Fe55.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_Fe55.GetBinCenter(tprofile_Fe55.FindLastBinAbove(3., 1, 250)))
tprofile_Fe55.Fit(tf1, 'RB')
if do_input: input()


print('1400mV')
tfile_1400mV = ROOT.TFile('Data/old_blpix/1400mV.root', 'READ')
tprofile_1400mV = tfile_1400mV.Get('average')
tf1.SetParameter('A', 750.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_1400mV.GetBinCenter(tprofile_1400mV.GetMaximumBin()))
tprofile_1400mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_1400mV.GetBinCenter(tprofile_1400mV.FindLastBinAbove(3., 1, 250)))
tprofile_1400mV.Fit(tf1, 'RB')
if do_input: input()


print('1000mV')
tfile_1000mV = ROOT.TFile('Data/old_blpix/1000mV.root', 'READ')
tprofile_1000mV = tfile_1000mV.Get('average')
tf1.SetParameter('A', 500.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_1000mV.GetBinCenter(tprofile_1000mV.GetMaximumBin()))
tprofile_1000mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_1000mV.GetBinCenter(tprofile_1000mV.FindLastBinAbove(3., 1, 250)))
tprofile_1000mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


print('900mV')
tfile_900mV = ROOT.TFile('Data/old_blpix/900mV.root', 'READ')
tprofile_900mV = tfile_900mV.Get('average')
tf1.SetParameter('A', 470.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_900mV.GetBinCenter(tprofile_900mV.GetMaximumBin()))
tprofile_900mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_900mV.GetBinCenter(tprofile_900mV.FindLastBinAbove(3., 1, 250)))
tprofile_900mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


print('800mV')
tfile_800mV = ROOT.TFile('Data/old_blpix/800mV.root', 'READ')
tprofile_800mV = tfile_800mV.Get('average')
tf1.SetParameter('A', 400.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_800mV.GetBinCenter(tprofile_800mV.GetMaximumBin()))
tprofile_800mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_800mV.GetBinCenter(tprofile_800mV.FindLastBinAbove(3., 1, 250)))
tprofile_800mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


print('700mV')
tfile_700mV = ROOT.TFile('Data/old_blpix/700mV.root', 'READ')
tprofile_700mV = tfile_700mV.Get('average')
tf1.SetParameter('A', 330.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_700mV.GetBinCenter(tprofile_700mV.GetMaximumBin()))
tprofile_700mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_700mV.GetBinCenter(tprofile_700mV.FindLastBinAbove(3., 1, 250)))
tprofile_700mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


print('600mV')
tfile_600mV = ROOT.TFile('Data/old_blpix/600mV.root', 'READ')
tprofile_600mV = tfile_600mV.Get('average')
tf1.SetParameter('A', 270.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_600mV.GetBinCenter(tprofile_600mV.GetMaximumBin()))
tprofile_600mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_600mV.GetBinCenter(tprofile_600mV.FindLastBinAbove(3., 1, 250)))
tprofile_600mV.GetXaxis().SetRangeUser(-1, 6.)
tprofile_600mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


print('500mV')
tfile_500mV = ROOT.TFile('Data/old_blpix/500mV.root', 'READ')
tprofile_500mV = tfile_500mV.Get('average')
tf1.SetParameter('A', 210.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_500mV.GetBinCenter(tprofile_500mV.GetMaximumBin()))
tprofile_500mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_500mV.GetBinCenter(tprofile_500mV.FindLastBinAbove(3., 1, 250)))
tprofile_500mV.GetXaxis().SetRangeUser(-1, 6.)
tprofile_500mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


print('400mV')
tfile_400mV = ROOT.TFile('Data/old_blpix/400mV.root', 'READ')
tprofile_400mV = tfile_400mV.Get('average')
tf1.SetParameter('A', 150.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_400mV.GetBinCenter(tprofile_400mV.GetMaximumBin()))
tprofile_400mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_400mV.GetBinCenter(tprofile_400mV.FindLastBinAbove(3., 1, 250)))
tprofile_400mV.GetXaxis().SetRangeUser(-1, 6.)
tprofile_400mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


print('300mV')
tfile_300mV = ROOT.TFile('Data/old_blpix/300mV.root', 'READ')
tprofile_300mV = tfile_300mV.Get('average')
tf1.SetParameter('A', 105.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_300mV.GetBinCenter(tprofile_300mV.GetMaximumBin()))
tprofile_300mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_300mV.GetBinCenter(tprofile_300mV.FindLastBinAbove(3., 1, 250)))
tprofile_300mV.GetXaxis().SetRangeUser(-1, 6.)
tprofile_300mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


print('200mV')
tfile_200mV = ROOT.TFile('Data/old_blpix/200mV.root', 'READ')
tprofile_200mV = tfile_200mV.Get('average')
tf1.SetParameter('A', 60.)
tf1.SetParameter('t_0', -0.02)
tf1.SetRange(-0.5, tprofile_200mV.GetBinCenter(tprofile_200mV.GetMaximumBin()))
tprofile_200mV.Fit(tf1, 'RBN')
tf1.SetRange(-0.5, tprofile_200mV.GetBinCenter(tprofile_200mV.FindLastBinAbove(3., 1, 250)))
tprofile_200mV.GetXaxis().SetRangeUser(-1, 6.)
tprofile_200mV.Fit(tf1, 'RB')
A_arr.append(tf1.GetParameter('A'))
t_R_arr.append(tf1.GetParameter('t_R'))
t_F_arr.append(tf1.GetParameter('t_F'))
t_S_arr.append(tf1.GetParameter('t_S'))
Fb_arr.append(tf1.GetParameter('Fb'))
Fb_D_arr.append(tf1.GetParameter('Fb_D'))
if do_input: input()


injvs = [1000, 900, 800, 700, 600, 500, 400, 300, 200]
print(injvs)
print(A_arr)


plt.figure('A')
plt.grid(True)
plt.plot(injvs, A_arr)
plt.figure('t_R')
plt.grid(True)
plt.plot(injvs, t_R_arr)
plt.figure('t_F')
plt.grid(True)
plt.plot(injvs, t_F_arr)
plt.figure('t_S')
plt.grid(True)
plt.plot(injvs, t_S_arr)
plt.figure('Fb')
plt.grid(True)
plt.plot(injvs, Fb_arr)
plt.figure('Fb_D')
plt.grid(True)
plt.plot(injvs, Fb_D_arr)

plt.show()
