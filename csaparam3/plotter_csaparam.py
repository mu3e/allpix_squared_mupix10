#!/usr/bin/python3

from array import array
import ROOT


def CSAParamAvgWaveforms() -> ROOT.TCanvas:
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_CSAParamAvgWaveforms', 'canvas_CSAParamAvgWaveforms', -1280, 720)

    injvs = [1400, 1300, 1200, 1100, 1000, 900, 800, 700, 600, 500, 400, 300, 200]
    _memory_presistency = []

    for injv in injvs[0:1]:
        tfile_injv = ROOT.TFile(f'Data/old_blpix/{injv}mV.root', 'READ')

        tprofile_injv = tfile_injv.Get('average')

        tprofile_injv.SetTitle(';time [us];AmpOut [mV]')
        tprofile_injv.GetXaxis().SetRangeUser(-0.5, 10.5)
        tprofile_injv.GetYaxis().SetRangeUser(-40., 360.)

        tprofile_injv.Draw('hist C')

        _memory_presistency.append(tfile_injv)

    for injv in injvs[1:]:
        tfile_injv = ROOT.TFile(f'Data/old_blpix/{injv}mV.root', 'READ')

        tprofile_injv = tfile_injv.Get('average')

        tprofile_injv.Draw('same hist C')

        _memory_presistency.append(tfile_injv)

    canvas.Modified()
    canvas.Update()

    canvas.SaveAs('CSAParamAvgWaveforms.pdf')

    for tfile in _memory_presistency:
        tfile.Close()

    print('Done: CSAParamAvgWaveforms')

    return canvas


def CSAParamAinjvdep() -> ROOT.TCanvas:
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_CSAParamAinjvdep', 'canvas_CSAParamAinjvdep', -1280, 720)

    tfile_ana = ROOT.TFile(f'Data/old_blpix/Analysis.root', 'READ')

    tf1_A_inj_dep = tfile_ana.Get('injection/A_inj_dep_fit')
    th1_A_inj_dep = tfile_ana.Get('injection/A_inj_dep')

    tf1_A_inj_dep.SetTitle(';injection voltage [mV];amplification parameter [mV]')

    tf1_A_inj_dep.Draw()
    canvas.Modified()
    canvas.Update()
    th1_A_inj_dep.Draw('same C')

    canvas.Modified()
    canvas.Update()

    canvas.SaveAs('CSAParamAinjvdep.pdf')

    tfile_ana.Close()

    print('Done: CSAParamAinjvdep')

    return canvas


def CSAParamAqinjdep() -> ROOT.TCanvas:
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_CSAParamAqinjdep', 'canvas_CSAParamAqinjdep', -1280, 720)

    tfile_ana = ROOT.TFile(f'Data/old_blpix/Analysis.root', 'READ')

    tf1_A_q_dep = tfile_ana.Get('fe55/A_q_dep_fit')
    th1_A_q_dep = tfile_ana.Get('fe55/A_q_dep')

    tf1_A_q_dep.SetTitle(';charge [e];amplification parameter [mV]')

    tf1_A_q_dep.Draw()
    th1_A_q_dep.Draw('same C')

    canvas.Modified()
    canvas.Update()

    canvas.SaveAs('CSAParamAqinjdep.pdf')

    tfile_ana.Close()

    print('Done: CSAParamAqinjdep')

    return canvas


def CSAParamPulseFits() -> list:
    ROOT.SetDefaultStyle()

    tfile_fitres = ROOT.TFile('Data/std/FitResultsAverage.root', 'READ')
    tfile_fitres.cd('averages')

    _memory_presistency = []

    for key in ROOT.gDirectory.GetListOfKeys():
        canvas = ROOT.TCanvas(f'canvas_CSAParamPulseFit{key.GetName()}', f'canvas_CSAParamPulseFit{key.GetName()}', -1280, 720)

        tprofile_injv = ROOT.gDirectory.Get(key.GetName())

        tprofile_injv.SetTitle(';time [us];AmpOut [mV]')
        tprofile_injv.GetXaxis().SetRangeUser(-0.5, tprofile_injv.GetBinCenter(tprofile_injv.GetMinimumBin()))

        tprofile_injv.Draw('C')

        canvas.Modified()
        canvas.Update()

        canvas.SaveAs(f'CSAParamPulseFit{key.GetName()}.pdf')

        print(f'Done: CSAParamPulseFit{key.GetName()}')

        _memory_presistency.append(canvas)

    tfile_fitres.Close()

    print('Done: CSAParamPulseFits')

    return _memory_presistency


def CSAParamAllPlots() -> None:
    CSAParamAvgWaveforms()
    CSAParamAinjvdep()
    CSAParamAqinjdep()
    CSAParamPulseFits()
    print('Done: CSAParam')


if __name__ == '__main__':
    ROOT.gInterpreter.ProcessLine('#include \"../csaparam/AllpixStyle.hpp\"')
    ROOT.SetInitialStyle()

    _canvas_CSAParamAvgWaveforms = CSAParamAvgWaveforms()
    _canvas_CSAParamAinjvdep = CSAParamAinjvdep()
    _canvas_CSAParamAqinjdep = CSAParamAqinjdep()
    _canvas_list_CSAParamPulseFits = CSAParamPulseFits()

    input()
