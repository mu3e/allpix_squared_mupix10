#!/usr/bin/python3
#
# This script analyses the fit results of the average and single waveforms for
# each injection and uses the fit results of Fe55 to perform a calibration.

#%% imports and settings
import ROOT

from csaparam.timer import Timer
from csaparam import roottools
from csaparam import analysis

#%% measure script duration
timer = Timer()
timer.Start()

#%% data settings
datadir_prefix = 'old_blpix'

analyse_average = True
analyse_single = True
analyse_fe55 = True

#%% output file
tfile_output = roottools.OpenTFile(datadir_prefix, 'Analysis', 'RECREATE')

#%% main loop
tfile_fitres = roottools.OpenTFile(datadir_prefix, 'FitResultsAverage')

analysis.FullAnalysis(tfile_fitres, tfile_output)

#%% close files
tfile_fitres.Close()
tfile_output.Close()

#%% print total duration
timer.Stop()
print(f'Finished analysis in {timer.GetPrettyDuration()}')
