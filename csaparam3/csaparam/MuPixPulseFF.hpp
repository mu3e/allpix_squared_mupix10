#include <cmath>


// Function for fitting with ROOT
double MuPixPulseFitFunction(double* x, double* par)
{
    // Get function variables
    const double t = x[0];

    // Get function parameters
    const double timestep = par[0];  // Integration step size, e.g. 0.001 us
    const double t_0      = par[1];  // Time offset, e.g. 0 us
    const double U_0      = par[2];  // Voltage offset, e.g. 0 mV
    const double A        = par[3];  // Amplification parameter, e.g. 250 mV
    const double t_R      = par[4];  // Amplification rise time parameter, e.g. 0.1 us
    const double t_F      = par[5];  // Amplficiation fall time parameter, e.g. 4.8 us
    const double t_S      = par[6];  // Amplification smoothing parameter, e.g. 0.1 us
    const double Fb       = par[7];  // Feedback fall rate, e.g. 23 mV/us
    const double Fb_D     = par[8];  // Feedback damping parameter, e.g. 2 mV
    const double U_sat    = par[9];  // Saturation voltage, e.g. 360 mV

    // // Lambda for double-exponential amplification with smoothing
    auto Amplification = [=](double t) -> double
    {
        /*const double rise = 1. - exp(-t / t_R);
        const double fall = exp(-t / t_F) - 1.;
        const double smoothing = 1. - exp(-t / t_S);
        return A * (rise + fall) * smoothing;*/
        return A * (exp(-t / t_F) - exp(-t / t_R)) * (1. - exp(-t / t_S));
    };

    // Lambda for quasi-constant feedback with exponential damping for small signals
    auto Feedback = [=](double U) -> double
    {
        return Fb * (1. - exp(-U / Fb_D));
    };

    // Lambda for saturation at maximum voltage using the logistic function
    auto Saturation = [=](double U) -> double
    {
        return U_sat * (2. / (1. + exp(-2. * U / U_sat)) - 1.);
    };

    // Remove time offset to get evaluation time and number of iterations
    const double t_eval = t - t_0;
    const int iterations = static_cast<int>(std::ceil(t_eval / timestep));

    // Prepare output variable
    double U_out = 0.;

    // Prepare cache variable for amplification output
    double U_amp_t_n   = 0.;
    double U_amp_t_nm1 = 0.;

    // Loop over time
    for (auto n = 1; n < iterations; ++n)
    {
        // Calculate amplification output
        U_amp_t_n = Amplification(static_cast<double>(n) * timestep);

        // Add amplification difference to output
        U_out += (U_amp_t_n - U_amp_t_nm1);

        // Cache amplification output for next iteration
        U_amp_t_nm1 = U_amp_t_n;

        // Substract feedback
        U_out -= Feedback(U_out) * timestep;
    }

    // Apply saturation
    U_out = Saturation(U_out);

    // Add voltage offset
    U_out += U_0;

    // Return result
    return U_out;
}
