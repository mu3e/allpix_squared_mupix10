#include <TF1.h>
#include <TProfile.h>


void MuPixPulsePrefit(TF1& tf1_ff, const TProfile& waveform, uint U_0_bins, double U_0_lim)
{
    // determine U_0

    double U_0_avg = 0.;
    double U_0_avg_err_up = 0.;

    for(auto bin = 1; bin <= U_0_bins; ++bin)
    {
        U_0_avg += waveform.GetBinContent(bin);
        U_0_avg_err_up += waveform.GetBinErrorUp(bin);
    }

    U_0_avg /= U_0_bins;
    U_0_avg_err_up /= U_0_bins;

    tf1_ff.FixParameter(tf1_ff.GetParNumber("U_0"), U_0_avg);


    // determine t_0

    uint bin_t_0 = waveform.FindFirstBinAbove(U_0_avg + U_0_avg_err_up, 1, U_0_bins);
    double t_0_max = waveform.GetBinCenter(bin_t_0);
    double t_0_min = waveform.GetBinCenter(U_0_bins);

    while ((waveform.GetBinContent(bin_t_0) - U_0_avg > U_0_lim) && (bin_t_0 > U_0_bins))
    {
        bin_t_0 -= 1;
    }

    double t_0 = waveform.GetBinCenter(bin_t_0);

    tf1_ff.SetParameter(tf1_ff.GetParNumber("t_0"), t_0);
    tf1_ff.SetParLimits(tf1_ff.GetParNumber("t_0"), t_0_min, t_0_max);


    // determine A

    uint maximum_bin = waveform.GetMaximumBin();
    double A = waveform.GetBinContent(maximum_bin) + waveform.GetBinErrorUp(maximum_bin);

    tf1_ff.SetParameter(tf1_ff.GetParNumber("A"), A);
}


void MuPixPulseSetFitter1Range(TF1& tf1_ff, const TProfile& waveform, int U_0_bins)
{
    double t_min = waveform.GetBinCenter(U_0_bins);

    uint maximum_bin = waveform.GetMaximumBin();
    double t_max = waveform.GetBinCenter(maximum_bin);

    tf1_ff.SetRange(t_min, t_max);
}


void MuPixPulseSetFitter2Range(TF1& tf1_ff, const TProfile& waveform, int U_0_bins)
{
    double t_min = waveform.GetBinCenter(U_0_bins);
    double t_max = 0.;

    double U_0 = tf1_ff.GetParameter("U_0");
    uint nbins = waveform.GetNbinsX();
    uint maximum_bin = waveform.GetMaximumBin();

    for (uint bin = maximum_bin; bin < nbins; ++bin)
    {
        if (waveform.GetBinContent(bin) < U_0)
        {
            t_max = waveform.GetBinCenter(bin);
            break;
        }
    }

    tf1_ff.SetRange(t_min, t_max);
}
