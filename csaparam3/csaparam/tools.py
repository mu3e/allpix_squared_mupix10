# This file contains random functions used sometimes.

import os.path
import sys

from . import units
from . import constants


# Get full path of the datadir prefix
def GetDatadirPrefixPath(datadir_prefix: str) -> str:
    return os.path.join(sys.path[0], 'Data', datadir_prefix)


# Function to convert injection datadir name to a float
def GetInjectionFromDatadir(datadir: str) -> float:
    ret = 0.
    if datadir[-2:] == 'mV':
        ret = float(datadir[:-2]) * units.mV
    elif datadir[-1:] == 'V':
        ret = float(datadir[:-1]) * units.V
    elif datadir == 'Fe55':
        ret = constants.injv_fe55
    elif datadir == 'Sr90':
        ret = constants.injv_sr90
    return ret
