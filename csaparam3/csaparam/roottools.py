# This file contains some useful functions when working with ROOT in Python.

import os.path
from array import array
import ROOT

from . import tools


# Open TFile in data directory
def OpenTFile(datadir_prefix: str, filename: str, option: str = 'READ') -> ROOT.TFile:
    path = os.path.join(tools.GetDatadirPrefixPath(datadir_prefix), f'{filename}.root')
    return ROOT.TFile(path, option)


# Change directory in ROOT file with creating it if not present
def MakeAndChangeDir(tfile: ROOT.TFile, dirname: str = '') -> None:
    if tfile.GetDirectory(dirname) == None:
        tfile.mkdir(dirname)
        tfile.cd(dirname)
    else:
        tfile.cd(dirname)


# Get TObjection from TKey when loop a ROOT directory
def GetTObjectFromTKey(tdirectory: ROOT.TDirectory, tkey: ROOT.TKey) -> ROOT.TObject:
    return tdirectory.Get(tkey.GetName())


# Calculate derivative from TProfile
def CalcTProfileDerivate(tprofile: ROOT.TProfile) -> ROOT.TGraph:
    nbinsx = tprofile.GetNbinsX()

    deriv_x = array('f', [])
    deriv_y = array('f', [])

    for n in range(2, nbinsx):
        dy_np1 = tprofile.GetBinContent(n+1) - tprofile.GetBinContent(n)
        dx_np1 = tprofile.GetBinCenter(n+1) - tprofile.GetBinCenter(n)

        dy_nm1 = tprofile.GetBinContent(n) - tprofile.GetBinContent(n-1)
        dx_nm1 = tprofile.GetBinCenter(n) - tprofile.GetBinCenter(n-1)

        deriv_x.append(tprofile.GetBinCenter(n))
        deriv_y.append(0.5 * (dy_np1 / dx_np1 + dy_nm1 / dx_nm1))

    return ROOT.TGraph(len(deriv_x), deriv_x, deriv_y)
