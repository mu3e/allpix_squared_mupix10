#include <TROOT.h>
#include <TStyle.h>
#include <TColor.h>


// Base settings for all styles based on Allpix Squared
void SetInitialStyle() {
    // CUSTOM use modern style as base
    gROOT->SetStyle("Modern");
    TStyle* modern_style = gROOT->GetStyle("Modern");

    // new style copied from Modern
    TStyle* style = new TStyle("Allpix", "Allpix");
    modern_style->Copy(*style);

    // Prefer OpenGL if available
    style->SetCanvasPreferGL(kTRUE);

    // Set backgrounds
    style->SetCanvasColor(kWhite);
    style->SetFrameFillColor(kWhite);
    style->SetStatColor(kWhite);
    style->SetPadColor(kWhite);
    style->SetFillColor(10);
    style->SetTitleFillColor(kWhite);

    // CUSTOM legend fill color
    style->SetLegendFillColor(TColor::GetColorTransparent(kRed, 0.));

    // SetPaperSize wants width & height in cm: A4 is 20,26
    style->SetPaperSize(20, 26);
    // No yellow border around histogram
    style->SetDrawBorder(0);
    // Remove border of canvas*
    style->SetCanvasBorderMode(0);
    // Remove border of pads
    style->SetPadBorderMode(0);
    style->SetFrameBorderMode(0);
    style->SetLegendBorderSize(0);

    // Default text size
    style->SetTextSize(0.04f);
    style->SetTitleSize(0.04f, "xyz");
    style->SetLabelSize(0.03f, "xyz");

    // CUSTOM offset distance between given text and axis
    style->SetLabelOffset(0.01f, "xyz");
    style->SetTitleOffset(1.0f, "y");
    style->SetTitleOffset(1.1f, "xz");

    // Set font settings
    short font = 42; // Use a clear font
    style->SetTitleFont(font);
    style->SetTitleFontSize(0.06f);
    style->SetStatFont(font);
    style->SetStatFontSize(0.07f);
    style->SetTextFont(font);
    style->SetLabelFont(font, "xyz");
    style->SetTitleFont(font, "xyz");
    style->SetTitleBorderSize(0);
    style->SetStatBorderSize(1);

    // CUSTOM font and size settings
    style->SetLegendFont(font);
    style->SetLegendTextSize(0.04f);
    style->SetTitleSize(0.05f, "xy");
    style->SetTitleSize(0.04f, "z");
    style->SetLabelSize(0.04f, "xyz");

    // Set style for markers
    style->SetMarkerStyle(1);
    style->SetLineWidth(1);
    style->SetMarkerSize(1.2f);

    // CUSTOM Color palette in 2d histogram
    style->SetPalette(kRainBow, nullptr);
    style->SetNumberContours(99);

    // Disable title by default for histograms
    style->SetOptTitle(0);

    // Set statistics
    style->SetOptStat(0);
    style->SetOptFit(0);

    // Number of decimals used for errors
    style->SetEndErrorSize(5);

    // CUSTOM line width and color
    style->SetHistLineWidth(1);
    style->SetFrameLineWidth(1);
    style->SetFuncWidth(2);
    style->SetHistLineColor(kBlue);
    style->SetMarkerColor(kBlue);
    style->SetFuncColor(kRed);
    style->SetLabelColor(kBlack, "xyz");

    // CUSTOM set the margins
    style->SetPadBottomMargin(0.13f);
    style->SetPadTopMargin(0.06f);
    style->SetPadRightMargin(0.03f);
    style->SetPadLeftMargin(0.105f);

    // Set the default number of divisions to show
    style->SetNdivisions(506, "xy");

    // Turn off xy grids
    style->SetPadGridX(false);
    style->SetPadGridY(false);

    // Set the tick mark style
    style->SetPadTickX(1);
    style->SetPadTickY(1);
    style->SetCanvasDefW(-1280);
    style->SetCanvasDefH(720);

    // Set and force the style
    gROOT->SetStyle("Allpix");
    gROOT->ForceStyle();
}


// Style for TH1
void SetDefaultStyle() {
    TStyle* style = gROOT->GetStyle("Allpix");

    style->SetOptTitle(0);

    style->SetPadTopMargin(0.06f);
    style->SetPadRightMargin(0.03f);

    style->SetOptLogx(false);
    style->SetOptLogy(false);
}


// Style for TH1 with logy
void SetLogyStyle() {
    TStyle* style = gROOT->GetStyle("Allpix");

    style->SetOptTitle(0);

    style->SetPadTopMargin(0.06f);
    style->SetPadRightMargin(0.03f);

    style->SetOptLogx(false);
    style->SetOptLogy(true);
}


// Style for TH1 with title
void SetTitleStyle() {
    TStyle* style = gROOT->GetStyle("Allpix");

    style->SetOptTitle(1);

    style->SetPadTopMargin(0.09f);
    style->SetPadRightMargin(0.03f);

    style->SetOptLogx(false);
    style->SetOptLogy(false);
}


// Style for TH2
void SetTH2Style() {
    TStyle* style = gROOT->GetStyle("Allpix");

    style->SetOptTitle(0);

    style->SetPadTopMargin(0.06f);
    style->SetPadRightMargin(0.14f);

    style->SetOptLogx(false);
    style->SetOptLogy(false);
}


// Style for TH2 with logx
void SetTH2LogxStyle() {
    TStyle* style = gROOT->GetStyle("Allpix");

    style->SetOptTitle(0);

    style->SetPadTopMargin(0.06f);
    style->SetPadRightMargin(0.14f);

    style->SetOptLogx(true);
    style->SetOptLogy(false);
}
