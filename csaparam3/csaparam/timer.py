# This file contains a timer class.

import time


# Class that easily allows measuring the duration of a process
class Timer:
    def __init__(self):
        self._t1 = 0.
        self._t2 = 0.
        self._p = 0.995


    # Starts the timer
    def Start(self) -> None:
        self._t1 = time.perf_counter()


    # Stops the timer
    def Stop(self) -> None:
        self._t2 = time.perf_counter()


    # Prints duration in pretty units
    def GetPrettyDuration(self) -> str:
        t_diff = self._t2 - self._t1

        if (t_diff < 1e-3 * self._p):
            out = f'{t_diff * 1e6 :.3g}us'
        elif (t_diff < 1. * self._p):
            out = f'{t_diff * 1e3 :.2g}ms'
        elif (t_diff < 100. * self._p):
            out = f'{t_diff :.2g}s'
        elif (t_diff / 60. <  100. * self._p):
            out = f'{t_diff / 60. :.2g}min'
        elif (t_diff / 3600. < 100. * self._p):
            out = f'{t_diff / 3600. :.2g}h'
        else:
            out = f'{t_diff / 3600. :.0f}h'

        return out
