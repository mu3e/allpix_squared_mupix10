# This file contains all the analysis functions.

import os.path
import ROOT

from .analysis_private.classes import InjAnaRes, Fe55AnaRes
from .analysis_private.injection import analysis_injection
from .analysis_private.fe55 import analysis_fe55


# Analyse injection and Fe55
def FullAnalysis(tfile_fitres: ROOT.TFile, tfile_output: ROOT.TFile) -> None:
    # enable MT for RDataFrame
    ROOT.EnableImplicitMT()
    # use Minuit2 with Minimize for fitting
    ROOT.Math.MinimizerOptions.SetDefaultMinimizer('Minuit2', 'Minimize')

    # get TTree with fit results
    ttree_fitres = tfile_fitres.Get('fitres')

    # analysis for injection
    inj_ana_res = InjectionAnalysis(ttree_fitres, tfile_output)

    # analysis for Fe55
    Fe55Analysis(ttree_fitres, tfile_output, inj_ana_res)


# Analyse injection
def InjectionAnalysis(ttree_fitres: ROOT.TTree, tfile_output: ROOT.TFile) -> InjAnaRes:
    return analysis_injection(ttree_fitres, tfile_output)


# Analyse Fe55
def Fe55Analysis(ttree_fitres: ROOT.TTree, tfile_output: ROOT.TFile, inj_ana_res: InjAnaRes) -> Fe55AnaRes:
    return analysis_fe55(ttree_fitres, tfile_output, inj_ana_res)
