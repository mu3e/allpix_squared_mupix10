# This file contains the used unit system.

from scipy import constants as SI


# Base units for best floating-point precision

us = 1.
mV = 1.
e = 1.


# SI units derived from base units

s = 1e6 * us
V = 1e3 * mV
C = 1. / SI.e
F = V / C


# Derived units

aC = 1e-18 * C
fF = aC / mV
eV = e * V
