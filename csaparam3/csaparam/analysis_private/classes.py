# This file contains result classes for the analysis.

import ROOT

from .. import units


class InjAnaRes:
    def __init__(self):
        self.A_m_inj = 0.
        self.A_m_inj_err = 0.
        self.A_mu_inj = 0.
        self.A_mu_inj_err = 0.
        self.A_c = 0.
        self.A_c_err = 0.
        self.chi2 = 0.
        self.ndf = 0
        self.tf1_Ainjv = None
        self.rdf_inj = None


    def SetAminj(self, A_m_inj: float, A_m_inj_err: float) -> None:
        self.A_m_inj = A_m_inj
        self.A_m_inj_err = A_m_inj_err

        A_m_inj_obj = ROOT.std.make_tuple(A_m_inj, A_m_inj_err)
        ROOT.gDirectory.WriteObject(A_m_inj_obj, 'A_m_inj__(mV/mV)')

        print(f' InjAnaRes: A_m_inj={A_m_inj :.4f} A_m_inj_err={A_m_inj_err :.4f} (mV/mV)')


    def SetAc(self, A_c: float, A_c_err: float) -> None:
        self.A_c = A_c
        self.A_c_err = A_c_err

        A_c_obj = ROOT.std.make_tuple(A_c, A_c_err)
        ROOT.gDirectory.WriteObject(A_c_obj, 'A_c__(mV)')

        print(f' InjAnaRes: A_c={A_c :.1f} A_c_err={A_c_err :.1f} (mV)')


    def SetAmuinj(self, A_mu_inj: float, A_mu_inj_err: float) -> None:
        self.A_mu_inj = A_mu_inj
        self.A_mu_inj_err = A_mu_inj_err

        A_mu_inj_obj = ROOT.std.make_tuple(A_mu_inj, A_mu_inj_err)
        ROOT.gDirectory.WriteObject(A_mu_inj_obj, 'A_mu_inj__(mV/mV)')

        print(f' InjAnaRes: A_mu_inj={A_mu_inj :.0f} A_mu_inj_err={A_mu_inj_err :.0f} (mV)')


    def SetChiNdf(self, chi2: float, ndf: float) -> None:
        self.chi2 = chi2
        self.ndf = ndf

        chi2_ndf_obj = ROOT.std.make_tuple(chi2, ndf)
        ROOT.gDirectory.WriteObject(chi2_ndf_obj, 'chi2__ndf')

        print(f' InjAnaRes: chi2={chi2 :.1f} ndf={ndf :.0f} chi2red={chi2/ndf :.2f}')


    def StoreTF1(self, tf1_Ainjv: ROOT.TF1) -> None:
        self.tf1_Ainjv = tf1_Ainjv


    def StoreRDF(self, rdf_inj: ROOT.RDataFrame) -> None:
        self.rdf_inj = rdf_inj


class Fe55AnaRes:
    def __init__(self):
        self.A_cal = 0.
        self.A_cal_err = 0.
        self.Q_cal = 0.
        self.Q_cal_err = 0.
        self.U_cal = 0.
        self.U_cal_err = 0.
        self.C_inj = 0.
        self.C_inj_err = 0.
        self.A_m = 0.
        self.A_m_err = 0.
        self.A_mu = 0.
        self.A_mu_err = 0.


    def SetQcal(self, Q_cal: float, Q_cal_err: float) -> None:
        self.Q_cal = Q_cal
        self.Q_cal_err = Q_cal_err

        Q_cal_obj = ROOT.std.make_tuple(Q_cal, Q_cal_err)
        ROOT.gDirectory.WriteObject(Q_cal_obj, 'Q_cal__(e)')

        print(f' Fe55AnaRes: Q_cal={Q_cal :.0f} Q_cal_err={Q_cal_err :.0f} (e)')


    def SetAcal(self, A_cal: float, A_cal_err: float) -> None:
        self.A_cal = A_cal
        self.A_cal_err = A_cal_err

        A_cal_obj = ROOT.std.make_tuple(A_cal, A_cal_err)
        ROOT.gDirectory.WriteObject(A_cal_obj, 'A_cal__(mV)')

        print(f' Fe55AnaRes: A_cal={A_cal :.1f} A_cal_err={A_cal_err :.1f} (mV)')


    def SetUcal(self, U_cal: float, U_cal_err: float) -> None:
        self.U_cal = U_cal
        self.U_cal_err = U_cal_err

        U_cal_obj = ROOT.std.make_tuple(U_cal, U_cal_err)
        ROOT.gDirectory.WriteObject(U_cal_obj, 'U_cal__(mV)')

        print(f' Fe55AnaRes: U_cal={U_cal :.1f} U_cal_err={U_cal_err :.1f} (mV)')


    def SetCinj(self, C_inj: float, C_inj_err: float) -> None:
        self.C_inj = C_inj
        self.C_inj_err = C_inj_err

        C_inj_obj = ROOT.std.make_tuple(C_inj, C_inj_err)
        ROOT.gDirectory.WriteObject(C_inj_obj, 'C_inj__(e/mV)')

        print(f' Fe55AnaRes: C_inj={C_inj :.2f} C_inj_err={C_inj_err :.2f} (e/mV)')

        C_inj_fF = C_inj/units.fF
        C_inj_fF_err = C_inj_err/units.fF

        C_inj_fF_obj = ROOT.std.make_tuple(C_inj_fF, C_inj_fF_err)
        ROOT.gDirectory.WriteObject(C_inj_fF_obj, 'C_inj__(fF)')

        print(f' Fe55AnaRes: C_inj={C_inj_fF :.3f} C_inj_err={C_inj_fF_err :.3f} (fF)')


    def SetAm(self, A_m: float, A_m_err: float) -> None:
        self.A_m = A_m
        self.A_m_err = A_m_err

        A_m_obj = ROOT.std.make_tuple(A_m, A_m_err)
        ROOT.gDirectory.WriteObject(A_m_obj, 'A_m__(mV/e)')

        print(f' Fe55AnaRes: A_m={A_m :.4f} A_m_err={A_m_err :.4f} (mV/e)')


    def SetAmu(self, A_mu: float, A_mu_err: float) -> None:
        self.A_mu = A_mu
        self.A_mu_err = A_mu_err

        A_mu_obj = ROOT.std.make_tuple(A_mu, A_mu_err)
        ROOT.gDirectory.WriteObject(A_mu_obj, 'A_mu__(e)')

        print(f' Fe55AnaRes: A_mu={A_mu :.0f} A_mu_err={A_mu_err :.0f} (e)')
