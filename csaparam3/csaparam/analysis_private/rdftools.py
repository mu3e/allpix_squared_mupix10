# This file contains some useful functions when working with RDataFrames.


from array import array
import numpy as np
import ROOT

from .. import constants


# Creates a TGraphErrors from an RDataFrame given the branch names for x, y, ey and ex
def GraphErrors(rdf: ROOT.RDataFrame, x: str, y: str, ey: str = '', ex: str = '') -> ROOT.TGraphErrors:
    # check if with errors for x and y
    with_ey = (ey != '')
    with_ex = (ex != '')

    # branches to fetch from rdf
    branches = [x, y]
    if with_ex:
        branches.append(ex)
    if with_ey:
        branches.append(ey)

    # get number of entries
    entries = rdf.Count()

    # get columns as numpy array
    cols = rdf.AsNumpy(branches)

    # get arrays for x and y
    vec_x = array('f', cols[x])
    vec_y = array('f', cols[y])

    # get array for ey and ex or set to zeros
    vec_ey = array('f', [])
    if with_ey:
        vec_ey.fromlist(cols[ey].tolist())
    else:
        vec_ey.fromlist(np.zeros(entries.GetValue()).tolist())
    vec_ex = array('f', [])
    if with_ex:
        vec_ex.fromlist(cols[ex].tolist())
    else:
        vec_ex.fromlist(np.zeros(entries.GetValue()).tolist())

    # create and return new graph
    graph = ROOT.TGraphErrors(entries.GetValue(), vec_x, vec_y, vec_ex, vec_ey)
    graph.SetName(f'tgraph_{y}_vs_{x}')
    graph.SetTitle(f'{y} vs {x};{x};{y}')
    return graph


# Creates useful definitions for fitresult RDataFrame
def create_rdf_definitions(rdf: ROOT.RDataFrame) -> ROOT.RDataFrame:
    rdf = rdf.Define('chi2red', 'chi2/ndf')
    return rdf


# Use Fe55 entries from RDataFrame
def filter_fe55(rdf: ROOT.RDataFrame) -> ROOT.RDataFrame:
    return rdf.Filter(f'injv == {constants.injv_fe55}')


# Use injection entries from RDataFrame
def filter_injv(rdf: ROOT.RDataFrame) -> ROOT.RDataFrame:
    return rdf.Filter('injv > 0.')


# Create Histo2D with automatic model
def Histo2D(rdf: ROOT.RDataFrame, x_branch: str, y_branch: str):
    x_min = rdf.Min(x_branch)
    x_max = rdf.Max(x_branch)
    y_min = rdf.Min(y_branch)
    y_max = rdf.Max(y_branch)

    th2dm = ROOT.RDF.TH2DModel(f'th2dmodel_{y_branch}_vs_{x_branch}', f'{y_branch} vs {x_branch}', 128, x_min.GetValue(), x_max.GetValue(), 128, y_min.GetValue(), y_max.GetValue())

    return rdf.Histo2D(th2dm, x_branch, y_branch)
