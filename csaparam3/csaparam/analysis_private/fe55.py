# This file contains the analysis function for Fe55.

import os.path
from array import array
import numpy as np
import ROOT

from .classes import InjAnaRes, Fe55AnaRes
from . import rdftools
from .. import roottools
from .. import units


def analysis_fe55(ttree_fitres: ROOT.TTree, tfile_output: ROOT.TFile, inj_ana_res: InjAnaRes) -> Fe55AnaRes:
    analyser = Fe55Analyser(ttree_fitres, tfile_output, inj_ana_res)
    analyser.GetAcal()
    analyser.GetQcal()
    analyser.CinjCalculation()
    analyser.ConvertInjParams()
    analyser.ConvertInjvGraph()
    return analyser.Return()


class Fe55Analyser:
    def __init__(self, ttree_fitres: ROOT.TTree, tfile_output: ROOT.TFile, inj_ana_res: InjAnaRes):
        _rdf = ROOT.RDataFrame(ttree_fitres)
        self.rdf = rdftools.filter_fe55(rdftools.create_rdf_definitions(_rdf))

        self.inj_ana_res = inj_ana_res
        self.fe55_ana_res = Fe55AnaRes()

        roottools.MakeAndChangeDir(tfile_output, 'fe55')


    def GetQcal(self) -> None:
        current_dir = ROOT.gDirectory.GetDirectory('')
        tfile_cal_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', 'fe55', 'Fe55ChargeDistribution.root')
        tfile_cal = ROOT.TFile(tfile_cal_path, 'READ')

        tf1_fe55cf = tfile_cal.Get('k_alpha_fit')

        Q_cal = tf1_fe55cf.GetParameter('Mean')
        Q_cal_err = tf1_fe55cf.GetParameter('Sigma')

        tfile_cal.Close()
        current_dir.cd()

        self.fe55_ana_res.SetQcal(Q_cal, Q_cal_err)


    def GetAcal(self) -> None:
        as_numpy = self.rdf.AsNumpy(['A', 'A_err'])

        if len(as_numpy['A']) != 1 or len(as_numpy['A_err']) != 1:
            raise Exception('Fe55Analyser: A or A_err not a single value')

        A_cal = as_numpy['A'][0]
        A_cal_err = as_numpy['A_err'][0]

        self.fe55_ana_res.SetAcal(A_cal, A_cal_err)


    def CinjCalculation(self) -> None:
        tf1_Ainjv = self.inj_ana_res.tf1_Ainjv

        tf1_Ainjv.SetParameter('A_m_inj', self.inj_ana_res.A_m_inj - self.inj_ana_res.A_m_inj_err)
        tf1_Ainjv.SetParameter('A_c', self.inj_ana_res.A_c - self.inj_ana_res.A_c_err)
        tf1_Ainjv.SetParameter('A_mu_inj', self.inj_ana_res.A_mu_inj + self.inj_ana_res.A_mu_inj_err)
        U_cal_errp = tf1_Ainjv.GetX(self.fe55_ana_res.A_cal + self.fe55_ana_res.A_cal_err, 200., 400.)

        tf1_Ainjv.SetParameter('A_m_inj', self.inj_ana_res.A_m_inj + self.inj_ana_res.A_m_inj_err)
        tf1_Ainjv.SetParameter('A_c', self.inj_ana_res.A_c + self.inj_ana_res.A_c_err)
        tf1_Ainjv.SetParameter('A_mu_inj', self.inj_ana_res.A_mu_inj - self.inj_ana_res.A_mu_inj_err)
        U_cal_errm = tf1_Ainjv.GetX(self.fe55_ana_res.A_cal - self.fe55_ana_res.A_cal_err, 200., 400.)

        U_cal_err = 0.5 * (U_cal_errp - U_cal_errm)

        tf1_Ainjv.SetParameter('A_m_inj', self.inj_ana_res.A_m_inj)
        tf1_Ainjv.SetParameter('A_c', self.inj_ana_res.A_c)
        tf1_Ainjv.SetParameter('A_mu_inj', self.inj_ana_res.A_mu_inj)
        U_cal = tf1_Ainjv.GetX(self.fe55_ana_res.A_cal, 200., 400.)

        self.fe55_ana_res.SetUcal(U_cal, U_cal_err)

        C_inj = self.fe55_ana_res.Q_cal / U_cal
        C_inj_err = np.sqrt(self.fe55_ana_res.Q_cal_err**2 + (C_inj * U_cal_err)**2) / U_cal

        self.fe55_ana_res.SetCinj(C_inj, C_inj_err)


    def ConvertInjParams(self) -> None:
        A_m = self.inj_ana_res.A_m_inj / self.fe55_ana_res.C_inj
        A_m_err = np.sqrt(self.inj_ana_res.A_m_inj_err**2 + (A_m * self.fe55_ana_res.C_inj_err)**2) / self.fe55_ana_res.C_inj

        self.fe55_ana_res.SetAm(A_m, A_m_err)

        A_mu = self.inj_ana_res.A_mu_inj * self.fe55_ana_res.C_inj
        A_mu_err = np.sqrt((self.inj_ana_res.A_mu_inj_err * self.fe55_ana_res.C_inj)**2 + (self.inj_ana_res.A_mu_inj * self.fe55_ana_res.C_inj_err)**2)

        self.fe55_ana_res.SetAmu(A_mu, A_mu_err)


    def ConvertInjvGraph(self) -> None:
        rdf_inj = self.inj_ana_res.rdf_inj

        rdf_inj = rdf_inj.Define('qinj', f'injv*{self.fe55_ana_res.C_inj}')
        rdf_inj = rdf_inj.Define('qinj_err', f'injv*{self.fe55_ana_res.C_inj_err}')

        graph = rdftools.GraphErrors(rdf_inj, 'qinj', 'A', 'A_err', 'qinj_err')

        tf1_Aqinj = ROOT.TF1('tf1_Aqinj', '([A_m] * x + [A_c]) * (1. - exp(-x/[A_mu]))', 0., 8700.)
        tf1_Aqinj.SetParameter('A_m', self.fe55_ana_res.A_m)
        tf1_Aqinj.SetParameter('A_c', self.inj_ana_res.A_c)
        tf1_Aqinj.SetParameter('A_mu', self.fe55_ana_res.A_mu)

        tf1_Aqinj.Write('A_q_dep_fit')
        graph.Write('A_q_dep')


    def Return(self) -> Fe55AnaRes:
        return self.fe55_ana_res
