# This file contains the analysis function for the injection waveforms.

import ROOT

from .classes import InjAnaRes
from . import rdftools
from .. import roottools


def analysis_injection(ttree_fitres: ROOT.TTree, tfile_output: ROOT.TFile) -> InjAnaRes:
    analyser = InjAnalyser(ttree_fitres, tfile_output)
    analyser.Ainjdep()
    return analyser.Return()


class InjAnalyser:
    def __init__(self, ttree_fitres: ROOT.TTree, tfile_output: ROOT.TFile):
        _rdf = ROOT.RDataFrame(ttree_fitres)
        self.rdf = rdftools.filter_injv(rdftools.create_rdf_definitions(_rdf))

        self.inj_ana_res = InjAnaRes()

        roottools.MakeAndChangeDir(tfile_output, 'injection')


    def Ainjdep(self) -> None:
        graph = rdftools.GraphErrors(self.rdf, 'injv', 'A', 'A_err')

        tf1_Ainjv = ROOT.TF1('tf1_Ainjv', '([A_m_inj] * x + [A_c]) * (1. - exp(-x/[A_mu_inj]))', 0., 1500.)
        tf1_Ainjv.SetParameter('A_m_inj', 0.5)
        tf1_Ainjv.SetParameter('A_c', 100.)
        tf1_Ainjv.SetParameter('A_mu_inj', 500.)

        graph.Fit(tf1_Ainjv, 'QN')
        self.inj_ana_res.SetChiNdf(tf1_Ainjv.GetChisquare(), tf1_Ainjv.GetNDF())

        A_m_inj = tf1_Ainjv.GetParameter('A_m_inj')
        A_m_inj_err = tf1_Ainjv.GetParError(tf1_Ainjv.GetParNumber('A_m_inj'))
        self.inj_ana_res.SetAminj(A_m_inj, A_m_inj_err)

        A_c = tf1_Ainjv.GetParameter('A_c')
        A_c_err = tf1_Ainjv.GetParError(tf1_Ainjv.GetParNumber('A_c'))
        self.inj_ana_res.SetAc(A_c, A_c_err)

        A_mu_inj = tf1_Ainjv.GetParameter('A_mu_inj')
        A_mu_inj_err = tf1_Ainjv.GetParError(tf1_Ainjv.GetParNumber('A_mu_inj'))
        self.inj_ana_res.SetAmuinj(A_mu_inj, A_mu_inj_err)

        self.inj_ana_res.StoreTF1(tf1_Ainjv)
        self.inj_ana_res.StoreRDF(self.rdf)

        tf1_Ainjv.Write('A_inj_dep_fit')
        graph.Write('A_inj_dep')


    def Return(self) -> InjAnaRes:
        return self.inj_ana_res
