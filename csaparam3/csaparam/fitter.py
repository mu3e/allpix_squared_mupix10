# This file contains functions for fitting.

import os.path
from array import array
import ROOT

from . import constants


# Class holding a single fit result
class FitRes:
    def __init__(self, tf1_ff: ROOT.TF1):
        self.A = tf1_ff.GetParameter(tf1_ff.GetParNumber('A'))
        self.A_err = tf1_ff.GetParError(tf1_ff.GetParNumber('A'))
        self.t_0 = tf1_ff.GetParameter(tf1_ff.GetParNumber('t_0'))
        self.U_0 = tf1_ff.GetParameter(tf1_ff.GetParNumber('U_0'))
        self.t_0 = tf1_ff.GetParameter(tf1_ff.GetParNumber('t_0'))
        self.chi2 = tf1_ff.GetChisquare()
        self.ndf = tf1_ff.GetNDF()


# Class holding a TTree with the fit results
class FitResTree:
    def __init__(self, suffix: str = ''):
        # Create TTree
        ttree_name = 'fitres' if suffix == '' else f'fitres_{suffix}'
        self.ttree = ROOT.TTree(ttree_name, 'fit results')

        # Create arrays for branches
        self.b_injv = array('d', [0.])
        self.b_A = array('f', [0.])
        self.b_A_err = array('f', [0])
        self.b_t_0 = array('f', [0.])
        self.b_U_0 = array('f', [0.])
        self.b_chi2 = array('f', [0.])
        self.b_ndf = array('f', [0.])

        # Set branch addresses
        self.ttree.Branch('injv', self.b_injv, 'injv/D')
        self.ttree.Branch('A', self.b_A, 'A/F')
        self.ttree.Branch('A_err', self.b_A_err, 'A_err/F')
        self.ttree.Branch('t_0', self.b_t_0, 't_0/F')
        self.ttree.Branch('U_0', self.b_U_0, 'U_0/F')
        self.ttree.Branch('chi2', self.b_chi2, 'chi2/F')
        self.ttree.Branch('ndf', self.b_ndf, 'ndf/F')


    # Fill TTree from FitRes
    def Fill(self, fitres: FitRes, injv: float) -> None:
        self.b_injv[0] = injv

        # Read data from FitRes class
        self.b_A[0] = fitres.A
        self.b_A_err[0] = fitres.A_err
        self.b_t_0[0] = fitres.t_0
        self.b_U_0[0] = fitres.U_0
        self.b_chi2[0] = fitres.chi2
        self.b_ndf[0] = fitres.ndf

        # Fill TTree
        self.ttree.Fill()


    # Write TTree
    def Write(self) -> None:
        self.ttree.Write()


# Class providing functions for fitting
class Fitter:
    tf1_ff = None

    def __init__(self, waveform: ROOT.TProfile):
        self.waveform = waveform

        if Fitter.tf1_ff is None:
            ROOT.Math.MinimizerOptions.SetDefaultPrintLevel(constants.fitter_print_level)
            ROOT.Math.MinimizerOptions.SetDefaultMinimizer(constants.fitter_type, constants.fitter2_min_algo)
            ROOT.Math.MinimizerOptions.SetDefaultTolerance(constants.fitter2_tolerance)
            ROOT.Math.MinimizerOptions.SetDefaultStrategy(constants.fitter_strategy)

            csaparam_path = os.path.dirname(__file__)
            ROOT.gInterpreter.ProcessLine('#include \"' + os.path.join(csaparam_path, 'MuPixPulseFF.hpp\"'))
            ROOT.gInterpreter.ProcessLine('#include \"' + os.path.join(csaparam_path, 'MuPixPulseHelper.hpp\"'))
            ROOT.gInterpreter.ProcessLine('#include \"' + os.path.join(csaparam_path, 'AllpixStyle.hpp\"'))

            tf1 = ROOT.TF1('mupix_pulse_ff', ROOT.MuPixPulseFitFunction, -1., 10., 10)
            tf1.SetParName(0, 'timestep')
            tf1.SetParName(1, 't_0')
            tf1.SetParName(2, 'U_0')
            tf1.SetParName(3, 'A')
            tf1.SetParName(4, 't_R')
            tf1.SetParName(5, 't_F')
            tf1.SetParName(6, 't_S')
            tf1.SetParName(7, 'Fb')
            tf1.SetParName(8, 'Fb_D')
            tf1.SetParName(9, 'U_sat')

            tf1.FixParameter(tf1.GetParNumber('timestep'), constants.ff_timestep_fitter2)
            tf1.FixParameter(tf1.GetParNumber('t_R'), constants.ff_t_R)
            tf1.FixParameter(tf1.GetParNumber('t_F'), constants.ff_t_F)
            tf1.FixParameter(tf1.GetParNumber('t_S'), constants.ff_t_S)
            tf1.FixParameter(tf1.GetParNumber('Fb'), constants.ff_Fb)
            tf1.FixParameter(tf1.GetParNumber('Fb_D'), constants.ff_Fb_D)
            tf1.FixParameter(tf1.GetParNumber('U_sat'), constants.ff_U_sat)

            tf1.SetParameter(tf1.GetParNumber('A'), constants.ff_A_min)
            tf1.SetParLimits(tf1.GetParNumber('A'), constants.ff_A_min, constants.ff_A_max)

            Fitter.tf1_ff = tf1


    # Fit waveform
    def Fit(self) -> FitRes:
        # set prefit
        ROOT.MuPixPulsePrefit(self.tf1_ff, self.waveform, constants.ff_U_0_bins, constants.ff_U_0_lim)

        # first fitting run
        ROOT.Math.MinimizerOptions.SetDefaultMinimizer(constants.fitter_type, constants.fitter1_min_algo)
        ROOT.Math.MinimizerOptions.SetDefaultTolerance(constants.fitter1_tolerance)
        self.tf1_ff.FixParameter(self.tf1_ff.GetParNumber('timestep'), constants.ff_timestep_fitter1)
        ROOT.MuPixPulseSetFitter1Range(self.tf1_ff, self.waveform, constants.ff_U_0_bins)
        self.waveform.Fit(self.tf1_ff, 'RBN')
        self.tf1_ff.FixParameter(self.tf1_ff.GetParNumber('t_0'), self.tf1_ff.GetParameter('t_0'))

        # second fitting run
        ROOT.Math.MinimizerOptions.SetDefaultMinimizer(constants.fitter_type, constants.fitter2_min_algo)
        ROOT.Math.MinimizerOptions.SetDefaultTolerance(constants.fitter2_tolerance)
        self.tf1_ff.FixParameter(self.tf1_ff.GetParNumber('timestep'), constants.ff_timestep_fitter2)
        ROOT.MuPixPulseSetFitter2Range(self.tf1_ff, self.waveform, constants.ff_U_0_bins)
        self.waveform.Fit(self.tf1_ff, 'RB')

        return FitRes(self.tf1_ff)


    # Draw waveform
    def Draw(self):
        self.waveform.Draw()
        self.tf1_ff.SetNpx(int((self.tf1_ff.GetXmax() - self.tf1_ff.GetXmin()) * constants.ff_draw_ppt))
        self.tf1_ff.Draw('same')
