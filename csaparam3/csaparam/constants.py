# This file contains some numbers used in the analysis.


from . import units

# Minimizer type for fitter
fitter_type = 'Minuit2'

# Minimizing strategy for fitter
fitter_strategy = 0

# Print level for fitter
fitter_print_level = 2

# Minimizer algorithm for first fitter
fitter1_min_algo = 'Simplex'

# Tolerance for first fitter
fitter1_tolerance = 0.01

# Minimizer algorithm for second fitter
fitter2_min_algo = 'Migrad'

# Tolerance for second fitter
fitter2_tolerance = 0.01


# Timestep for pulse shape fit function for first fitter
ff_timestep_fitter1 = 0.0001 * units.us

# Timestep for pulse shape fit function for second fitter
ff_timestep_fitter2 = 0.0001 * units.us

# Amount of bins to determine voltage offset for pulse shape fit function
ff_U_0_bins = 180

# Voltage offset difference limit in time offset calculation for pulse shape fit function
ff_U_0_lim = 1.5 * units.mV

# Amplitude parameter limits for pulse shape fit function
ff_A_min =  40. * units.mV
ff_A_max = 900. * units.mV

# Rise time parameter for pulse shape fit function
ff_t_R = 0.1 * units.us

# Fall time parameter for pulse shape fit function
ff_t_F = 4.8 * units.us

# Smoothing parameter or pulse shape fit function
ff_t_S = 0.1 * units.us

# Feedback fall rate for pulse shape fit function
ff_Fb = 23. * units.mV/units.us

# Feedback damping parameter for pulse shape fit function
ff_Fb_D = 2. * units.mV

# Saturation voltage for pulse shape fit function
ff_U_sat = 360. * units.mV

# Points per time to draw
ff_draw_ppt = 50. / units.us


# Injection voltage value for Fe55
injv_fe55 = -1.

# Injection voltage value for Sr0
injv_sr90 = -2.
