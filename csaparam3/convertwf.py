#!/usr/bin/python3

# This script converts the csv waveforms from the Tektronix DPO7254C
# to ROOT TProfiles and creates an averge waveform for each dataset.

#%% imports
import os.path
from glob import glob
from natsort import natsorted
import ROOT

from csaparam.timer import Timer
from csaparam import units
from csaparam import tools
from csaparam import roottools

#%% measure script duration
timer = Timer()
timer.Start()

#%% data settings
datadir_prefix = 'std'
datadirs = [
    '200mV',
    '300mV',
    '400mV',
    '500mV',
    '600mV',
    '700mV',
    '800mV',
    '900mV',
    '1000mV',
    '1100mV',
    '1200mV',
    '1300mV',
    '1400mV',
    'Fe55',
]

#%% function to convert one csv line to float pair
def convert_line(line: str) -> (float, float):
    columns = line.split(',')
    time = float(columns[-2]) * units.s
    voltage = float(columns[-1]) * units.V
    return (time, voltage)

#%% function to get binning
def get_binning(lines: list[str]) -> (int, float, float):
    # get information for binning from file
    entries = len(lines)
    t_first = convert_line(lines[0])[0]
    t_last = convert_line(lines[-1])[0]

    # calculate bin size
    t_bin_size = (t_last - t_first) / (entries - 1)

    # calculate minimum and maximum bin boundary
    t_min = t_first - 0.5 * t_bin_size
    t_max = t_last + 0.5 * t_bin_size

    return (entries, t_min, t_max)

#%% function to convert lines of file to tprofile
def convert_file(lines: list[str]) -> ROOT.TProfile:
    # get binning
    entries, t_min, t_max = get_binning(lines)

    # create new TProfile
    tprofile = ROOT.TProfile('tprofile', '', entries, t_min, t_max)

    # loop lines and fill TProfile
    for line in lines:
        time, voltage = convert_line(line)
        tprofile.Fill(time, voltage)

    return tprofile

#%% loop datadirs
total_count = 0
for datadir in datadirs:
    print(f'Starting conversion for {datadir}')
    timer_datadir = Timer()
    timer_datadir.Start()

    # create ROOT file
    tfile = roottools.OpenTFile(datadir_prefix, datadir, 'RECREATE')

    # read folder in alphanumeric order, assume only csv files
    datadir_path = os.path.join(tools.GetDatadirPrefixPath(datadir_prefix), datadir)
    csvfile_paths = natsorted(glob(os.path.join(datadir_path, '*.csv')))

    # variables for binning
    entries = 0
    t_min = 0.
    t_max = 0.

    # open first file, used for average binning
    with open(os.path.join(datadir_path, csvfile_paths[0]), mode='rt') as csvfile:
        # read all lines
        lines = csvfile.readlines()
        # get binning
        entries, t_min, t_max = get_binning(lines)

    # create TProfile for average
    tprofile_average = ROOT.TProfile('tprofile_average', '', entries, t_min, t_max)
    tprofile_average.SetErrorOption('s')
    tprofile_average.SetTitle('Average AmpOut;time [us];voltage [mV]')

    # create folder
    tfile.mkdir('single')
    tfile.cd('single')

    # loop files
    count = 0
    for csvfile_path in csvfile_paths:
        count += 1

        # read lines from file
        lines = list[str]()
        with open(os.path.join(datadir_path, csvfile_path), mode='rt') as csvfile:
            lines = csvfile.readlines()

        # convert to tprofile
        tprofile = convert_file(lines)

        # write tprofile to tfile
        tprofile.Write(str(count))

        # add tprofile to average
        tprofile_average.Add(tprofile)

        # free memory
        del lines
        tprofile.Delete()

    # write average tprofile and free memory
    tfile.cd()
    tprofile_average.Write('average')
    tprofile_average.Delete()

    # close ROOT file
    tfile.Close()
    tfile.Delete()

    # print duration
    total_count += count
    timer_datadir.Stop()
    print(f' converted {count} waveforms in {timer_datadir.GetPrettyDuration()}')

#%% print total duration
timer.Stop()
print(f'Finished converting {total_count} files in {timer.GetPrettyDuration()}')
