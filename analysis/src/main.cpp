#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <vector>
#include <numeric>
#include <string>

#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TDirectoryFile.h>
#include <TKey.h>
#include <TCanvas.h>
#include <TDirectory.h>
#include <TH2I.h>
#include <TH1I.h>
#include <TH1D.h>

#include <objects/PixelHit.hpp>
#include <objects/Pixel.hpp>
#include <objects/MCParticle.hpp>
#include <objects/PixelCharge.hpp>
#include <objects/MCTrack.hpp>


void analysis(const std::string& data_file_name) {
    // read allpix data
    auto* data_file = new TFile(data_file_name.c_str(), "READ");
    auto* pixel_hit_tree = static_cast<TTree*>(data_file->Get("PixelHit"));
    auto* mc_particle_tree = static_cast<TTree*>(data_file->Get("MCParticle"));
    auto* pixel_charge_tree = static_cast<TTree*>(data_file->Get("PixelCharge"));
    auto* mc_track_tree = static_cast<TTree*>(data_file->Get("MCTrack"));

    // pixel hit branch
    auto* pixel_hit_branch = pixel_hit_tree->FindBranch("detector");
    std::vector<allpix::PixelHit*> input_hits;
    pixel_hit_branch->SetObject(&input_hits);

    // mc particle branch
    auto* mc_particle_branch = mc_particle_tree->FindBranch("detector");
    std::vector<allpix::MCParticle*> input_particles;
    mc_particle_branch->SetObject(&input_particles);

    // pixel charge branch
    auto* pixel_charge_branch = pixel_charge_tree->FindBranch("detector");
    std::vector<allpix::PixelCharge*> input_charges;
    pixel_charge_branch->SetObject(&input_charges);

    // mc track branch
    auto* mc_track_branch = mc_track_tree->FindBranch("global");
    std::vector<allpix::MCTrack*> input_tracks;
    mc_track_branch->SetObject(&input_tracks);

    // read config
    auto* config = (TDirectoryFile*)(data_file->Get("config"));
    auto* csa_config = (TDirectoryFile*)(config->FindKey("MuPixDigitizer:detector")->ReadObj());
    auto* ts1_config_key = csa_config->FindKey("clock_bin_ts1");
    double clk_ts1 = std::stod((*(std::string*)ts1_config_key->ReadObj()));
    auto* ts2_config_key = csa_config->FindKey("clock_bin_ts2");
    double clk_ts2 = std::stod((*(std::string*)ts2_config_key->ReadObj()));
    auto* integration_time_config_key = csa_config->FindKey("integration_time");
    auto integration_time_string =  (*(std::string*)integration_time_config_key->ReadObj());
    double integration_time = std::stod(integration_time_string);
    integration_time *= integration_time_string.find("us") != std::string::npos ? 1e3 : 1.;

    // output file
    std::string output_file_name{};
    auto data_file_name_pos_last_dir = data_file_name.find_last_of("/") + 1;
    output_file_name = data_file_name.substr(0, data_file_name_pos_last_dir);
    output_file_name += "analysis_";
    output_file_name += data_file_name.substr(data_file_name_pos_last_dir, data_file_name.npos);

    // new trees for output
    auto* output_file = new TFile(output_file_name.c_str(), "RECREATE");
    output_file->cd();
    auto* output_event_tree = new TTree("event_info", "Event information");

    // event nr
    int event_nr;
    output_event_tree->Branch("event_nr", &event_nr);

    // ts1
    std::vector<double> ts1;
    output_event_tree->Branch("ts1", &ts1);

    // ts2
    std::vector<double> ts2;
    output_event_tree->Branch("ts2", &ts2);

    // tot
    std::vector<double> tot;
    output_event_tree->Branch("tot", &tot);

    // position
    std::vector<int> pixel_x;
    std::vector<int> pixel_y;
    output_event_tree->Branch("pixel_x", &pixel_x);
    output_event_tree->Branch("pixel_y", &pixel_y);

    // charge
    std::vector<int> charge;
    output_event_tree->Branch("charge", &charge);

    // cluster size
    int cluster_size;
    output_event_tree->Branch("cluster_size", &cluster_size);

    // tot sum
    double tot_sum;
    output_event_tree->Branch("tot_sum", &tot_sum);

    // charge sum
    int charge_sum;
    output_event_tree->Branch("charge_sum", &charge_sum);

    // primary particle pid
    int pdg_code;
    output_event_tree->Branch("pdg_code", &pdg_code);

    // initial kin energy
    double kin_energy;
    output_event_tree->Branch("kin_energy", &kin_energy);

    // count events
    int events_with_clusters = 0;
    int non_identifieable_events = 0;
    int no_primary_events = 0;

    // loop events
    const int _data_entries = pixel_hit_tree->GetEntries();
    for (int n = 0; n < _data_entries; ++n) {
        pixel_hit_tree->GetEntry(n);
        mc_particle_tree->GetEntry(n);
        pixel_charge_tree->GetEntry(n);
        mc_track_branch->GetEntry(n);

        cluster_size = input_hits.size();

        if (cluster_size == 0)
        {
            continue;
        }

        events_with_clusters++;
        event_nr = n + 1;

        ts1.clear();
        ts2.clear();
        tot.clear();
        pixel_x.clear();
        pixel_y.clear();
        charge.clear();
        ts1.reserve(cluster_size);
        ts2.reserve(cluster_size);
        tot.reserve(cluster_size);
        pixel_x.reserve(cluster_size);
        pixel_y.reserve(cluster_size);
        charge.reserve(cluster_size);
        tot_sum = 0.;
        charge_sum = 0;

        // loop hits in event
        for (auto& _hit : input_hits)
        {
            const auto _ts1 = _hit->getLocalTime() * clk_ts1;
            const auto _ts2 = _hit->getSignal() * clk_ts2;
            const auto _tot = _ts2 - _ts1;
            tot_sum += _tot;

            ts1.push_back(_ts1);
            ts2.push_back(_ts2);
            tot.push_back(_tot);

            const auto _index = _hit->getPixel().getIndex();
            pixel_x.push_back(_index.x());
            pixel_y.push_back(_index.y());

            const auto* _pixel_charge = _hit->getPixelCharge();
            const auto _charge = _pixel_charge->getCharge();
            charge.push_back(_charge);
            charge_sum += _charge;

            // primary particle, 0 if not identifieable
            if (_hit->getPrimaryMCParticles().size() == 1)
            {
                const auto particle = _hit->getPrimaryMCParticles()[0];
                pdg_code = particle->getParticleID();
                kin_energy = particle->getTrack()->getKineticEnergyInitial();

            }
            else if (_hit->getPrimaryMCParticles().size() == 0)
            {
                pdg_code = 0;
                kin_energy = 0;
                no_primary_events++;
            }
            else
            {
                pdg_code = 0;
                kin_energy = 0;
                non_identifieable_events++;
            }
        }

        output_event_tree->Fill();
    }
    output_event_tree->Write();

    // summarise
    std::cout << "Finished looping " << _data_entries << " entries" << std::endl;
    std::cout << "Events with cluster: " << events_with_clusters << std::endl;
    std::cout << "Events with cluster but no primaries: " << no_primary_events << std::endl;
    std::cout << "Events with more than one primary: " << non_identifieable_events << std::endl;

    // histograms
    auto* canvas = new TCanvas();

    // pdg_code filters
    auto pdg_code_eq_gamma = "pdg_code==22";
    auto pdg_code_eq_electron = "pdg_code==11";
    auto pdg_code_neq_zero = "pdg_code!=0";

    // bins for tot sum
    double max_tot_sum = output_event_tree->GetMaximum("tot_sum") + clk_ts2;
    auto nbinsx_tot_sum = static_cast<int>(std::ceil(max_tot_sum / clk_ts2));
    auto binstr_tot_sum = "(" + std::to_string(nbinsx_tot_sum) + ",0," + std::to_string(max_tot_sum) + ")";

    // bins for tot
    auto nbinsx_tot = static_cast<int>(std::ceil(integration_time / clk_ts2));
    auto binstr_tot = "(" + std::to_string(nbinsx_tot) + ",0," + std::to_string(nbinsx_tot * clk_ts2) + ")";

    // bins for cluster size
    auto max_cluster_size = static_cast<int>(output_event_tree->GetMaximum("cluster_size"));
    auto binstr_cluster_size = "(" + std::to_string(max_cluster_size + 2) + ",-0.5," + std::to_string(max_cluster_size + 1.5) + ")";

    // hitmap
    output_event_tree->Draw("pixel_y:pixel_x>>h_hitmap");
    auto* h_hitmap = (TH2I*)gDirectory->Get("h_hitmap");
    h_hitmap->SetTitle("Hitmap");
    h_hitmap->SetXTitle("Pixel x");
    h_hitmap->SetYTitle("Pixel y");
    h_hitmap->SetOption("colz");
    h_hitmap->SetStats(false);
    h_hitmap->Write();

    // kinetic energy
    output_event_tree->Draw("kin_energy>>h_kin_energy");
    auto* h_kin_energy = (TH1D*)gDirectory->Get("h_kin_energy");
    h_kin_energy->SetTitle("Kinetic Energy");
    h_kin_energy->SetXTitle("Kinetic Energy [MeV]");
    h_kin_energy->SetYTitle("Entries");
    h_kin_energy->Write();

    // charge sum
    output_event_tree->Draw("charge_sum>>h_charge_sum");
    auto* h_charge_sum = (TH1D*)gDirectory->Get("h_charge_sum");
    h_charge_sum->SetTitle("Total Charge");
    h_charge_sum->SetXTitle("Charge [e]");
    h_charge_sum->SetYTitle("Entries");
    h_charge_sum->Write();

    // tot sum
    auto draw_tot_sum = "tot_sum>>h_tot_sum" + binstr_tot_sum;
    output_event_tree->Draw(draw_tot_sum.c_str());
    auto* h_tot_sum = (TH1D*)gDirectory->Get("h_tot_sum");
    h_tot_sum->SetTitle("ToT sum (All particles)");
    h_tot_sum->SetXTitle("ToT [ns]");
    h_tot_sum->SetYTitle("Entries");
    h_tot_sum->Write();

    // charge sum vs kin energy
    output_event_tree->Draw("charge_sum:kin_energy>>h_charge_sum_vs_kin_energy", pdg_code_neq_zero);
    auto* h_charge_sum_vs_kin_energy = (TH2I*)gDirectory->Get("h_charge_sum_vs_kin_energy");
    h_charge_sum_vs_kin_energy->SetTitle("Total Charge vs Kinetic Energy");
    h_charge_sum_vs_kin_energy->SetXTitle("Kinetic Energy [MeV]");
    h_charge_sum_vs_kin_energy->SetYTitle("Charge [e]");
    h_charge_sum_vs_kin_energy->SetOption("colz");
    h_charge_sum_vs_kin_energy->SetStats(false);
    h_charge_sum_vs_kin_energy->Write();

    // tot sum vs kin energy
    output_event_tree->Draw("tot_sum:kin_energy>>h_tot_sum_vs_kin_energy", pdg_code_neq_zero);
    auto* h_tot_sum_vs_kin_energy = (TH2I*)gDirectory->Get("h_tot_sum_vs_kin_energy");
    h_tot_sum_vs_kin_energy->SetTitle("ToT sum vs Kinetic Energy");
    h_tot_sum_vs_kin_energy->SetXTitle("Kinetic Energy [MeV]");
    h_tot_sum_vs_kin_energy->SetYTitle("ToT [ns]");
    h_tot_sum_vs_kin_energy->SetOption("colz");
    h_tot_sum_vs_kin_energy->SetStats(false);
    h_tot_sum_vs_kin_energy->Write();

    // charge vs kin energy
    output_event_tree->Draw("charge[]:kin_energy>>h_charge_vs_kin_energy", pdg_code_neq_zero);
    auto* h_charge_vs_kin_energy = (TH2I*)gDirectory->Get("h_charge_vs_kin_energy");
    h_charge_vs_kin_energy->SetTitle("Pixel Charge vs Kinetic Energy");
    h_charge_vs_kin_energy->SetXTitle("Kinetic Energy [MeV]");
    h_charge_vs_kin_energy->SetYTitle("Charge [e]");
    h_charge_vs_kin_energy->SetOption("colz");
    h_charge_vs_kin_energy->SetStats(false);
    h_charge_vs_kin_energy->Write();

    // tot vs kin energy
    output_event_tree->Draw("tot[]:kin_energy>>h_tot_vs_kin_energy", pdg_code_neq_zero);
    auto* h_tot_vs_kin_energy = (TH2I*)gDirectory->Get("h_tot_vs_kin_energy");
    h_tot_vs_kin_energy->SetTitle("Pixel ToT vs Kinetic Energy");
    h_tot_vs_kin_energy->SetXTitle("Kinetic Energy [MeV]");
    h_tot_vs_kin_energy->SetYTitle("ToT [ns]");
    h_tot_vs_kin_energy->SetOption("colz");
    h_tot_vs_kin_energy->SetStats(false);
    h_tot_vs_kin_energy->Write();

    // tot vs charge
    output_event_tree->Draw("tot:charge>>h_tot_vs_charge");
    auto* h_tot_vs_charge = (TH2I*)gDirectory->Get("h_tot_vs_charge");
    h_tot_vs_charge->SetTitle("Pixel ToT vs Pixel Charge");
    h_tot_vs_charge->SetXTitle("Charge [e]");
    h_tot_vs_charge->SetYTitle("ToT [ns]");
    h_tot_vs_charge->SetOption("colz");
    h_tot_vs_charge->SetStats(false);
    h_tot_vs_charge->Write();

    // charge
    output_event_tree->Draw("charge>>h_charge");
    auto* h_charge = (TH1D*)gDirectory->Get("h_charge");
    h_charge->SetTitle("Pixel Charge (All particles)");
    h_charge->SetXTitle("Charge [e]");
    h_charge->SetYTitle("Entries");
    h_charge->Write();

    // charge gamma
    output_event_tree->Draw("charge>>h_charge_gamma", pdg_code_eq_gamma);
    auto* h_charge_gamma = (TH1D*)gDirectory->Get("h_charge_gamma");
    if (h_charge_gamma->GetEntries() > 0) {
        h_charge_gamma->SetTitle("Pixel Charge (Gammas)");
        h_charge_gamma->SetXTitle("Charge [e]");
        h_charge_gamma->SetYTitle("Entries");
        h_charge_gamma->Write();
    }

    // charge electron
    output_event_tree->Draw("charge>>h_charge_electron", pdg_code_eq_electron);
    auto* h_charge_electron = (TH1D*)gDirectory->Get("h_charge_electron");
    if (h_charge_electron->GetEntries() > 0) {
        h_charge_electron->SetTitle("Pixel Charge (Electrons)");
        h_charge_electron->SetXTitle("Charge [e]");
        h_charge_electron->SetYTitle("Entries");
        h_charge_electron->Write();
    }

    // tot
    auto draw_tot = "tot>>h_tot" + binstr_tot;
    output_event_tree->Draw(draw_tot.c_str());
    auto* h_tot = (TH1D*)gDirectory->Get("h_tot");
    h_tot->SetTitle("ToT (All particles)");
    h_tot->SetXTitle("ToT [ns]");
    h_tot->SetYTitle("Entries");
    h_tot->Write();

    // tot gamma
    auto draw_tot_gamma = "tot>>h_tot_gamma" + binstr_tot;
    output_event_tree->Draw(draw_tot_gamma.c_str(), pdg_code_eq_gamma);
    auto* h_tot_gamma = (TH1D*)gDirectory->Get("h_tot_gamma");
    if (h_tot_gamma->GetEntries() > 0) {
        h_tot_gamma->SetTitle("ToT (Gammas)");
        h_tot_gamma->SetXTitle("ToT [ns]");
        h_tot_gamma->SetYTitle("Entries");
        h_tot_gamma->Write();
    }

    // tot electron
    auto draw_tot_electron = "tot>>h_tot_electron" + binstr_tot;
    output_event_tree->Draw(draw_tot_electron.c_str(), pdg_code_eq_electron);
    auto* h_tot_electron = (TH1D*)gDirectory->Get("h_tot_electron");
    if (h_tot_electron->GetEntries() > 0) {
        h_tot_electron->SetTitle("ToT (Electrons)");
        h_tot_electron->SetXTitle("ToT [ns]");
        h_tot_electron->SetYTitle("Entries");
        h_tot_electron->Write();
    }

    // cluster size
    auto draw_cluster_size = "cluster_size>>h_cluster_size" + binstr_cluster_size;
    output_event_tree->Draw(draw_cluster_size.c_str());
    auto* h_cluster_size = (TH1I*)gDirectory->Get("h_cluster_size");
    h_cluster_size->SetTitle("Cluster Size (All particles)");
    h_cluster_size->SetXTitle("Cluster Size");
    h_cluster_size->SetYTitle("Entries");
    h_cluster_size->Write();

    // cluster size gamma
    auto draw_cluster_size_gamma = "cluster_size>>h_cluster_size_gamma" + binstr_cluster_size;
    output_event_tree->Draw(draw_cluster_size_gamma.c_str(), pdg_code_eq_gamma);
    auto* h_cluster_size_gamma = (TH1I*)gDirectory->Get("h_cluster_size_gamma");
    if (h_cluster_size_gamma->GetEntries() > 0)
    {
        h_cluster_size_gamma->SetTitle("Cluster Size (Gammas)");
        h_cluster_size_gamma->SetXTitle("Cluster Size");
        h_cluster_size_gamma->SetYTitle("Entries");
        h_cluster_size_gamma->Write();
    }

    // cluster size electron
    auto draw_cluster_size_electron = "cluster_size>>h_cluster_size_electron" + binstr_cluster_size;
    output_event_tree->Draw(draw_cluster_size_electron.c_str(), pdg_code_eq_electron);
    auto* h_cluster_size_electron = (TH1I*)gDirectory->Get("h_cluster_size_electron");
    if (h_cluster_size_electron->GetEntries() > 0)
    {
        h_cluster_size_electron->SetTitle("Cluster Size (Electrons)");
        h_cluster_size_electron->SetXTitle("Cluster Size");
        h_cluster_size_electron->SetYTitle("Entries");
        h_cluster_size_electron->Write();
    }

    output_file->Close();
    data_file->Close();
    delete canvas;
}

int main(int argc, char *argv[])
{
    bool exit = EXIT_SUCCESS;

    if (argc == 2)
    {
        if (std::filesystem::exists(argv[1]))
        {
            analysis(argv[1]);
        }
        else {
            std::cout << "Error: file \"" << argv[1] << "\" doesn't exist" << std::endl;
            exit = EXIT_FAILURE;
        }
    }
    else
    {
        std::cout << "Error: to many arguments" << std::endl;
        exit = EXIT_FAILURE;
    }

    return exit;
}
