cmake_minimum_required(VERSION 3.8)
project(analysis)

set(CMAKE_CXX_STANDARD 17)

find_package(ROOT REQUIRED)
include(${ROOT_USE_FILE})

find_package(Allpix REQUIRED)
include_directories(${ALLPIX_INCLUDE_DIR})
set(ALLPIX_LIBRARIES ${ALLPIX_LIBRARY_DIR}/libAllpixObjects.so)

include_directories(${PROJECT_SOURCE_DIR}/src)
file(GLOB src ${PROJECT_SOURCE_DIR}/src/*)

add_executable(analysis ${src})
target_compile_options(analysis PUBLIC -O3 -march=native)
target_link_libraries(analysis ${ROOT_LIBRARIES})
target_link_libraries(analysis ${ALLPIX_LIBRARIES})
