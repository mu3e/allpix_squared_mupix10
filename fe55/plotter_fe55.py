#!/usr/bin/python3

import ROOT


def Fe55ChargeDistribution() -> ROOT.TCanvas:
    ROOT.SetDefaultStyle()
    canvas = ROOT.TCanvas('canvas_Fe55ChargeDistribution', 'canvas_Fe55ChargeDistribution', -1280, 720)

    tfile = ROOT.TFile('output_fe55_tcad_hhf/analysis_data.root', 'READ')
    ttree = tfile.Get('event_info')

    ttree.Draw('charge>>h_fe55_charge_gamma', 'pdg_code==22 && charge >= 1500 && charge <= 1850', 'goff')
    th1_pixel_charge = ROOT.gDirectory.Get('h_fe55_charge_gamma')

    tf1_gaus = ROOT.TF1('tf1_gaus', 'gaus', 1555, 1655)
    tf1_gaus.SetParameter(0, th1_pixel_charge.GetMaximum())
    tf1_gaus.SetParameter(1, 1605)
    tf1_gaus.SetParameter(2, 15)
    th1_pixel_charge.Fit(tf1_gaus, 'RN')

    tfile_output = ROOT.TFile('Fe55ChargeDistribution.root', 'RECREATE')
    tfile_output.cd()
    tf1_gaus.Write('k_alpha_fit')
    th1_pixel_charge.Write('charge_distribution')

    th1_pixel_charge.GetYaxis().SetMaxDigits(4)

    th1_pixel_charge.SetStats(1)
    th1_pixel_charge.SetTitle(';pixel charge [e];entries')

    th1_pixel_charge.Draw()
    tf1_gaus.Draw('same')

    canvas.Modified()
    canvas.Update()

    canvas.SaveAs('Fe55ChargeDistribution.pdf')

    tfile.Close()
    tfile_output.Close()

    print('Done: Fe55ChargeDistribution')

    return canvas


def Fe55AllPlots() -> None:
    Fe55ChargeDistribution()
    print('Done: Fe55')


if __name__ == '__main__':
    ROOT.Math.MinimizerOptions.SetDefaultMinimizer('Minuit2', 'Minimize')

    ROOT.gInterpreter.ProcessLine('#include \"../csaparam/AllpixStyle.hpp\"')
    ROOT.SetInitialStyle()

    _canvas_Fe55ChargeDistribution = Fe55ChargeDistribution()

    input()
