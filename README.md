# Allpix Squared simulation for the MuPix10

[Allpix Squared](https://project-allpix-squared.web.cern.ch/project-allpix-squared/) is a framework to simulate Pixel Detector. It can be used to generate ToT spectra for any input, for example Fe55 or Sr90.

## Allpix Usage

### Software version

The current version of Allpix Squared does not have a module that simulates the digitization well enough. There are two efforts: a generalization of the CSADigitizer module that easily allows adding new types of digitizers ([here](https://github.com/allpix-squared/allpix-squared/pull/36)), and a new MuPixDigitizer module specifically made for the MuPix10 ([here](https://github.com/allpix-squared/allpix-squared/pull/35)).

For now, the second option is the preferred one. One should clone the master branch of Allpix Squared and rebase the MuPixDigitzer branch on top of it.

Note: In the current state, the ToT results don't really make sense since there is no proper parametrisation. However, it's fine for simulating the charge.

### Running simulations

Run simulations with `allpix -c path/to/config_file.conf`.

Most relevant Parameters:

- `-o output_directory="/path/to/output/dir/"`: path to store output

- `-o number_of_events=123456789`: number of events to be simulated

- `-o workers=12`: number of threads for multithreading

For more information, check the [full documentation](https://project-allpix-squared.web.cern.ch/project-allpix-squared/usermanual/allpix-manual.html).

### TCAD

Allpix Squared can use information from TCAD simulations such as the electric field. Thanks to Annie Meneses González for providing the TCAD files for the MuPix10.

Assuming you have the `mupix10_100um_100V.dat` and `mupix10_100um_100.grd` files in the `DF-ISE` format from [here](https://cernbox.cern.ch/index.php/s/ucTmvWtOfgZtHVL), you can convert them to the Allpix `APF` format using:

```
cd allpix_data/
mesh_converter -f mupix10_100um_100V
```

By default, this produces only the electric field. The doping concentration can be extraced similary, simply change `observable = "ElectricField"` to `observable = "DopingConcentration"` in `mupix10_100um_100V.conf`.

Not-so-important TODOs:

- Script to download and rename?

### Analysis

After a run in Allpix Squared, you can run the analysis program to extract the useful data. To build it, run:

```
cd analysis/
mkdir build
cd build/
cmake .. -DAllpix_DIR=path/to/allpix2/installation/share/cmake
```

The run the analysis on a data file run:

```
./analysis path/to/allpix/output/data.root
```

It will create a file called `data_analysis.root`, which includes a single TTree with the most relevant event information and some useful histograms.

Not-so-important TODOs:

- Code cleanup

## CSA Parametrization

### Introduction

The MuPix10 uses a charge-sensitive amplifier (CSA). For a proper ToT simulation, the exact parameters of the response need to be known.

Heiko Augustin provided the empirical parametrization `A*(1-exp(-t/R))-F*t` and `0` when term is negative. The expectation is that `R` and `F` and constant while `A` depends linearly on the input charge. `csa_plot.py` can be used to visualize different input pulses for a given parametrization.

Issue: how to get proper parametrisation values?

There are two possible ways to fix this issue:

- Measurements. The idea is straight forward: take a monochromatic source and measure rise time, fall time and amplitude. Then simulate the amount of particle deposited with Geant4/Allpix-Squared. With the simplification `AmpOut_Signal = CSA_curve * charges` one can fit the parameters two the measurements. The issue: finding a monochromatic source with a high enough signal. If the signal is too small (e.g. Fe55), the noise is high and thus the deviations in the measurement.

- Simulation. Benjamin Weinläder did one, the data can be found in `CSASim.csv`. A very basic fitting routine is implemented in `csa_sim_ana.py`. However the data seems off (way too small amplification).

In this repo, a parametrization with Measurements is done.

### Measurements

TODO
